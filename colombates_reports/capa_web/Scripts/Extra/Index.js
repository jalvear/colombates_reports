﻿$(document).ready(function () {
    /* Variables */

    var grdExtras = $("#grdExtras");

    crearGrid_grdExtras();



    /* Métodos */

    function crearGrid_grdExtras() {

        grdExtras.kendoGrid({
            columns: [{
                editor: function (container, options) {
                    var grid = grdExtras.data("kendoGrid");
                    var input = $('<input id="txtFechaRegistro" />')
                        .appendTo(container)
                        .kendoDatePicker({
                            change: function () {
                                var fila = grdExtras.find("tr.k-grid-edit-row")
                                    .first();
                                var registroEditando = grid.dataItem(fila);

                                registroEditando.ExtraFechaRegistro = this.value();
                                registroEditando.dirty = true;
                            },
                            format: '{0:dd/MMM/yyyy}'
                        }).data("kendoDatePicker");

                    if (options.model.ExtraFechaRegistro != null) {
                        input.value(options.model.ExtraFechaRegistro);
                    }
                    else {
                        var fecha = new Date();
                        var fila = grdExtras.find("tr.k-grid-edit-row")
                            .first();
                        var registroEditando = grid.dataItem(fila);

                        input.value(fecha);
                        registroEditando.ExtraFechaRegistro = fecha;
                        registroEditando.dirty = true;
                    }
                },
                field: "ExtraFechaRegistro",
                format: "{0:dd/MMM/yyyy}",
                title: "Fecha Registro",
                type: "date",
                width: "180px",
            },
            {
                editor: function (container, options) {
                    var contenedor_input = $(div_contenedor_input);
                    var input = $('<div id="cmbCentros" style="width: 100%;"></div>');

                    contenedor_input.append(input);
                    contenedor_input.appendTo(container);

                    input.kendoComboBox({
                        dataSource: {
                            transport: {
                                read: function (options) {
                                    /*var consultar = "";

                                    if (options.data.filter != undefined && options.data.filter.filters.length > 0) {
                                        consultar = options.data.filter.filters[0].value;
                                    }

                                    var datosEnviar = JSON.stringify({
                                        estado: "A",
                                        filtro: consultar,
                                        codigosOmitir: cmbLaboratorioComparte.value()
                                    });
                                    */
                                    $.ajax({
                                        url: "/Centro/ObtenerCentros",
                                        //data: datosEnviar,
                                        //contentType: "application/json; charset=utf-8",
                                        dataType: "json",
                                        processData: false,
                                        method: "post",
                                        success: function (centros) {
                                            console.log("Se consultaron los Centros correctamente");
                                            options.success(centros);
                                        },
                                        error: function (xhr) {
                                            options.error();

                                            bootbox.alert({
                                                title: "No se pudo consultar los Centros.",
                                                message: ObtenerErrorServidor(xhr.responseText)
                                            });
                                        }
                                    });
                                }
                            },
                            //serverFiltering: true
                        },
                        dataTextField: "Descripcion_Siglas",
                        dataValueField: "Id",
                        delay: 300,
                        filter: 'contains',
                        minLength: 1,
                        placeholder: "Asociar Centro",
                        select: function (e) {
                            var grid = grdExtras.data("kendoGrid");
                            var fila = grdExtras.find("tr.k-grid-edit-row")
                                .first();
                            var registroEditando = grid.dataItem(fila);
                            var centroSeleccionado = this.dataItem(e.item.index());

                            registroEditando.CentroId = centroSeleccionado.Id;
                            registroEditando.CentroSiglas = centroSeleccionado.Siglas;
                            registroEditando.CentroDescripcion = centroSeleccionado.Descripcion;
                            registroEditando.dirty = true;
                        },
                        valuePrimitive: true
                    }).data("kendoComboBox");

                    input.data("kendoComboBox").value(options.model.CentroSiglas);
                },
                field: "CentroId",
                filterable: {
                    cell: {
                        operator: "eq",
                        showOperators: false,
                        template: function (args) {
                            args.element.kendoComboBox({
                                dataSource: {
                                    transport: {
                                        read: function (options_filter) {
                                            /*var consultar = "";
        
                                            if (options.data.filter != undefined && options.data.filter.filters.length > 0) {
                                                consultar = options.data.filter.filters[0].value;
                                            }
        
                                            var datosEnviar = JSON.stringify({
                                                estado: "A",
                                                filtro: consultar,
                                                codigosOmitir: cmbLaboratorioComparte.value()
                                            });
                                            */
                                            $.ajax({
                                                url: "/Centro/ObtenerCentros",
                                                //data: datosEnviar,
                                                //contentType: "application/json; charset=utf-8",
                                                dataType: "json",
                                                processData: false,
                                                method: "post",
                                                success: function (centros) {
                                                    console.log("Se consultaron los Centros correctamente");
                                                    options_filter.success(centros);
                                                },
                                                error: function (xhr) {
                                                    options_filter.error();

                                                    bootbox.alert({
                                                        title: "No se pudo consultar los Centros.",
                                                        message: ObtenerErrorServidor(xhr.responseText)
                                                    });
                                                }
                                            });
                                        }
                                    },
                                    //serverFiltering: true
                                },
                                dataTextField: "Descripcion_Siglas",
                                dataValueField: "Id",
                                delay: 300,
                                filter: 'contains',
                                minLength: 1,
                                //placeholder: "Filtrar por Centro",
                                valuePrimitive: true
                            });
                        }
                    }
                },
                template: "#= CentroSiglas #",
                title: "Centro",
                width: "180px"
            },
            {
                editor: function (container, options) {
                    var contenedor_input = $(div_contenedor_input);
                    var input = $('<div name="cmbTipoHoras" style="width: 100%;"></div>');

                    contenedor_input.append(input);
                    contenedor_input.appendTo(container);

                    input.kendoComboBox({
                        dataSource: {
                            transport: {
                                read: function (options) {
                                    /*var consultar = "";

                                    if (options.data.filter != undefined && options.data.filter.filters.length > 0) {
                                        consultar = options.data.filter.filters[0].value;
                                    }

                                    var datosEnviar = JSON.stringify({
                                        estado: "A",
                                        filtro: consultar,
                                        codigosOmitir: cmbLaboratorioComparte.value()
                                    });
                                    */
                                    $.ajax({
                                        url: "/TipoHora/ObtenerTiposHora",
                                        //data: datosEnviar,
                                        //contentType: "application/json; charset=utf-8",
                                        dataType: "json",
                                        processData: false,
                                        method: "post",
                                        success: function (centros) {
                                            console.log("Se consultaron los Tipos de Hora correctamente");
                                            options.success(centros);
                                        },
                                        error: function (xhr) {
                                            options.error();

                                            bootbox.alert({
                                                title: "No se pudo consultar los Tipos de Hora.",
                                                message: ObtenerErrorServidor(xhr.responseText)
                                            });
                                        }
                                    });
                                }
                            },
                            //serverFiltering: true
                        },
                        dataTextField: "Siglas",
                        dataValueField: "Id",
                        delay: 300,
                        filter: 'contains',
                        minLength: 1,
                        placeholder: "Asociar Tipo de Hora",
                        select: function (e) {
                            var grid = grdExtras.data("kendoGrid");
                            var fila = grdExtras.find("tr.k-grid-edit-row")
                                .first();
                            var registroEditando = grid.dataItem(fila);
                            var tipoHoraSeleccionado = this.dataItem(e.item.index());

                            registroEditando.TipoHoraId = tipoHoraSeleccionado.Id;
                            registroEditando.TipoHoraSiglas = tipoHoraSeleccionado.Siglas;
                            registroEditando.TipoHoraDescripcion = tipoHoraSeleccionado.Descripcion;
                            registroEditando.dirty = true;
                        },
                        valuePrimitive: true
                    }).data("kendoComboBox");

                    if (options.model.TipoHoraId > 0) {
                        input.data("kendoComboBox").value(options.model.TipoHoraId);
                    }
                },
                field: "TipoHoraId",
                filterable: {
                    cell: {
                        operator: "eq",
                        showOperators: false,
                        template: function (args) {
                            args.element.kendoComboBox({
                                dataSource: {
                                    transport: {
                                        read: function (options_filter) {
                                            /*var consultar = "";
        
                                            if (options.data.filter != undefined && options.data.filter.filters.length > 0) {
                                                consultar = options.data.filter.filters[0].value;
                                            }
        
                                            var datosEnviar = JSON.stringify({
                                                estado: "A",
                                                filtro: consultar,
                                                codigosOmitir: cmbLaboratorioComparte.value()
                                            });
                                            */
                                            $.ajax({
                                                url: "/TipoHora/ObtenerTiposHora",
                                                //data: datosEnviar,
                                                //contentType: "application/json; charset=utf-8",
                                                dataType: "json",
                                                processData: false,
                                                method: "post",
                                                success: function (centros) {
                                                    console.log("Se consultaron los Tipos de Hora correctamente");
                                                    options_filter.success(centros);
                                                },
                                                error: function (xhr) {
                                                    options_filter.error();

                                                    bootbox.alert({
                                                        title: "No se pudo consultar los Tipos de Hora.",
                                                        message: ObtenerErrorServidor(xhr.responseText)
                                                    });
                                                }
                                            });
                                        }
                                    },
                                    //serverFiltering: true
                                },
                                dataTextField: "Siglas",
                                dataValueField: "Id",
                                delay: 300,
                                filter: 'contains',
                                minLength: 1,
                                //placeholder: "Filtrar por Centro",
                                valuePrimitive: true
                            });
                        }
                    }
                },
                template: "#= TipoHoraSiglas #",
                title: "Tipos Horas",
                width: "180px"
            },
            {
                editor: function (container, options) {
                    var contenedor_input = $(div_contenedor_input);
                    var input = $('<div id="cmbCausalExtra" style="width: 100%;"></div>');

                    contenedor_input.append(input);
                    contenedor_input.appendTo(container);

                    input.kendoComboBox({
                        dataSource: {
                            transport: {
                                read: function (options) {
                                    /*var consultar = "";

                                    if (options.data.filter != undefined && options.data.filter.filters.length > 0) {
                                        consultar = options.data.filter.filters[0].value;
                                    }

                                    var datosEnviar = JSON.stringify({
                                        estado: "A",
                                        filtro: consultar,
                                        codigosOmitir: cmbLaboratorioComparte.value()
                                    });
                                    */
                                    $.ajax({
                                        url: "/CausalExtra/ObtenerCausalesExtras",
                                        //data: datosEnviar,
                                        //contentType: "application/json; charset=utf-8",
                                        dataType: "json",
                                        processData: false,
                                        method: "post",
                                        success: function (centros) {
                                            console.log("Se consultaron los Causales de Extra correctamente");
                                            options.success(centros);
                                        },
                                        error: function (xhr) {
                                            options.error();

                                            bootbox.alert({
                                                title: "No se pudo consultar los Causales de Extra.",
                                                message: ObtenerErrorServidor(xhr.responseText)
                                            });
                                        }
                                    });
                                }
                            },
                            //serverFiltering: true
                        },
                        dataTextField: "Descripcion",
                        dataValueField: "Id",
                        delay: 300,
                        filter: 'contains',
                        minLength: 1,
                        placeholder: "Asociar Causal",
                        select: function (e) {
                            var grid = grdExtras.data("kendoGrid");
                            var fila = grdExtras.find("tr.k-grid-edit-row")
                                .first();
                            var registroEditando = grid.dataItem(fila);
                            var causalSeleccionado = this.dataItem(e.item.index());

                            registroEditando.CausalExtraId = causalSeleccionado.Id;
                            registroEditando.CausalExtraDescripcion = causalSeleccionado.Descripcion;
                            registroEditando.dirty = true;
                        },
                        valuePrimitive: true
                    }).data("kendoComboBox");

                    if (options.model.CausalExtraId != 0) {
                        input.data("kendoComboBox").value(options.model.CausalExtraId);
                    }
                },
                field: "CausalExtraId",
                filterable: {
                    cell: {
                        operator: "eq",
                        showOperators: false,
                        template: function (args) {
                            args.element.kendoComboBox({
                                dataSource: {
                                    transport: {
                                        read: function (options_filter) {
                                            /*var consultar = "";
        
                                            if (options.data.filter != undefined && options.data.filter.filters.length > 0) {
                                                consultar = options.data.filter.filters[0].value;
                                            }
        
                                            var datosEnviar = JSON.stringify({
                                                estado: "A",
                                                filtro: consultar,
                                                codigosOmitir: cmbLaboratorioComparte.value()
                                            });
                                            */
                                            $.ajax({
                                                url: "/CausalExtra/ObtenerCausalesExtras",
                                                //data: datosEnviar,
                                                //contentType: "application/json; charset=utf-8",
                                                dataType: "json",
                                                processData: false,
                                                method: "post",
                                                success: function (centros) {
                                                    console.log("Se consultaron los Causales de Extra correctamente");
                                                    options_filter.success(centros);
                                                },
                                                error: function (xhr) {
                                                    options_filter.error();

                                                    bootbox.alert({
                                                        title: "No se pudo consultar los Causales de Extra.",
                                                        message: ObtenerErrorServidor(xhr.responseText)
                                                    });
                                                }
                                            });
                                        }
                                    },
                                    //serverFiltering: true
                                },
                                dataTextField: "Descripcion",
                                dataValueField: "Id",
                                delay: 300,
                                filter: 'contains',
                                minLength: 1,
                                //placeholder: "Filtrar por Centro",
                                valuePrimitive: true
                            });
                        }
                    }
                },
                template: "#= CausalExtraDescripcion #",
                title: "Causa",
                width: "180px"
            },
            {
                editor: function (container, options) {
                    var contenedor_input = $(div_contenedor_input);
                    var input = $.Editor({
                        atributos: {
                            id: "txtHorasExtra",
                            min: "0"
                        },
                        tipo: "numero"
                    });

                    if (options.model.ExtraHorasExtra != null) {
                        input.val(options.model.ExtraHorasExtra);
                    }

                    contenedor_input.append(input);
                    contenedor_input.appendTo(container);

                    input.change(function () {
                        //$("#txtHorasProgramadas").val(input.val() * 8);

                        var grid = grdExtras.data("kendoGrid");
                        var fila = grdExtras.find("tr.k-grid-edit-row")
                            .first();
                        var registroEditando = grid.dataItem(fila);

                        registroEditando.ExtraHorasExtra = input.val();
                        registroEditando.dirty = true;
                    });
                },
                field: "ExtraHorasExtra",
                filterable: false,
                title: "Horas Extra",
                width: "120px"
            },
            {
                field: "Semana",
                filterable: false,
                title: "Semana",
                width: "90px"
            },
            {
                command: [{
                    name: "edit",
                    text: { edit: "Modificar", cancel: "Cancelar", update: "Guardar" }
                },
                {
                    name: "eliminar",
                    click: function (e) {
                        e.preventDefault();

                        var grid = this;


                        bootbox.confirm({
                            buttons: {
                                confirm: {
                                    label: 'Si',
                                    className: 'btn-success'
                                },
                                cancel: {
                                    label: 'No',
                                    className: 'btn-danger'
                                }
                            },
                            message: "¿Realmente desea eliminar este registro de Extras?",
                            callback: function (result) {
                                if (result) {
                                    console.log('Eliminando registro de Extras');

                                    var tr = $(e.target).closest("tr"); // get the current table row (tr)
                                    grid.removeRow(tr);
                                }
                            }
                        });
                    },
                    text: "Eliminar"
                }],
                width: "200px"
            }],
            dataSource: dataSource_grdExtras(),
            editable: {
                confirmation: false,
                mode: 'inline'
            },
            filterable: { mode: "row" },
            groupable: false,
            pageable: {
                refresh: true,
                pageSizes: [5, 10, 20],
            },
            resizable: true,
            selectable: "row",
            sortable: true,
            scrollable: true,
            toolbar: [{ name: "create", text: "Crear" }]
        });
    }

    function dataSource_grdExtras() {
        var gridDataSource = new kendo.data.DataSource({
            serverFiltering: true,
            serverPaging: true,
            serverSorting: true,
            transport: {
                read: function (options) {
                    var filtering = [];
                    var sorting = [];

                    if (options.data.filter != undefined) {
                        $.each(options.data.filter.filters, function (indice, filtro) {
                            filtering.push({ Field: filtro.field, Value: filtro.value, Operator: filtro.operator });
                        });
                    }

                    if (options.data.sort != undefined) {
                        $.each(options.data.sort, function (indice, orden) {
                            sorting.push({ Field: orden.field, Dir: orden.dir });
                        });
                    }

                    var take = options.data.take;

                    if (take == undefined) {
                        take = 10;
                    }

                    var datosEnviar = {
                        filter: filtering,
                        sort: sorting,
                        skip: options.data.skip,
                        take: take
                    };


                    // TODO: Agregar espera
                    console.log('Consultando Extras');

                    $.ajax({
                        url: "/Extra/ObtenerExtras",
                        method: "POST",
                        dataType: "json", // Lo que recibo del servidor
                        contentType: "application/json", // Tipo de datos que envio al servidor 
                        data: JSON.stringify(datosEnviar),
                        complete: function () {
                            // TODO: Terminar espera
                        },
                        success: function (respuesta) {
                            console.log('Extras consultadas correctamente');
                            options.success(respuesta);
                        },
                        error: function (xhr) {
                            options.error();

                            bootbox.alert({
                                title: "Ocurrió un error al consultar los Extras.",
                                message: ObtenerErrorServidor(xhr.responseText)
                            });
                        }
                    });
                },
                create: function (options) {
                    var grid = grdExtras.data("kendoGrid");
                    var modelo = ConvertirModelo(options.data);
                    var errores = ValidarModelo(modelo);

                    if (errores.length > 0) {
                        options.error();

                        bootbox.alert({
                            title: "Tener en cuenta lo siguiente:",
                            message: errores.join("")
                        });

                        return;
                    }

                    console.log("Creando Extra");

                    $.ajax({
                        url: "/Extra/Crear",
                        method: "POST",
                        //dataType: "json", // Lo que recibo del servidor
                        contentType: "application/json", // Tipo de datos que envio al servidor
                        data: JSON.stringify(modelo),
                        success: function (respuesta) {
                            console.log('Extra creado correctamente');
                            options.success();

                            //$("#btnGuardar, #btnCancelar").fadeOut(250, function () {
                            //    $("#btnCrear, #btnEditar, #btnEliminar").fadeIn();
                            //});

                            grid.dataSource.read();
                        },
                        error: function (xhr) {
                            options.error();

                            bootbox.alert({
                                title: "Ocurrió un error al crear la Extra.",
                                message: ObtenerErrorServidor(xhr.responseText)
                            });
                        }
                    });
                },
                update: function (options) {
                    var grid = grdExtras.data("kendoGrid");
                    var modelo = ConvertirModelo(options.data);
                    var errores = ValidarModelo(modelo);

                    if (errores.length > 0) {
                        options.error();

                        bootbox.alert({
                            title: "Tener en cuenta lo siguiente:",
                            message: errores.join("")
                        });

                        return;
                    }

                    console.log('Editando Extra');

                    $.ajax({
                        url: "/Extra/Editar",
                        method: "POST",
                        //dataType: "json", // Lo que recibo del servidor
                        contentType: "application/json", // Tipo de datos que envio al servidor
                        data: JSON.stringify(modelo),
                        success: function (respuesta) {
                            console.log('Extra editada correctamente');
                            options.success();

                            //$("#btnGuardar, #btnCancelar").fadeOut(250, function () {
                            //    $("#btnCrear, #btnEditar, #btnEliminar").fadeIn();
                            //});

                            grid.dataSource.read();
                        },
                        error: function (xhr) {
                            options.error();

                            bootbox.alert({
                                title: "Ocurrió un error al editar la Extra.",
                                message: ObtenerErrorServidor(xhr.responseText)
                            });
                        }
                    });
                },
                destroy: function (options) {
                    var grid = grdExtras.data("kendoGrid");

                    $.ajax({
                        url: "/Extra/Eliminar",
                        method: "POST",
                        //dataType: "json", // Lo que recibo del servidor
                        contentType: "application/json", // Tipo de datos que envio al servidor
                        data: JSON.stringify({
                            id: options.data.ExtraId
                        }),
                        success: function (respuesta) {
                            console.log('Extra eliminado correctamente');
                            options.success();

                            //$("#btnGuardar, #btnCancelar").fadeOut(250, function () {
                            //    $("#btnCrear, #btnEditar, #btnEliminar").fadeIn();
                            //});

                            grid.dataSource.read();
                        },
                        error: function (xhr) {
                            options.error();

                            bootbox.alert({
                                title: "Ocurrió un error al eliminar la Extra.",
                                message: ObtenerErrorServidor(xhr.responseText)
                            });
                        }
                    });
                }
            },
            pageSize: 10,
            schema: {
                data: 'value',
                model: {
                    id: "ExtraId",
                    fields: {
                        ExtraId: { editable: false, type: "number" },
                        CentroId: { editable: true, nullable: false, type: "number" },
                        CentroSiglas: { editable: true, nullable: false, type: "string" },
                        CentroDescripcion: { editable: true, nullable: false, type: "string" },
                        Semana: { defaultValue: 1, editable: false, nullable: true, type: "number" },
                        Mes: { defaultValue: 1, editable: false, nullable: true, type: "number" },
                        ExtraHorasExtra: { defaultValue: 0, editable: true, nullable: false, type: "number" },
                        CausalExtraId: { editable: true, nullable: false, type: "number" },
                        CausalExtraDescripcion: { editable: true, nullable: false, type: "string" },
                        TipoHoraId: { editable: true, nullable: false, type: "number" },
                        TipoHoraSiglas: { editable: true, nullable: false, type: "string" },
                        TipoHoraDescripcion: { editable: true, nullable: false, type: "string" },
                        ExtraFechaRegistro: { editable: true, format: '{0:dd/MMM/yyyy HH:mm tt}', nullable: true, type: 'date' },
                        //FechaCreado: { editable: false, format: '{0:dd/MMM/yyyy HH:mm tt}', nullable: true, type: 'date' },
                        //FechaModificado: { editable: false, format: '{0:dd/MMM/yyyy HH:mm tt}', nullable: true, type: 'date' }
                    }
                },
                total: 'count',
            }
        });

        return gridDataSource;
    }

    function ValidarModelo(modelo) {
        var mensajes = [];

        if (modelo.CentroId == null || (modelo.CentroId != null && modelo.CentroId == 0)) {
            mensajes.push("<p>El Centro no puede estár vacío.</p>");
        }

        if (modelo.CausalExtraId == null || (modelo.CausalExtraId != null && modelo.CausalExtraId == 0)) {
            mensajes.push("<p>El Turno no puede estár vacío.</p>");
        }

        if (modelo.TipoHoraId == null || (modelo.TipoHoraId != null && modelo.TipoHoraId == 0)) {
            mensajes.push("<p>El Tipo de Hora no puede estár vacío.</p>");
        }

        if (modelo.FechaRegistro == null) {
            mensajes.push("<p>La Fecha de Registro no puede estar vacía</p>");
        }

        if (modelo.HorasExtra == null || (modelo.HorasExtra != null && modelo.HorasExtra == 0)) {
            mensajes.push("<p>El campo Horas Extra no puede estar vacías</p>");
        }

        return mensajes;
    }

    function ConvertirModelo(modelo) {
        var nuevoModelo = {
            CentroId: modelo.CentroId,
            CausalExtraId: modelo.CausalExtraId,
            TipoHoraId: modelo.TipoHoraId
        };

        if (modelo.ExtraId !== undefined) {
            nuevoModelo.Id = modelo.ExtraId;
        }

        if (modelo.ExtraHorasExtra !== undefined) {
            nuevoModelo.HorasExtra = parseFloat(modelo.ExtraHorasExtra);
        }

        if (modelo.ExtraFechaRegistro !== undefined) {
            nuevoModelo.FechaRegistro = modelo.ExtraFechaRegistro;
        }

        return nuevoModelo;
    }
});