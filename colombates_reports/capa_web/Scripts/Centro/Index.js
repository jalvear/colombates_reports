﻿$(document).ready(function () {
    /* Variables */

    var grdCentros = $("#grdCentros");

    crearGrid_grdCentros();



    /* Métodos */

    function crearGrid_grdCentros() {

        grdCentros.kendoGrid({
            columns: [{
                editor: function (container, options) {
                    var contenedor_input = $(div_contenedor_input);
                    var input = $.Editor({
                        atributos: {
                            id: "txtDescripcion",
                            maxlength: "100"
                        },
                        tipo: "texto"
                    });

                    if (options.model.Descripcion != null) {
                        input.val(options.model.Descripcion);
                    }

                    contenedor_input.append(input);
                    contenedor_input.appendTo(container);

                    input.change(function () {
                        options.model.Descripcion = input.val();
                        options.model.dirty = true;
                    });
                },
                field: "Descripcion",
                title: "Descripción",
                width: "300px"
            }, {
                editor: function (container, options) {
                    var contenedor_input = $(div_contenedor_input);
                    var input = $.Editor({
                        atributos: {
                            id: "txtSiglas",
                            maxlength: "2"
                        },
                        tipo: "texto"
                    });

                    if (options.model.Siglas != null) {
                        input.val(options.model.Siglas);
                    }

                    contenedor_input.append(input);
                    contenedor_input.appendTo(container);

                    input.change(function () {
                        options.model.Siglas = input.val()
                        options.model.dirty = true;
                    });
                },
                field: "Siglas",
                title: "Siglas",
                width: "120px"
            },
            {
                command: [{
                    name: "edit",
                    text: { edit: "Modificar", cancel: "Cancelar", update: "Guardar" }
                },
                {
                    name: "eliminar",
                    click: function (e) {
                        e.preventDefault();

                        var grid = this;


                        bootbox.confirm({
                            buttons: {
                                confirm: {
                                    label: 'Si',
                                    className: 'btn-success'
                                },
                                cancel: {
                                    label: 'No',
                                    className: 'btn-danger'
                                }
                            },
                            message: "¿Realmente desea eliminar el Centro?",
                            callback: function (result) {
                                if (result) {
                                    console.log('Eliminando Centro');

                                    var tr = $(e.target).closest("tr"); // get the current table row (tr)
                                    grid.removeRow(tr);
                                }
                            }
                        });
                    },
                    text: "Eliminar"
                }]
            }],
            dataSource: dataSource_grdCentros(),
            editable: {
                confirmation: false,
                mode: 'inline'
            },
            filterable: {
                mode: "row"
            },
            groupable: false,
            pageable: {
                refresh: true,
                pageSizes: [5, 10, 20],
            },
            resizable: true,
            selectable: "row",
            sortable: true,
            scrollable: true,
            toolbar: [{ name: "create", text: "Crear" }]
        });
    }

    function dataSource_grdCentros() {
        var gridDataSource = new kendo.data.DataSource({
            serverFiltering: false,
            serverPaging: false,
            serverSorting: false,
            transport: {
                read: function (options) {
                    /*
                    var filtering = [];
                    var sorting = [];
    
                    if (options.data.filter != undefined) {
                        $.each(options.data.filter.filters, function (indice, filtro) {
                            filtering.push({ Field: filtro.field, Value: filtro.value, Operator: filtro.operator });
                        });
                    }
    
                    if (options.data.sort != undefined) {
                        $.each(options.data.sort, function (indice, orden) {
                            sorting.push({ Field: orden.field, Dir: orden.dir });
                        });
                    }
    
                    var take = options.data.take;
    
                    if (take == undefined) {
                        take = 5;
                    }
    
                    
    
                    var datosEnviar = {
                        filter: filtering,
                        sort: sorting,
                        skip: options.data.skip,
                        take: take
                    };
                    */

                    // TODO: Agregar espera
                    console.log('Consultando Centros');

                    $.ajax({
                        url: "/Centro/ObtenerCentros",
                        method: "POST",
                        dataType: "json", // Lo que recibo del servidor
                        /*contentType: "application/json", // Tipo de datos que envio al servidor */
                        /*data: JSON.stringify(datosEnviar),*/
                        complete: function () {
                            // TODO: Terminar espera
                        },
                        success: function (respuesta) {
                            console.log('Centros consultados correctamente');
                            options.success(respuesta);
                        },
                        error: function (xhr) {
                            options.error();

                            bootbox.alert({
                                title: "Ocurrió un error al consultar los Centros.",
                                message: ObtenerErrorServidor(xhr.responseText)
                            });
                        }
                    });
                },
                create: function (options) {
                    var grid = grdCentros.data("kendoGrid");
                    var errores = ValidarModelo(options.data);

                    if (errores.length > 0) {
                        options.error();

                        bootbox.alert({
                            title: "Tener en cuenta lo siguiente:",
                            message: errores.join("")
                        });

                        return;
                    }

                    console.log("Creando centro");

                    $.ajax({
                        url: "/Centro/Crear",
                        method: "POST",
                        //dataType: "json", // Lo que recibo del servidor
                        contentType: "application/json", // Tipo de datos que envio al servidor
                        data: JSON.stringify(options.data),
                        success: function (respuesta) {
                            console.log('Centro creado correctamente');
                            options.success();

                            //$("#btnGuardar, #btnCancelar").fadeOut(250, function () {
                            //    $("#btnCrear, #btnEditar, #btnEliminar").fadeIn();
                            //});

                            grid.dataSource.read();
                        },
                        error: function (xhr) {
                            options.error();

                            bootbox.alert({
                                title: "Ocurrió un error al crear el Centro.",
                                message: ObtenerErrorServidor(xhr.responseText)
                            });
                        }
                    });
                },
                update: function (options) {
                    var grid = grdCentros.data("kendoGrid");
                    var errores = ValidarModelo(options.data);

                    if (errores.length > 0) {
                        options.error();

                        bootbox.alert({
                            title: "Tener en cuenta lo siguiente:",
                            message: errores.join("")
                        });

                        return;
                    }

                    console.log('Editando Centro');

                    $.ajax({
                        url: "/Centro/Editar",
                        method: "POST",
                        //dataType: "json", // Lo que recibo del servidor
                        contentType: "application/json", // Tipo de datos que envio al servidor
                        data: JSON.stringify(options.data),
                        success: function (respuesta) {
                            console.log('Centro editado correctamente');
                            options.success();

                            //$("#btnGuardar, #btnCancelar").fadeOut(250, function () {
                            //    $("#btnCrear, #btnEditar, #btnEliminar").fadeIn();
                            //});

                            grid.dataSource.read();
                        },
                        error: function (xhr) {
                            options.error();

                            bootbox.alert({
                                title: "Ocurrió un error al crear el Centro.",
                                message: ObtenerErrorServidor(xhr.responseText)
                            });
                        }
                    });
                },
                destroy: function (options) {
                    var grid = grdCentros.data("kendoGrid");

                    $.ajax({
                        url: "/Centro/Eliminar",
                        method: "POST",
                        //dataType: "json", // Lo que recibo del servidor
                        contentType: "application/json", // Tipo de datos que envio al servidor
                        data: JSON.stringify({
                            id: options.data.Id
                        }),
                        success: function (respuesta) {
                            console.log('Centro eliminado correctamente');
                            options.success();

                            //$("#btnGuardar, #btnCancelar").fadeOut(250, function () {
                            //    $("#btnCrear, #btnEditar, #btnEliminar").fadeIn();
                            //});

                            grid.dataSource.read();
                        },
                        error: function (xhr) {
                            options.error();

                            bootbox.alert({
                                title: "Ocurrió un error al crear el Centro.",
                                message: ObtenerErrorServidor(xhr.responseText)
                            });
                        }
                    });
                }
            },
            pageSize: 5,
            schema: {
                //data: 'value',
                model: {
                    id: "Id",
                    fields: {
                        Id: { editable: false, type: "number" },
                        Descripcion: { editable: true, nullable: false, type: "string" },
                        Siglas: { editable: true, nullable: false, type: "string" },
                        FechaCreado: { editable: false, format: '{0:dd/MMM/yyyy HH:mm tt}', nullable: true, type: 'date' },
                        FechaModificado: { editable: false, format: '{0:dd/MMM/yyyy HH:mm tt}', nullable: true, type: 'date' }
                    }
                },
                //total: 'count',
            }
        });

        return gridDataSource;
    }

    function ValidarModelo(modelo) {
        var mensajes = [];

        if (modelo.Descripcion == null || modelo.Descripcion.replace(/\s+/, "").length == 0) {
            mensajes.push("<p>La descripción no puede estar vacía.</p>");
        } else if (modelo.Descripcion.length > 100) {
            mensajes.push("<p>La descripción puede contener hasta 100 caracteres</p>");
        }

        if (modelo.Siglas == null || modelo.Siglas.replace(/\s+/, "").length == 0) {
            mensajes.push("<p>Las siglas no pueden estar vacías</p>");
        } else if (modelo.Siglas.length > 2) {
            mensajes.push("<p>Las siglas pueden contener hasta 2 caracteres</p>");
        }

        return mensajes;
    }
});