﻿$(function () {
    // Variables
    var reporte = $("#btnGenerarReporte").data("reporte");
    var reporte_url = "/" + reporte + "/ObtenerDatosMensual";

    // Controles
    var txtAnio = $("#txtAnio").kendoNumericTextBox({
        decimals: 0,
        format: "n0",
        min: 2018,
        value: 2018
    }).data("kendoNumericTextBox");

    var selSemanas = $("#selSemanas").kendoMultiSelect({
        dataSource: obtener_arreglo_numeros(1, 52),
        placeholder: "Seleccione una o varias semanas",
        enable: false
    }).data("kendoMultiSelect");

    var chkConsultarPorSemanas = $("#chkConsultarPorSemanas");

    var btnGenerarReporte = $("#btnGenerarReporte");
    var btnExportarPDF = $("#btnExportarPDF");
    var btnExportarImagen = $("#btnExportarImagen");

    // Eventos
    chkConsultarPorSemanas.change(function (event) {
        var checked = event.target.checked;

        selSemanas.enable(checked);
        txtAnio.enable(!checked);

        var grafica = $("#grafica1").data("kendoChart");

        if (!checked) {
            switch (reporte) {
                case "HorasHombrePlantaSacos":
                    reporte_url = "/HorasHombrePlantaSacos/ObtenerDatosMensual";
                    grafica.setOptions({ title: { text: "HORAS HOMBRE PLANTA SACOS" } });
                    break;
                default:
            }
        }
        else {
            switch (reporte) {
                case "HorasHombrePlantaSacos":
                    reporte_url = "/HorasHombrePlantaSacos/ObtenerDatosSemanal";
                    grafica.setOptions({ title: { text: "HORAS HOMBRE PLANTA SEMANAL SACOS" } });
                    break;
                default:
            }
        }
    });

    btnGenerarReporte.click(function () {
        var errores = validar_antes_generar();

        if (errores.length > 0) {

            bootbox.alert({
                title: "Tener en cuenta lo siguiente:",
                message: errores.join("")
            });

            return;
        }

        var datosEnviar = {
            anio: txtAnio.value()
        };

        if (chkConsultarPorSemanas.prop("checked")) {
            // Semanal
            datosEnviar.semanas = selSemanas.value();
        }

        // TODO: Agregar espera
        console.log('Generando Reporte');

        $.ajax({
            url: reporte_url,
            method: "POST",
            dataType: "json", // Lo que recibo del servidor
            contentType: "application/json", // Tipo de datos que envio al servidor 
            data: JSON.stringify(datosEnviar),
            complete: function () {
                // TODO: Terminar espera
            },
            success: function (respuesta) {
                console.log('Reporte generado correctamente');

                var grafica = $("#grafica1").data("kendoChart");

                grafica.dataSource.data(respuesta);
            },
            error: function (xhr) {

                bootbox.alert({
                    title: "Ocurrió un error al generar el reporte.",
                    message: ObtenerErrorServidor(xhr.responseText)
                });
            }
        });
    });

    btnExportarPDF.click(function () {
        var grafica = $("#grafica1").data("kendoChart");

        if (grafica.dataSource.data().length === 0) {
            bootbox.alert({
                title: "Alerta",
                message: "Por favor genere primero el reporte."
            });

            return;
        }

        grafica.exportPDF(/*{ paperSize: "A5", landscape: true }*/).done(function (data) {
            kendo.saveAs({
                dataURI: data,
                fileName: reporte + "_" + kendo.format("{0:ddMMyyyy_hhmmss}", new Date()) + ".pdf"
            });
        });
    });

    btnExportarImagen.click(function () {
        var grafica = $("#grafica1").data("kendoChart");

        if (grafica.dataSource.data().length === 0) {
            bootbox.alert({
                title: "Alerta",
                message: "Por favor genere primero el reporte."
            });

            return;
        }

        grafica.exportImage().done(function (data) {
            kendo.saveAs({
                dataURI: data,
                fileName: reporte + "_" + kendo.format("{0:ddMMyyyy_hhmmss}", new Date()) + ".png"
            });
        });
    });

    // Funciones
    function validar_antes_generar() {
        var errores = [];

        if (reporte === undefined || reporte === null) {
            errores.push("<p>No se especificó el reporte a generar.</p>");
        }

        if (txtAnio.value() === null) {
            errores.push("<p>Por favor especifique el año a consultar.</p>");
        }

        if (chkConsultarPorSemanas.prop("checked")) {
            // Semanal

            if (selSemanas.value().length === 0) {
                errores.push("<p>Por favor especifique las semenas a consultar.</p>");
            }
        }

        return errores;
    }
});