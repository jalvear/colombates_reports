﻿$(document).ready(function () {
    /* Variables */

    var grdEmpleados = $("#grdEmpleados");

    crearGrid_grdEmpleados();

    /* Métodos */

    function Asignar_Anio(anio) {
        var grid = grdEmpleados.data("kendoGrid");
        var fila = grdEmpleados.find("tr.k-grid-edit-row")
            .first();
        var registroEditando = grid.dataItem(fila);

        registroEditando.Anio = anio;
        registroEditando.dirty = true;
    }

    function crearGrid_grdEmpleados() {

        grdEmpleados.kendoGrid({
            columns: [
                {
                    editor: function (container, options) {
                        var contenedor_input = $(div_contenedor_input);
                        var input = $.Editor({
                            atributos: {
                                id: "txtAnio",
                                min: "1"
                            },
                            tipo: "numero_entero"
                        });

                        var anio = options.model.Anio;

                        if (options.model.Anio === 0) {
                            anio = new Date().getFullYear();
                        }

                        input.val(anio);
                        Asignar_Anio(anio);

                        contenedor_input.append(input);
                        contenedor_input.appendTo(container);

                        input.change(function () {
                            //$("#txtHorasProgramadas").val(input.val() * 8);

                            var valor = input.val();

                            if (valor.replace(/\s+/, "").length === 0) {
                                valor = 0;
                            }

                            Asignar_Anio(valor);
                        });
                    },
                    field: "Anio",
                    title: "Año",
                    width: "180px"
                },
                {
                    editor: function (container, options) {
                        var contenedor_input = $(div_contenedor_input);
                        var input = $.Editor({
                            atributos: {
                                id: "txtSemana",
                                min: "1"
                            },
                            tipo: "numero_entero"
                        });

                        input.val(options.model.Semana);

                        contenedor_input.append(input);
                        contenedor_input.appendTo(container);

                        input.change(function () {
                            var grid = grdEmpleados.data("kendoGrid");
                            var fila = grdEmpleados.find("tr.k-grid-edit-row")
                                .first();
                            var registroEditando = grid.dataItem(fila);
                            var valor = input.val();


                            if (valor.replace(/\s+/, "").length === 0) {
                                valor = 0;
                            }

                            registroEditando.Semana = valor;
                            registroEditando.dirty = true;
                        });
                    },
                    field: "Semana",
                    title: "Semana",
                    width: "180px"
                },
                {
                    editor: function (container, options) {
                        var contenedor_input = $(div_contenedor_input);
                        var input = $('<div name="cmbCentros" style="width: 100%;"></div>');

                        contenedor_input.append(input);
                        contenedor_input.appendTo(container);

                        input.kendoComboBox({
                            dataSource: {
                                transport: {
                                    read: function (options) {
                                        /*var consultar = "";
    
                                        if (options.data.filter != undefined && options.data.filter.filters.length > 0) {
                                            consultar = options.data.filter.filters[0].value;
                                        }
    
                                        var datosEnviar = JSON.stringify({
                                            estado: "A",
                                            filtro: consultar,
                                            codigosOmitir: cmbLaboratorioComparte.value()
                                        });
                                        */
                                        $.ajax({
                                            url: "/Centro/ObtenerCentros",
                                            //data: datosEnviar,
                                            //contentType: "application/json; charset=utf-8",
                                            dataType: "json",
                                            processData: false,
                                            method: "post",
                                            success: function (centros) {
                                                console.log("Se consultaron los Centros correctamente");
                                                options.success(centros);
                                            },
                                            error: function (xhr) {
                                                options.error();

                                                bootbox.alert({
                                                    title: "No se pudo consultar los Centros.",
                                                    message: ObtenerErrorServidor(xhr.responseText)
                                                });
                                            }
                                        });
                                    }
                                }
                                //serverFiltering: true
                            },
                            dataTextField: "Descripcion_Siglas",
                            dataValueField: "Id",
                            delay: 300,
                            filter: 'contains',
                            minLength: 1,
                            placeholder: "Asociar Centro",
                            select: function (e) {
                                var grid = grdEmpleados.data("kendoGrid");
                                var fila = grdEmpleados.find("tr.k-grid-edit-row")
                                    .first();
                                var registroEditando = grid.dataItem(fila);
                                var centroSeleccionado = this.dataItem(e.item.index());

                                registroEditando.CentroId = centroSeleccionado.Id;
                                registroEditando.CentroSiglas = centroSeleccionado.Siglas;
                                registroEditando.CentroDescripcion = centroSeleccionado.Descripcion;
                                registroEditando.dirty = true;
                            },
                            valuePrimitive: true
                        }).data("kendoComboBox");

                        input.data("kendoComboBox").value(options.model.CentroSiglas);
                    },
                    field: "CentroId",
                    filterable: {
                        cell: {
                            operator: "eq",
                            showOperators: false,
                            template: function (args) {
                                args.element.kendoComboBox({
                                    dataSource: {
                                        transport: {
                                            read: function (options_filter) {
                                                /*var consultar = "";
            
                                                if (options.data.filter != undefined && options.data.filter.filters.length > 0) {
                                                    consultar = options.data.filter.filters[0].value;
                                                }
            
                                                var datosEnviar = JSON.stringify({
                                                    estado: "A",
                                                    filtro: consultar,
                                                    codigosOmitir: cmbLaboratorioComparte.value()
                                                });
                                                */
                                                $.ajax({
                                                    url: "/Centro/ObtenerCentros",
                                                    //data: datosEnviar,
                                                    //contentType: "application/json; charset=utf-8",
                                                    dataType: "json",
                                                    processData: false,
                                                    method: "post",
                                                    success: function (centros) {
                                                        console.log("Se consultaron los Centros correctamente");
                                                        options_filter.success(centros);
                                                    },
                                                    error: function (xhr) {
                                                        options_filter.error();

                                                        bootbox.alert({
                                                            title: "No se pudo consultar los Centros.",
                                                            message: ObtenerErrorServidor(xhr.responseText)
                                                        });
                                                    }
                                                });
                                            }
                                        }
                                        //serverFiltering: true
                                    },
                                    dataTextField: "Descripcion_Siglas",
                                    dataValueField: "Id",
                                    delay: 300,
                                    filter: 'contains',
                                    minLength: 1,
                                    //placeholder: "Filtrar por Centro",
                                    valuePrimitive: true
                                });
                            }
                        }
                    },
                    template: "#= CentroSiglas #",
                    title: "Centro",
                    width: "180px"
                },
                {
                    editor: function (container, options) {
                        var contenedor_input = $(div_contenedor_input);
                        var input = $.Editor({
                            atributos: {
                                id: "txtCantidadEmpleados",
                                min: "1"
                            },
                            tipo: "numero_entero"
                        });

                        input.val(options.model.Cantidad);

                        contenedor_input.append(input);
                        contenedor_input.appendTo(container);

                        input.change(function () {
                            //$("#txtHorasProgramadas").val(input.val() * 8);

                            var grid = grdEmpleados.data("kendoGrid");
                            var fila = grdEmpleados.find("tr.k-grid-edit-row")
                                .first();
                            var registroEditando = grid.dataItem(fila);
                            var valor = input.val();

                            if (valor.replace(/\s+/, "").length === 0) {
                                valor = 0;
                            }

                            registroEditando.Cantidad = valor;
                            registroEditando.dirty = true;
                        });
                    },
                    field: "Cantidad",
                    filterable: false,
                    title: "Cantidad Empleados",
                    width: "120px"
                },
                {
                    editor: function (container, options) {
                        var contenedor_input = $(div_contenedor_input);
                        var input = $.Editor({
                            atributos: {
                                id: "txtTubos",
                                min: "1"
                            },
                            tipo: "numero_entero"
                        });

                        if (options.model.Tubos !== null) {
                            input.val(options.model.Tubos);
                        }

                        contenedor_input.append(input);
                        contenedor_input.appendTo(container);

                        input.change(function () {
                            //$("#txtHorasProgramadas").val(input.val() * 8);

                            var grid = grdEmpleados.data("kendoGrid");
                            var fila = grdEmpleados.find("tr.k-grid-edit-row")
                                .first();
                            var registroEditando = grid.dataItem(fila);

                            var valor = input.val();

                            if (valor.replace(/\s+/, "").length === 0) {
                                valor = null;
                            }

                            registroEditando.Tubos = valor;

                            registroEditando.dirty = true;
                        });
                    },
                    field: "Tubos",
                    filterable: false,
                    title: "Tubos",
                    width: "120px"
                },
                {
                    command: [{
                        name: "edit",
                        text: { edit: "Modificar", cancel: "Cancelar", update: "Guardar" }
                    },
                    {
                        name: "eliminar",
                        click: function (e) {
                            e.preventDefault();

                            var grid = this;


                            bootbox.confirm({
                                buttons: {
                                    confirm: {
                                        label: 'Si',
                                        className: 'btn-success'
                                    },
                                    cancel: {
                                        label: 'No',
                                        className: 'btn-danger'
                                    }
                                },
                                message: "¿Realmente desea eliminar el registro?",
                                callback: function (result) {
                                    if (result) {
                                        console.log('Eliminando registro');

                                        var tr = $(e.target).closest("tr"); // get the current table row (tr)
                                        grid.removeRow(tr);
                                    }
                                }
                            });
                        },
                        text: "Eliminar"
                    }]
                }],
            dataSource: dataSource_grdEmpleados(),
            editable: {
                confirmation: false,
                mode: 'inline'
            },
            filterable: { mode: "row" },
            groupable: false,
            pageable: {
                refresh: true,
                pageSizes: [5, 10, 20]
            },
            resizable: true,
            selectable: "row",
            sortable: true,
            scrollable: true,
            toolbar: [{ name: "create", text: "Crear" }]
        });
    }

    function dataSource_grdEmpleados() {
        var gridDataSource = new kendo.data.DataSource({
            serverFiltering: true,
            serverPaging: true,
            serverSorting: true,
            transport: {
                read: function (options) {
                    var filtering = [];
                    var sorting = [];

                    if (options.data.filter !== undefined) {
                        $.each(options.data.filter.filters, function (indice, filtro) {
                            filtering.push({ Field: filtro.field, Value: filtro.value, Operator: filtro.operator });
                        });
                    }

                    if (options.data.sort !== undefined) {
                        $.each(options.data.sort, function (indice, orden) {
                            sorting.push({ Field: orden.field, Dir: orden.dir });
                        });
                    }

                    var take = options.data.take;

                    if (take === undefined) {
                        take = 10;
                    }

                    var datosEnviar = {
                        filter: filtering,
                        sort: sorting,
                        skip: options.data.skip,
                        take: take
                    };

                    // TODO: Agregar espera
                    console.log('Consultando registros');

                    $.ajax({
                        url: "/EmpleadoSemana/ObtenerEmpleadosSemana",
                        method: "POST",
                        dataType: "json", // Lo que recibo del servidor
                        contentType: "application/json", // Tipo de datos que envio al servidor 
                        data: JSON.stringify(datosEnviar),
                        complete: function () {
                            // TODO: Terminar espera
                        },
                        success: function (respuesta) {
                            console.log('Registros consultados correctamente');
                            options.success(respuesta);
                        },
                        error: function (xhr) {
                            options.error();

                            bootbox.alert({
                                title: "Ocurrió un error al consultar los registros.",
                                message: ObtenerErrorServidor(xhr.responseText)
                            });
                        }
                    });
                },
                create: function (options) {
                    var grid = grdEmpleados.data("kendoGrid");
                    var modelo = ConvertirModelo(options.data);
                    var errores = ValidarModelo(modelo);

                    if (errores.length > 0) {
                        options.error();

                        bootbox.alert({
                            title: "Tener en cuenta lo siguiente:",
                            message: errores.join("")
                        });

                        return;
                    }

                    console.log("Creando registro");

                    $.ajax({
                        url: "/EmpleadoSemana/Crear",
                        method: "POST",
                        //dataType: "json", // Lo que recibo del servidor
                        contentType: "application/json", // Tipo de datos que envio al servidor
                        data: JSON.stringify(modelo),
                        success: function (respuesta) {
                            console.log('Empleado creado correctamente');
                            options.success();

                            grid.dataSource.read();
                        },
                        error: function (xhr) {
                            options.error();

                            bootbox.alert({
                                title: "Ocurrió un error al crear el registro.",
                                message: ObtenerErrorServidor(xhr.responseText)
                            });
                        }
                    });
                },
                update: function (options) {
                    var grid = grdEmpleados.data("kendoGrid");
                    var modelo = ConvertirModelo(options.data);
                    var errores = ValidarModelo(modelo);

                    if (errores.length > 0) {
                        options.error();

                        bootbox.alert({
                            title: "Tener en cuenta lo siguiente:",
                            message: errores.join("")
                        });

                        return;
                    }

                    console.log('Editando registro');

                    $.ajax({
                        url: "/EmpleadoSemana/Editar",
                        method: "POST",
                        //dataType: "json", // Lo que recibo del servidor
                        contentType: "application/json", // Tipo de datos que envio al servidor
                        data: JSON.stringify(modelo),
                        success: function (respuesta) {
                            console.log('Empleado editado correctamente');
                            options.success();

                            grid.dataSource.read();
                        },
                        error: function (xhr) {
                            options.error();

                            bootbox.alert({
                                title: "Ocurrió un error al editar el registro.",
                                message: ObtenerErrorServidor(xhr.responseText)
                            });
                        }
                    });
                },
                destroy: function (options) {
                    var grid = grdEmpleados.data("kendoGrid");

                    $.ajax({
                        url: "/EmpleadoSemana/Eliminar",
                        method: "POST",
                        //dataType: "json", // Lo que recibo del servidor
                        contentType: "application/json", // Tipo de datos que envio al servidor
                        data: JSON.stringify({
                            id: options.data.Id
                        }),
                        success: function (respuesta) {
                            console.log('Registro eliminado correctamente');
                            options.success();

                            grid.dataSource.read();
                        },
                        error: function (xhr) {
                            options.error();

                            bootbox.alert({
                                title: "Ocurrió un error al eliminar el registro.",
                                message: ObtenerErrorServidor(xhr.responseText)
                            });
                        }
                    });
                }
            },
            pageSize: 10,
            schema: {
                data: 'value',
                model: {
                    id: "Id",
                    fields: {
                        Id: { editable: false, type: "number" },
                        Anio: { editable: true, nullable: false, type: "number" },
                        Semana: { defaultValue: 1, editable: true, nullable: false, type: "number" },
                        CentroId: { editable: true, nullable: false, type: "number" },
                        CentroSiglas: { editable: false, nullable: true, type: "string" },
                        CentroDescripcion: { editable: false, nullable: true, type: "string" },
                        Cantidad: { defaultValue: 1, editable: true, nullable: false, type: "number" },
                        Tubos: { editable: true, nullable: true, type: "number" }
                        //FechaCreado: { editable: false, format: '{0:dd/MMM/yyyy HH:mm tt}', nullable: true, type: 'date' },
                        //FechaModificado: { editable: false, format: '{0:dd/MMM/yyyy HH:mm tt}', nullable: true, type: 'date' }
                    }
                },
                total: 'count'
            }
        });

        return gridDataSource;
    }

    function ValidarModelo(modelo) {
        var mensajes = [];

        if (modelo.Anio === null || (modelo.Anio !== null && modelo.Anio === 0)) {
            mensajes.push("<p>El Año no puede estár vacío.</p>");
        }

        if (modelo.Semana === null || (modelo.Semana !== null && modelo.Semana === 0)) {
            mensajes.push("<p>La Semana no puede estár vacía.</p>");
        }

        if (modelo.CentroId === null || (modelo.CentroId !== null && modelo.CentroId === 0)) {
            mensajes.push("<p>El Centro no puede estár vacío.</p>");
        }

        if (modelo.CantidadEmpleados === null || (modelo.CantidadEmpleados !== null && modelo.CantidadEmpleados === 0)) {
            mensajes.push("<p>La Cantidad no puede estar vacía</p>");
        }

        return mensajes;
    }

    function ConvertirModelo(modelo) {
        var nuevoModelo = {};

        if (modelo.Id !== undefined) {
            nuevoModelo.id = modelo.Id;
        }

        if (modelo.Anio !== undefined) {
            nuevoModelo.Anio = parseInt(modelo.Anio);
        }

        if (modelo.Semana !== undefined) {
            nuevoModelo.Semana = parseInt(modelo.Semana);
        }

        if (modelo.Cantidad !== undefined) {
            nuevoModelo.CantidadEmpleados = parseInt(modelo.Cantidad);
        }

        if (modelo.Tubos !== undefined && modelo.Tubos !== null) {
            nuevoModelo.Tubos = parseInt(modelo.Tubos);
        }

        if (modelo.CentroId !== undefined) {
            nuevoModelo.CentroId = parseInt(modelo.CentroId);
        }

        return nuevoModelo;
    }
});