﻿
var div_contenedor_input = '<div class="k-widget k-autocomplete"></div>';

function ObtenerErrorServidor(responseText) {
    var jsonValue = $(responseText).filter('title').get(0);
    return $(jsonValue).text();
}

function adicionar_meses_a_fecha(fecha, meses) {
    if (meses !== undefined && meses !== 0) {
        fecha.setMonth(fecha.getMonth() + (meses));
    }

    return fecha;
}

function adicionar_meses_a_fecha_inicio_mes(fecha, meses) {
    var fecha1 = adicionar_meses_a_fecha(fecha, meses);

    if (fecha1.getDate() > 1) {
        fecha1.setDate(fecha1.getDate() - (fecha1.getDate() - 1));
    }

    return fecha;
}

function obtener_arreglo_numeros(inicio, fin) {
    var arreglo = [];

    if (fin !== undefined) {
        var aux = inicio;

        while (aux <= fin) {
            arreglo.push(aux);

            aux++;
        }
    }

    return arreglo;
}

/* Creado por: Oscar David Díaz Fortaleché
 * Descripción: Control que se crea para guardar cifras significativas
 */
(function ($) {
    /*$.fn.extend({

    });*/

    $.Editor = function (opciones) {
        var control = $("<input />");

        if (opciones.tipo === undefined) {
            opciones.tipo = $.Editor.opcionesDefecto.tipo;
        }

        if (opciones.atributos.type === undefined) {
            opciones.atributos.type = "text";
        }

        if (opciones.atributos.style === undefined) {
            opciones.atributos.style = "width: 100%;";
        } else if (opciones.atributos.style.indexOf("width") < 0) {
            opciones.atributos.style += "width: 100%;";
        }

        // Añadir los atributos
        if (opciones.atributos !== undefined) {
            control.attr(opciones.atributos);
        }

        control.addClass("k-input");

        switch (opciones.tipo) {
            case "numero":
                control.on("keyup", function (e) {
                    e.preventDefault();

                    control.val(ValidarIngresoNumero(control));
                });

                control.attr("type", "number");
                break;
            case "numero_entero":
                control.on("keyup", function (e) {
                    e.preventDefault();

                    control.val(ValidarIngresoNumeroEntero(control));
                });

                control.attr("type", "number");
                break;
        }

        return control;
    };

    $.Editor.opcionesDefecto = {
        tipo: "texto"
    };

    function ValidarIngresoNumero(control) {
        var textoIngresado = control.val()
            .replace(/\-+/, "-")
            .replace(/\.+/, ".")
            .replace(/[^0-9\.\-]+/, "");

        if (textoIngresado.length > 0) {
            var textoBueno = textoIngresado.match(/^\-?\d+\.?\d*/g);

            if (textoBueno !== null) {
                textoIngresado = textoBueno[0];
            }
        }

        return textoIngresado;
    }

    function ValidarIngresoNumeroEntero(control) {
        var textoIngresado = control.val()
            .replace(/\-+/, "-")
            .replace(/\,+/, "")
            .replace(/\.+/, "")
            .replace(/[^0-9\-]+/, "");

        if (textoIngresado.length > 0) {
            var textoBueno = textoIngresado.match(/^\-?\d+/g);

            if (textoBueno !== null) {
                textoIngresado = textoBueno[0];
            }
        }

        return textoIngresado;
    }
})(jQuery);