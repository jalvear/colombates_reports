﻿$(document).ready(function () {
    /* Variables */

    var grdCorteNominaPlanta = $("#grdCorteNominaPlanta");

    crearGrid_grdCorteNominaPlanta();

    /* Métodos */

    function Asignar_Anio(anio) {
        var grid = grdCorteNominaPlanta.data("kendoGrid");
        var fila = grdCorteNominaPlanta.find("tr.k-grid-edit-row")
            .first();
        var registroEditando = grid.dataItem(fila);

        registroEditando.Anio = anio;
        registroEditando.dirty = true;
    }

    function Asignar_Mes(mes) {
        var grid = grdCorteNominaPlanta.data("kendoGrid");
        var fila = grdCorteNominaPlanta.find("tr.k-grid-edit-row")
            .first();
        var registroEditando = grid.dataItem(fila);

        registroEditando.Mes = mes;
        registroEditando.dirty = true;
    }

    function crearGrid_grdCorteNominaPlanta() {

        grdCorteNominaPlanta.kendoGrid({
            columns: [
                {
                    editor: function (container, options) {
                        var contenedor_input = $(div_contenedor_input);
                        var input = $.Editor({
                            atributos: {
                                id: "txtAnio",
                                min: "1"
                            },
                            tipo: "numero_entero"
                        });

                        var anio = options.model.Anio;

                        if (anio === 0) {
                            anio = new Date().getFullYear();
                        }

                        input.val(anio);
                        Asignar_Anio(anio);

                        contenedor_input.append(input);
                        contenedor_input.appendTo(container);

                        input.change(function () {
                            var valor = input.val();

                            if (valor.replace(/\s+/, "").length === 0) {
                                valor = 0;
                            } else {
                                valor = parseInt(valor);
                            }

                            Asignar_Anio(valor);
                        });
                    },
                    field: "Anio",
                    title: "Año",
                    width: "180px"
                },
                {
                    editor: function (container, options) {
                        var contenedor_input = $(div_contenedor_input);
                        var input = $.Editor({
                            atributos: {
                                id: "txtMes",
                                min: "1",
                                max: "12"
                            },
                            tipo: "numero"
                        });

                        var mes = options.model.Mes;

                        if (mes === 0) {
                            mes = new Date().getMonth() + 1;
                        }

                        input.val(mes);
                        Asignar_Mes(mes);

                        contenedor_input.append(input);
                        contenedor_input.appendTo(container);

                        input.change(function () {
                            var valor = input.val();

                            if (valor.replace(/\s+/, "").length === 0) {
                                valor = 0;
                            } else {
                                valor = parseInt(valor);
                            }

                            Asignar_Mes(valor);
                        });
                    },
                    field: "Mes",
                    title: "Mes",
                    width: "180px"
                },
                {
                    editor: function (container, options) {
                        var contenedor_input = $(div_contenedor_input);
                        var input = $.Editor({
                            atributos: {
                                id: "txtPeriodo",
                                min: "1"
                            },
                            tipo: "numero_entero"
                        });

                        input.val(options.model.Periodo);

                        contenedor_input.append(input);
                        contenedor_input.appendTo(container);

                        input.change(function () {
                            var grid = grdCorteNominaPlanta.data("kendoGrid");
                            var fila = grdCorteNominaPlanta.find("tr.k-grid-edit-row")
                                .first();
                            var registroEditando = grid.dataItem(fila);
                            var valor = input.val();

                            if (valor.replace(/\s+/, "").length === 0) {
                                valor = 0;
                            } else {
                                valor = parseInt(valor);
                            }

                            registroEditando.Periodo = valor;
                            registroEditando.dirty = true;
                        });
                    },
                    field: "Periodo",
                    filterable: false,
                    title: "Periodo",
                    width: "120px"
                },
                {
                    field: "FechaInicio",
                    title: "Fecha Inicio",
                    type: "date",
                    format: "{0:dd/MMM/yyyy}",
                    width: "170px",
                    editor: function (container, options) {
                        $('<input name="FechaInicio" data-bind="value:' + options.field + '" />')
                            .appendTo(container).kendoDatePicker({
                                format: '{0:dd/MMM/yyyy}'
                            });
                    }
                },
                {
                    field: "FechaFin",
                    title: "Fecha Fin",
                    type: "date",
                    format: "{0:dd/MMM/yyyy}",
                    width: "170px",
                    editor: function (container, options) {
                        $('<input name="FechaFin" data-bind="value:' + options.field + '" />')
                            .appendTo(container).kendoDatePicker({
                                format: '{0:dd/MMM/yyyy}'
                            });
                    }
                },
                {
                    command: [{
                        name: "edit",
                        text: { edit: "Modificar", cancel: "Cancelar", update: "Guardar" }
                    },
                    {
                        name: "eliminar",
                        click: function (e) {
                            e.preventDefault();

                            var grid = this;


                            bootbox.confirm({
                                buttons: {
                                    confirm: {
                                        label: 'Si',
                                        className: 'btn-success'
                                    },
                                    cancel: {
                                        label: 'No',
                                        className: 'btn-danger'
                                    }
                                },
                                message: "¿Realmente desea eliminar el registro?",
                                callback: function (result) {
                                    if (result) {
                                        console.log('Eliminando registro');

                                        var tr = $(e.target).closest("tr"); // get the current table row (tr)
                                        grid.removeRow(tr);
                                    }
                                }
                            });
                        },
                        text: "Eliminar"
                    }]
                }],
            dataSource: dataSource_grdCorteNominaPlanta(),
            editable: {
                confirmation: false,
                mode: 'inline'
            },
            filterable: { mode: "row" },
            groupable: false,
            pageable: {
                refresh: true,
                pageSizes: [5, 10, 20]
            },
            resizable: true,
            selectable: "row",
            sortable: true,
            scrollable: true,
            toolbar: [{ name: "create", text: "Crear" }]
        });
    }

    function dataSource_grdCorteNominaPlanta() {
        var gridDataSource = new kendo.data.DataSource({
            serverFiltering: true,
            serverPaging: true,
            serverSorting: true,
            transport: {
                read: function (options) {
                    var filtering = [];
                    var sorting = [];

                    if (options.data.filter !== undefined) {
                        $.each(options.data.filter.filters, function (indice, filtro) {
                            filtering.push({ Field: filtro.field, Value: filtro.value, Operator: filtro.operator });
                        });
                    }

                    if (options.data.sort !== undefined) {
                        $.each(options.data.sort, function (indice, orden) {
                            sorting.push({ Field: orden.field, Dir: orden.dir });
                        });
                    }

                    var take = options.data.take;

                    if (take === undefined) {
                        take = 10;
                    }

                    var datosEnviar = {
                        filter: filtering,
                        sort: sorting,
                        skip: options.data.skip,
                        take: take
                    };

                    // TODO: Agregar espera
                    console.log('Consultando registros');

                    $.ajax({
                        url: "/CorteNominaPlanta/ObtenerCorteNominaPlanta",
                        method: "POST",
                        dataType: "json", // Lo que recibo del servidor
                        contentType: "application/json", // Tipo de datos que envio al servidor 
                        data: JSON.stringify(datosEnviar),
                        complete: function () {
                            // TODO: Terminar espera
                        },
                        success: function (respuesta) {
                            console.log('Registros consultados correctamente');
                            options.success(respuesta);
                        },
                        error: function (xhr) {
                            options.error();

                            bootbox.alert({
                                title: "Ocurrió un error al consultar los registros.",
                                message: ObtenerErrorServidor(xhr.responseText)
                            });
                        }
                    });
                },
                create: function (options) {
                    var grid = grdCorteNominaPlanta.data("kendoGrid");
                    var modelo = options.data;
                    var errores = ValidarModelo(modelo);

                    if (errores.length > 0) {
                        options.error();

                        bootbox.alert({
                            title: "Tener en cuenta lo siguiente:",
                            message: errores.join("")
                        });

                        return;
                    }

                    console.log("Creando registro");

                    $.ajax({
                        url: "/CorteNominaPlanta/Crear",
                        method: "POST",
                        //dataType: "json", // Lo que recibo del servidor
                        contentType: "application/json", // Tipo de datos que envio al servidor
                        data: JSON.stringify(modelo),
                        success: function (respuesta) {
                            console.log('Empleado creado correctamente');
                            options.success();

                            grid.dataSource.read();
                        },
                        error: function (xhr) {
                            options.error();

                            bootbox.alert({
                                title: "Ocurrió un error al crear el registro.",
                                message: ObtenerErrorServidor(xhr.responseText)
                            });
                        }
                    });
                },
                update: function (options) {
                    var grid = grdCorteNominaPlanta.data("kendoGrid");
                    var modelo = options.data;
                    var errores = ValidarModelo(modelo);

                    if (errores.length > 0) {
                        options.error();

                        bootbox.alert({
                            title: "Tener en cuenta lo siguiente:",
                            message: errores.join("")
                        });

                        return;
                    }

                    console.log('Editando registro');

                    $.ajax({
                        url: "/CorteNominaPlanta/Editar",
                        method: "POST",
                        //dataType: "json", // Lo que recibo del servidor
                        contentType: "application/json", // Tipo de datos que envio al servidor
                        data: JSON.stringify(modelo),
                        success: function (respuesta) {
                            console.log('Empleado editado correctamente');
                            options.success();

                            grid.dataSource.read();
                        },
                        error: function (xhr) {
                            options.error();

                            bootbox.alert({
                                title: "Ocurrió un error al editar el registro.",
                                message: ObtenerErrorServidor(xhr.responseText)
                            });
                        }
                    });
                },
                destroy: function (options) {
                    var grid = grdCorteNominaPlanta.data("kendoGrid");

                    $.ajax({
                        url: "/CorteNominaPlanta/Eliminar",
                        method: "POST",
                        //dataType: "json", // Lo que recibo del servidor
                        contentType: "application/json", // Tipo de datos que envio al servidor
                        data: JSON.stringify({
                            id: options.data.Id
                        }),
                        success: function (respuesta) {
                            console.log('Registro eliminado correctamente');
                            options.success();

                            grid.dataSource.read();
                        },
                        error: function (xhr) {
                            options.error();

                            bootbox.alert({
                                title: "Ocurrió un error al eliminar el registro.",
                                message: ObtenerErrorServidor(xhr.responseText)
                            });
                        }
                    });
                }
            },
            pageSize: 10,
            schema: {
                data: 'value',
                model: {
                    id: "Id",
                    fields: {
                        Id: { editable: false, type: "number" },
                        Anio: { editable: true, nullable: false, type: "number" },
                        Mes: { editable: true, nullable: false, type: "number" },
                        Periodo: { defaultValue: 1, editable: true, nullable: false, type: "number" },
                        FechaInicio: { editable: true, format: '{0:dd/MMM/yyyy}', nullable: false, type: 'date' },
                        FechaFin: { editable: true, format: '{0:dd/MMM/yyyy}', nullable: false, type: 'date' }
                        //FechaCreado: { editable: false, format: '{0:dd/MMM/yyyy HH:mm tt}', nullable: true, type: 'date' },
                        //FechaModificado: { editable: false, format: '{0:dd/MMM/yyyy HH:mm tt}', nullable: true, type: 'date' }
                    }
                },
                total: 'count'
            }
        });

        return gridDataSource;
    }

    function ValidarModelo(modelo) {
        var mensajes = [];

        if (modelo.Anio === 0) {
            mensajes.push("<p>El Año no puede estar vacío.</p>");
        } else if (modelo.Anio < 0) {
            mensajes.push("<p>El Año no es válido.</p>");
        }

        if (modelo.Mes === 0) {
            mensajes.push("<p>El Mes no puede estar vacío.</p>");
        } else if (modelo.Mes < 1 || modelo.Mes > 12) {
            mensajes.push("<p>El Mes ingresado no es válido.</p>");
        }

        if (modelo.Periodo < 1) {
            mensajes.push("<p>El Perido ingresado no es válido.</p>");
        }

        if (modelo.FechaInicio === null) {
            mensajes.push("<p>La Fecha de Inicio no puede estar vacía</p>");
        }

        if (modelo.FechaFin === null) {
            mensajes.push("<p>La Fecha de Fin no puede estar vacía</p>");
        }

        return mensajes;
    }
});