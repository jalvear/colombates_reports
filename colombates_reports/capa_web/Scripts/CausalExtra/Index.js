﻿$(document).ready(function () {
    /* Variables */

    var grdCausalesExtra = $("#grdCausalesExtra");

    crearGrid_grdCausalesExtra();

    /* Métodos */

    function crearGrid_grdCausalesExtra() {

        grdCausalesExtra.kendoGrid({
            columns: [
                {
                    editor: function (container, options) {
                        var contenedor_input = $(div_contenedor_input);
                        var input = $('<div id="cmbGrupos" style="width: 100%;"></div>');

                        contenedor_input.append(input);
                        contenedor_input.appendTo(container);

                        input.kendoComboBox({
                            dataSource: {
                                transport: {
                                    read: function (options) {
                                        $.ajax({
                                            url: "/GrupoCausalExtra/ObtenerGruposCausalesExtras",
                                            //data: datosEnviar,
                                            //contentType: "application/json; charset=utf-8",
                                            dataType: "json",
                                            processData: false,
                                            method: "post",
                                            success: function (centros) {
                                                console.log("Se consultaron los Grupos correctamente");
                                                options.success(centros);
                                            },
                                            error: function (xhr) {
                                                options.error();

                                                bootbox.alert({
                                                    title: "No se pudo consultar los Grupos.",
                                                    message: ObtenerErrorServidor(xhr.responseText)
                                                });
                                            }
                                        });
                                    }
                                }
                                //serverFiltering: true
                            },
                            dataTextField: "Descripcion",
                            dataValueField: "Id",
                            delay: 300,
                            filter: 'contains',
                            minLength: 1,
                            placeholder: "Asociar Grupo",
                            select: function (e) {
                                var grid = grdCausalesExtra.data("kendoGrid");
                                var fila = grdCausalesExtra.find("tr.k-grid-edit-row")
                                    .first();
                                var registroEditando = grid.dataItem(fila);
                                var centroSeleccionado = this.dataItem(e.item.index());

                                registroEditando.GrupoId = centroSeleccionado.Id;
                                registroEditando.GrupoDescripcion = centroSeleccionado.Descripcion;
                                registroEditando.dirty = true;
                            },
                            valuePrimitive: true
                        }).data("kendoComboBox");

                        input.data("kendoComboBox").value(options.model.GrupoDescripcion);
                    },
                    field: "GrupoId",
                    filterable: {
                        cell: {
                            operator: "eq",
                            showOperators: false,
                            template: function (args) {
                                args.element.kendoComboBox({
                                    dataSource: {
                                        transport: {
                                            read: function (options_filter) {
                                                $.ajax({
                                                    url: "/GrupoCausalExtra/ObtenerGruposCausalesExtras",
                                                    //data: datosEnviar,
                                                    //contentType: "application/json; charset=utf-8",
                                                    dataType: "json",
                                                    processData: false,
                                                    method: "post",
                                                    success: function (centros) {
                                                        console.log("Se consultaron los Grupos correctamente");
                                                        options_filter.success(centros);
                                                    },
                                                    error: function (xhr) {
                                                        options_filter.error();

                                                        bootbox.alert({
                                                            title: "No se pudo consultar los Grupos.",
                                                            message: ObtenerErrorServidor(xhr.responseText)
                                                        });
                                                    }
                                                });
                                            }
                                        }
                                        //serverFiltering: true
                                    },
                                    dataTextField: "Descripcion",
                                    dataValueField: "Id",
                                    delay: 300,
                                    filter: 'contains',
                                    minLength: 1,
                                    //placeholder: "Filtrar por Centro",
                                    valuePrimitive: true
                                });
                            }
                        }
                    },
                    template: "#= GrupoDescripcion #",
                    title: "Grupo",
                    width: "180px"
                },
                {
                    editor: function (container, options) {
                        var contenedor_input = $(div_contenedor_input);
                        var input = $.Editor({
                            atributos: {
                                id: "txtDescripcion",
                                maxlength: "100"
                            },
                            tipo: "texto"
                        });

                        if (options.model.CausalDescripcion !== null) {
                            input.val(options.model.CausalDescripcion);
                        }

                        contenedor_input.append(input);
                        contenedor_input.appendTo(container);

                        input.change(function () {
                            options.model.CausalDescripcion = input.val();
                            options.model.dirty = true;
                        });
                    },
                    field: "CausalDescripcion",
                    title: "Descripción",
                    width: "300px"
                },
                {
                    command: [{
                        name: "edit",
                        text: { edit: "Modificar", cancel: "Cancelar", update: "Guardar" }
                    },
                    {
                        name: "eliminar",
                        click: function (e) {
                            e.preventDefault();

                            var grid = this;

                            bootbox.confirm({
                                buttons: {
                                    confirm: {
                                        label: 'Si',
                                        className: 'btn-success'
                                    },
                                    cancel: {
                                        label: 'No',
                                        className: 'btn-danger'
                                    }
                                },
                                message: "¿Realmente desea eliminar el Causal de Extra?",
                                callback: function (result) {
                                    if (result) {
                                        console.log('Eliminando Causal de Extra');

                                        var tr = $(e.target).closest("tr"); // get the current table row (tr)
                                        grid.removeRow(tr);
                                    }
                                }
                            });
                        },
                        text: "Eliminar"
                    }]
                }
            ],
            dataSource: dataSource_grdCausalesExtra(),
            editable: {
                confirmation: false,
                mode: 'inline'
            },
            filterable: {
                mode: "row"
            },
            groupable: false,
            pageable: {
                refresh: true,
                pageSizes: [5, 10, 20]
            },
            resizable: true,
            selectable: "row",
            sortable: true,
            scrollable: true,
            toolbar: [{ name: "create", text: "Crear" }]
        });
    }

    function dataSource_grdCausalesExtra() {
        var gridDataSource = new kendo.data.DataSource({
            serverFiltering: true,
            serverPaging: true,
            serverSorting: true,
            transport: {
                read: function (options) {
                    var filtering = [];
                    var sorting = [];

                    if (options.data.filter !== undefined) {
                        $.each(options.data.filter.filters, function (indice, filtro) {
                            filtering.push({ Field: filtro.field, Value: filtro.value, Operator: filtro.operator });
                        });
                    }

                    if (options.data.sort !== undefined) {
                        $.each(options.data.sort, function (indice, orden) {
                            sorting.push({ Field: orden.field, Dir: orden.dir });
                        });
                    }

                    var take = options.data.take;

                    if (take === undefined) {
                        take = 5;
                    }

                    var datosEnviar = {
                        filter: filtering,
                        sort: sorting,
                        skip: options.data.skip,
                        take: take
                    };

                    // TODO: Agregar espera
                    console.log('Consultando Causales de Extra');

                    $.ajax({
                        url: "/CausalExtra/ObtenerCausalesExtras",
                        method: "POST",
                        dataType: "json", // Lo que recibo del servidor
                        contentType: "application/json", // Tipo de datos que envio al servidor 
                        data: JSON.stringify(datosEnviar),
                        complete: function () {
                            // TODO: Terminar espera
                        },
                        success: function (respuesta) {
                            console.log('Causales de Extra consultados correctamente');
                            options.success(respuesta);
                        },
                        error: function (xhr) {
                            options.error();

                            bootbox.alert({
                                title: "Ocurrió un error al consultar los Causales de Extra.",
                                message: ObtenerErrorServidor(xhr.responseText)
                            });
                        }
                    });
                },
                create: function (options) {
                    var grid = grdCausalesExtra.data("kendoGrid");
                    var modelo = ConvertirModelo(options.data);
                    var errores = ValidarModelo(modelo);

                    if (errores.length > 0) {
                        options.error();

                        bootbox.alert({
                            title: "Tener en cuenta lo siguiente:",
                            message: errores.join("")
                        });

                        return;
                    }

                    console.log("Creando Causal de Extra");

                    $.ajax({
                        url: "/CausalExtra/Crear",
                        method: "POST",
                        //dataType: "json", // Lo que recibo del servidor
                        contentType: "application/json", // Tipo de datos que envio al servidor
                        data: JSON.stringify(modelo),
                        success: function (respuesta) {
                            console.log('Causal de Extra creado correctamente');
                            options.success();

                            //$("#btnGuardar, #btnCancelar").fadeOut(250, function () {
                            //    $("#btnCrear, #btnEditar, #btnEliminar").fadeIn();
                            //});

                            grid.dataSource.read();
                        },
                        error: function (xhr) {
                            options.error();

                            bootbox.alert({
                                title: "Ocurrió un error al crear el Causal de Extra.",
                                message: ObtenerErrorServidor(xhr.responseText)
                            });
                        }
                    });
                },
                update: function (options) {
                    var grid = grdCausalesExtra.data("kendoGrid");
                    var modelo = ConvertirModelo(options.data);
                    var errores = ValidarModelo(modelo);

                    if (errores.length > 0) {
                        options.error();

                        bootbox.alert({
                            title: "Tener en cuenta lo siguiente:",
                            message: errores.join("")
                        });

                        return;
                    }

                    console.log('Editando Causal de Extra');

                    $.ajax({
                        url: "/CausalExtra/Editar",
                        method: "POST",
                        //dataType: "json", // Lo que recibo del servidor
                        contentType: "application/json", // Tipo de datos que envio al servidor
                        data: JSON.stringify(modelo),
                        success: function (respuesta) {
                            console.log('Causal de Extra editado correctamente');
                            options.success();

                            //$("#btnGuardar, #btnCancelar").fadeOut(250, function () {
                            //    $("#btnCrear, #btnEditar, #btnEliminar").fadeIn();
                            //});

                            grid.dataSource.read();
                        },
                        error: function (xhr) {
                            options.error();

                            bootbox.alert({
                                title: "Ocurrió un error al editar el Causal de Extra.",
                                message: ObtenerErrorServidor(xhr.responseText)
                            });
                        }
                    });
                },
                destroy: function (options) {
                    var grid = grdCausalesExtra.data("kendoGrid");

                    $.ajax({
                        url: "/CausalExtra/Eliminar",
                        method: "POST",
                        //dataType: "json", // Lo que recibo del servidor
                        contentType: "application/json", // Tipo de datos que envio al servidor
                        data: JSON.stringify({
                            id: options.data.CausalId
                        }),
                        success: function (respuesta) {
                            console.log('Causal de Extra eliminado correctamente');
                            options.success();

                            //$("#btnGuardar, #btnCancelar").fadeOut(250, function () {
                            //    $("#btnCrear, #btnEditar, #btnEliminar").fadeIn();
                            //});

                            grid.dataSource.read();
                        },
                        error: function (xhr) {
                            options.error();

                            bootbox.alert({
                                title: "Ocurrió un error al crear el Causal de Extra.",
                                message: ObtenerErrorServidor(xhr.responseText)
                            });
                        }
                    });
                }
            },
            pageSize: 5,
            schema: {
                data: 'value',
                model: {
                    id: "CausalId",
                    fields: {
                        CausalId: { editable: false, type: "number" },
                        CausalDescripcion: { editable: true, nullable: false, type: "string" },
                        GrupoId: { editable: true, nullable: false, type: "number" },
                        GrupoDescripcion: { editable: true, nullable: true, type: "string" }
                        /*FechaCreado: { editable: false, format: '{0:dd/MMM/yyyy HH:mm tt}', nullable: true, type: 'date' },
                        FechaModificado: { editable: false, format: '{0:dd/MMM/yyyy HH:mm tt}', nullable: true, type: 'date' }*/
                    }
                },
                total: 'count'
            }
        });

        return gridDataSource;
    }

    function ValidarModelo(modelo) {
        var mensajes = [];

        if (modelo.Descripcion === null || modelo.Descripcion.replace(/\s+/, "").length === 0) {
            mensajes.push("<p>La descripción no puede estar vacía.</p>");
        } else if (modelo.Descripcion.length > 100) {
            mensajes.push("<p>La descripción puede contener hasta 100 caracteres</p>");
        }

        if (modelo.GrupoId === 0) {
            mensajes.push("<p>El Grupo no puede estar vacío.</p>");
        }

        return mensajes;
    }

    function ConvertirModelo(modelo) {
        var nuevoModelo = {
            GrupoId: 0
        };

        if (modelo.CausalId !== undefined) {
            nuevoModelo.Id = modelo.CausalId;
        }

        if (modelo.CausalDescripcion !== undefined) {
            nuevoModelo.Descripcion = modelo.CausalDescripcion;
        }

        if (modelo.GrupoId !== undefined) {
            nuevoModelo.GrupoId = modelo.GrupoId;
        }

        return nuevoModelo;
    }
});