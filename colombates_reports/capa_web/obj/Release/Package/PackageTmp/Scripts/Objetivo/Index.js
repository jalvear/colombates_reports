﻿$(document).ready(function () {
    /* Variables */

    var grdObjetivos = $("#grdObjetivos");

    crearGrid_grdObjetivos();



    /* Métodos */

    function crearGrid_grdObjetivos() {

        grdObjetivos.kendoGrid({
            columns: [
                {
                    editor: function (container, options) {
                        var contenedor_input = $(div_contenedor_input);
                        var input = $('<div id="cmbReportes" style="width: 100%;"></div>');

                        contenedor_input.append(input);
                        contenedor_input.appendTo(container);

                        input.kendoComboBox({
                            dataSource: {
                                transport: {
                                    read: function (options) {
                                        $.ajax({
                                            url: "/Reporte/ObtenerNombresReportes",
                                            //data: datosEnviar,
                                            //contentType: "application/json; charset=utf-8",
                                            dataType: "json",
                                            processData: false,
                                            method: "post",
                                            success: function (centros) {
                                                console.log("Se consultaron los Reportes correctamente");
                                                options.success(centros);
                                            },
                                            error: function (xhr) {
                                                options.error();

                                                bootbox.alert({
                                                    title: "No se pudo consultar los Reportes.",
                                                    message: ObtenerErrorServidor(xhr.responseText)
                                                });
                                            }
                                        });
                                    }
                                }
                                //serverFiltering: true
                            },
                            dataTextField: "Nombre",
                            dataValueField: "Id",
                            delay: 300,
                            filter: 'contains',
                            minLength: 1,
                            placeholder: "Asociar Reporte",
                            select: function (e) {
                                var grid = grdObjetivos.data("kendoGrid");
                                var fila = grdObjetivos.find("tr.k-grid-edit-row")
                                    .first();
                                var registroEditando = grid.dataItem(fila);
                                var centroSeleccionado = this.dataItem(e.item.index());

                                registroEditando.ReporteId = centroSeleccionado.Id;
                                registroEditando.ReporteNombre = centroSeleccionado.Nombre;
                                registroEditando.dirty = true;
                            },
                            valuePrimitive: true
                        }).data("kendoComboBox");

                        input.data("kendoComboBox").value(options.model.ReporteNombre);
                    },
                    field: "ReporteId",
                    filterable: {
                        cell: {
                            operator: "eq",
                            showOperators: false,
                            template: function (args) {
                                args.element.kendoComboBox({
                                    dataSource: {
                                        transport: {
                                            read: function (options_filter) {
                                                $.ajax({
                                                    url: "/Reporte/ObtenerNombresReportes",
                                                    //data: datosEnviar,
                                                    //contentType: "application/json; charset=utf-8",
                                                    dataType: "json",
                                                    processData: false,
                                                    method: "post",
                                                    success: function (centros) {
                                                        console.log("Se consultaron los Reportes correctamente");
                                                        options_filter.success(centros);
                                                    },
                                                    error: function (xhr) {
                                                        options_filter.error();

                                                        bootbox.alert({
                                                            title: "No se pudo consultar los Reportes.",
                                                            message: ObtenerErrorServidor(xhr.responseText)
                                                        });
                                                    }
                                                });
                                            }
                                        }
                                        //serverFiltering: true
                                    },
                                    dataTextField: "Nombre",
                                    dataValueField: "Id",
                                    delay: 300,
                                    filter: 'contains',
                                    minLength: 1,
                                    //placeholder: "Filtrar por Reporte",
                                    valuePrimitive: true
                                });
                            }
                        }
                    },
                    template: "#= ReporteNombre #",
                    title: "Reporte",
                    width: "260px"
                },
                {
                    editor: function (container, options) {
                        var contenedor_input = $(div_contenedor_input);
                        var input = $.Editor({
                            atributos: {
                                id: "txtObjetivoNombre",
                                maxlength: "120"
                            },
                            tipo: "texto"
                        });

                        if (options.model.ObjetivoNombre !== null) {
                            input.val(options.model.ObjetivoNombre);
                        }

                        contenedor_input.append(input);
                        contenedor_input.appendTo(container);

                        input.change(function () {
                            options.model.ObjetivoNombre = input.val();
                            options.model.dirty = true;
                        });
                    },
                    field: "ObjetivoNombre",
                    filterable: {
                        cell: {
                            operator: "contains",
                            showOperators: false
                        }
                    },
                    title: "Nombre",
                    width: "200px"
                },
                {
                    editor: function (container, options) {
                        var contenedor_input = $(div_contenedor_input);
                        var input = $.Editor({
                            atributos: {
                                id: "txtValor"
                            },
                            tipo: "numero"
                        });

                        if (options.model.ObjetivoValor !== null) {
                            input.val(options.model.ObjetivoValor);
                        }

                        contenedor_input.append(input);
                        contenedor_input.appendTo(container);

                        input.change(function () {
                            var valor = input.val();

                            if (valor.replace(/\s+/, "").length === 0) {
                                valor = 0;
                            }

                            options.model.ObjetivoValor = valor;
                            options.model.dirty = true;
                        });
                    },
                    field: "ObjetivoValor",
                    filterable: false,
                    title: "Valor",
                    width: "160px"
                },
                {
                    command: [{
                        name: "edit",
                        text: { edit: "Modificar", cancel: "Cancelar", update: "Guardar" }
                    },
                    {
                        name: "eliminar",
                        click: function (e) {
                            e.preventDefault();

                            var grid = this;


                            bootbox.confirm({
                                buttons: {
                                    confirm: {
                                        label: 'Si',
                                        className: 'btn-success'
                                    },
                                    cancel: {
                                        label: 'No',
                                        className: 'btn-danger'
                                    }
                                },
                                message: "¿Realmente desea eliminar la Objetivo?",
                                callback: function (result) {
                                    if (result) {
                                        console.log('Eliminando Objetivo');

                                        var tr = $(e.target).closest("tr"); // get the current table row (tr)
                                        grid.removeRow(tr);
                                    }
                                }
                            });
                        },
                        text: "Eliminar"
                    }]
                }],
            dataSource: dataSource_grdObjetivos(),
            editable: {
                confirmation: false,
                mode: 'inline'
            },
            filterable: { mode: "row" },
            groupable: false,
            pageable: {
                refresh: true,
                pageSizes: [5, 10, 20]
            },
            resizable: true,
            selectable: "row",
            sortable: true,
            scrollable: true,
            toolbar: [{ name: "create", text: "Crear" }]
        });
    }

    function dataSource_grdObjetivos() {
        var gridDataSource = new kendo.data.DataSource({
            serverFiltering: true,
            serverPaging: true,
            serverSorting: true,
            transport: {
                read: function (options) {
                    var filtering = [];
                    var sorting = [];

                    if (options.data.filter !== undefined) {
                        $.each(options.data.filter.filters, function (indice, filtro) {
                            filtering.push({ Field: filtro.field, Value: filtro.value, Operator: filtro.operator });
                        });
                    }

                    if (options.data.sort !== undefined) {
                        $.each(options.data.sort, function (indice, orden) {
                            sorting.push({ Field: orden.field, Dir: orden.dir });
                        });
                    }

                    var take = options.data.take;

                    if (take === undefined) {
                        take = 10;
                    }

                    var datosEnviar = {
                        filter: filtering,
                        sort: sorting,
                        skip: options.data.skip,
                        take: take
                    };


                    // TODO: Agregar espera
                    console.log('Consultando Objetivos');

                    $.ajax({
                        url: "/Objetivo/ObtenerObjetivos",
                        method: "POST",
                        dataType: "json", // Lo que recibo del servidor
                        contentType: "application/json", // Tipo de datos que envio al servidor 
                        data: JSON.stringify(datosEnviar),
                        complete: function () {
                            // TODO: Terminar espera
                        },
                        success: function (respuesta) {
                            console.log('Objetivos consultadas correctamente');
                            options.success(respuesta);
                        },
                        error: function (xhr) {
                            options.error();

                            bootbox.alert({
                                title: "Ocurrió un error al consultar los Objetivos.",
                                message: ObtenerErrorServidor(xhr.responseText)
                            });
                        }
                    });
                },
                create: function (options) {
                    var grid = grdObjetivos.data("kendoGrid");
                    var modelo = ConvertirModelo(options.data);
                    var errores = ValidarModelo(modelo);

                    if (errores.length > 0) {
                        options.error();

                        bootbox.alert({
                            title: "Tener en cuenta lo siguiente:",
                            message: errores.join("")
                        });

                        return;
                    }

                    console.log("Creando Objetivo");

                    $.ajax({
                        url: "/Objetivo/Crear",
                        method: "POST",
                        //dataType: "json", // Lo que recibo del servidor
                        contentType: "application/json", // Tipo de datos que envio al servidor
                        data: JSON.stringify(modelo),
                        success: function (respuesta) {
                            console.log('Objetivo creado correctamente');

                            options.success();

                            grid.dataSource.read();
                        },
                        error: function (xhr) {
                            options.error();

                            bootbox.alert({
                                title: "Ocurrió un error al crear el Objetivo.",
                                message: ObtenerErrorServidor(xhr.responseText)
                            });
                        }
                    });
                },
                update: function (options) {
                    var grid = grdObjetivos.data("kendoGrid");
                    var modelo = ConvertirModelo(options.data);
                    var errores = ValidarModelo(modelo);

                    if (errores.length > 0) {
                        options.error();

                        bootbox.alert({
                            title: "Tener en cuenta lo siguiente:",
                            message: errores.join("")
                        });

                        return;
                    }

                    console.log('Editando Objetivo');

                    $.ajax({
                        url: "/Objetivo/Editar",
                        method: "POST",
                        //dataType: "json", // Lo que recibo del servidor
                        contentType: "application/json", // Tipo de datos que envio al servidor
                        data: JSON.stringify(modelo),
                        success: function (respuesta) {
                            console.log('Objetivo editado correctamente');

                            options.success();

                            grid.dataSource.read();
                        },
                        error: function (xhr) {
                            options.error();

                            bootbox.alert({
                                title: "Ocurrió un error al editar el Objetivo.",
                                message: ObtenerErrorServidor(xhr.responseText)
                            });
                        }
                    });
                },
                destroy: function (options) {
                    var grid = grdObjetivos.data("kendoGrid");

                    $.ajax({
                        url: "/Objetivo/Eliminar",
                        method: "POST",
                        //dataType: "json", // Lo que recibo del servidor
                        contentType: "application/json", // Tipo de datos que envio al servidor
                        data: JSON.stringify({
                            id: options.data.ObjetivoId
                        }),
                        success: function (respuesta) {
                            console.log('Objetivo eliminado correctamente');

                            options.success();

                            grid.dataSource.read();
                        },
                        error: function (xhr) {
                            options.error();

                            bootbox.alert({
                                title: "Ocurrió un error al eliminar el Objetivo.",
                                message: ObtenerErrorServidor(xhr.responseText)
                            });
                        }
                    });
                }
            },
            pageSize: 10,
            schema: {
                data: 'value',
                model: {
                    id: "ObjetivoId",
                    fields: {
                        ObjetivoId: { editable: false, type: "number" },
                        ObjetivoNombre: { editable: true, nullable: false, type: "string" },
                        ObjetivoValor: { editable: true, nullable: false, type: "number" },
                        ReporteId: { editable: true, nullable: false, type: "number" },
                        ReporteNombre: { editable: true, nullable: true, type: "string" }
                        //FechaCreado: { editable: false, format: '{0:dd/MMM/yyyy HH:mm tt}', nullable: true, type: 'date' },
                        //FechaModificado: { editable: false, format: '{0:dd/MMM/yyyy HH:mm tt}', nullable: true, type: 'date' }
                    }
                },
                total: 'count'
            }
        });

        return gridDataSource;
    }

    function ValidarModelo(modelo) {
        var mensajes = [];

        if (modelo.ReporteId === null || (modelo.ReporteId !== null && modelo.ReporteId === 0)) {
            mensajes.push("<p>El Reporte no puede estár vacío.</p>");
        }

        if (modelo.Nombre === null || (modelo.Nombre !== null && modelo.Nombre.replace(/\s+/, "").length === 0)) {
            mensajes.push("<p>El Nombre no puede estar vacía</p>");
        } else if (modelo.Nombre.length > 120) {
            mensajes.push("<p>El Nombre puede contener hasta 120 caracteres</p>");
        }

        /*if (modelo.Valor === null) {
            mensajes.push("<p>El Valor no puede estar vacío</p>");
        } else if (modelo.Valor.length > 10) {
            mensajes.push("<p>El Valor puede contener hasta 10 caracteres</p>");
        }
        */

        return mensajes;
    }

    function ConvertirModelo(modelo) {
        var nuevoModelo = {};

        if (modelo.ObjetivoId !== undefined) {
            nuevoModelo.Id = modelo.ObjetivoId;
        }

        if (modelo.ReporteId !== undefined) {
            nuevoModelo.ReporteId = modelo.ReporteId;
        }

        if (modelo.ObjetivoNombre !== undefined) {
            nuevoModelo.Nombre = modelo.ObjetivoNombre;
        }

        if (modelo.ObjetivoValor !== undefined) {
            nuevoModelo.Valor = parseFloat(modelo.ObjetivoValor);
        }

        return nuevoModelo;
    }
});