﻿$(document).ready(function () {
    /* Variables */

    var grdEmpleados = $("#grdEmpleados");

    crearGrid_grdEmpleados();



    /* Métodos */

    function crearGrid_grdEmpleados() {

        grdEmpleados.kendoGrid({
            columns: [{
                editor: function (container, options) {
                    var contenedor_input = $(div_contenedor_input);
                    var input = $.Editor({
                        atributos: {
                            id: "txtFicha",
                            maxlength: "10"
                        },
                        tipo: "texto"
                    });

                    if (options.model.EmpleadoFicha != null) {
                        input.val(options.model.EmpleadoFicha);
                    }

                    contenedor_input.append(input);
                    contenedor_input.appendTo(container);

                    input.change(function () {
                        options.model.EmpleadoFicha = input.val();
                        options.model.dirty = true;
                    });
                },
                field: "EmpleadoFicha",
                filterable: {
                    cell: {
                        operator: "contains",
                        showOperators: false,
                    }
                },
                title: "Ficha",
                width: "120px"
            },
            {
                editor: function (container, options) {
                    var contenedor_input = $(div_contenedor_input);
                    var input = $.Editor({
                        atributos: {
                            id: "txtNombreCompleto",
                            maxlength: "300"
                        },
                        tipo: "texto"
                    });

                    if (options.model.EmpleadoNombreCompleto != null) {
                        input.val(options.model.EmpleadoNombreCompleto);
                    }

                    contenedor_input.append(input);
                    contenedor_input.appendTo(container);

                    input.change(function () {
                        options.model.EmpleadoNombreCompleto = input.val()
                        options.model.dirty = true;
                    });
                },
                field: "EmpleadoNombreCompleto",
                filterable: {
                    cell: {
                        operator: "contains",
                        showOperators: false,
                    }
                },
                title: "Nombre Completo",
                width: "300px"
            },
            {
                editor: function (container, options) {
                    var contenedor_input = $(div_contenedor_input);
                    var input = $('<div name="cmbCentros" style="width: 100%;"></div>');

                    contenedor_input.append(input);
                    contenedor_input.appendTo(container);

                    input.kendoComboBox({
                        dataSource: {
                            transport: {
                                read: function (options) {
                                    /*var consultar = "";

                                    if (options.data.filter != undefined && options.data.filter.filters.length > 0) {
                                        consultar = options.data.filter.filters[0].value;
                                    }

                                    var datosEnviar = JSON.stringify({
                                        estado: "A",
                                        filtro: consultar,
                                        codigosOmitir: cmbLaboratorioComparte.value()
                                    });
                                    */
                                    $.ajax({
                                        url: "/Centro/ObtenerCentros",
                                        //data: datosEnviar,
                                        //contentType: "application/json; charset=utf-8",
                                        dataType: "json",
                                        processData: false,
                                        method: "post",
                                        success: function (centros) {
                                            console.log("Se consultaron los Centros correctamente");
                                            options.success(centros);
                                        },
                                        error: function (xhr) {
                                            options.error();

                                            bootbox.alert({
                                                title: "No se pudo consultar los Centros.",
                                                message: ObtenerErrorServidor(xhr.responseText)
                                            });
                                        }
                                    });
                                }
                            },
                            //serverFiltering: true
                        },
                        dataTextField: "Descripcion_Siglas",
                        dataValueField: "Id",
                        delay: 300,
                        filter: 'contains',
                        minLength: 1,
                        placeholder: "Asociar Centro",
                        select: function (e) {
                            var grid = grdEmpleados.data("kendoGrid");
                            var fila = grdEmpleados.find("tr.k-grid-edit-row")
                                .first();
                            var registroEditando = grid.dataItem(fila);
                            var centroSeleccionado = this.dataItem(e.item.index());

                            registroEditando.CentroId = centroSeleccionado.Id;
                            registroEditando.CentroSiglas = centroSeleccionado.Siglas;
                            registroEditando.CentroDescripcion = centroSeleccionado.Descripcion;
                            registroEditando.dirty = true;
                        },
                        valuePrimitive: true
                    }).data("kendoComboBox");

                    input.data("kendoComboBox").value(options.model.CentroSiglas);
                },
                field: "CentroId",
                filterable: {
                    cell: {
                        operator: "eq",
                        showOperators: false,
                        template: function (args) {
                            args.element.kendoComboBox({
                                dataSource: {
                                    transport: {
                                        read: function (options_filter) {
                                            /*var consultar = "";
        
                                            if (options.data.filter != undefined && options.data.filter.filters.length > 0) {
                                                consultar = options.data.filter.filters[0].value;
                                            }
        
                                            var datosEnviar = JSON.stringify({
                                                estado: "A",
                                                filtro: consultar,
                                                codigosOmitir: cmbLaboratorioComparte.value()
                                            });
                                            */
                                            $.ajax({
                                                url: "/Centro/ObtenerCentros",
                                                //data: datosEnviar,
                                                //contentType: "application/json; charset=utf-8",
                                                dataType: "json",
                                                processData: false,
                                                method: "post",
                                                success: function (centros) {
                                                    console.log("Se consultaron los Centros correctamente");
                                                    options_filter.success(centros);
                                                },
                                                error: function (xhr) {
                                                    options_filter.error();

                                                    bootbox.alert({
                                                        title: "No se pudo consultar los Centros.",
                                                        message: ObtenerErrorServidor(xhr.responseText)
                                                    });
                                                }
                                            });
                                        }
                                    },
                                    //serverFiltering: true
                                },
                                dataTextField: "Descripcion_Siglas",
                                dataValueField: "Id",
                                delay: 300,
                                filter: 'contains',
                                minLength: 1,
                                //placeholder: "Filtrar por Centro",
                                valuePrimitive: true
                            });
                        }
                    }
                },
                template: "#= CentroSiglas #",
                title: "Centro",
                width: "180px"
            },
            {
                command: [{
                    name: "edit",
                    text: { edit: "Modificar", cancel: "Cancelar", update: "Guardar" }
                },
                {
                    name: "eliminar",
                    click: function (e) {
                        e.preventDefault();

                        var grid = this;


                        bootbox.confirm({
                            buttons: {
                                confirm: {
                                    label: 'Si',
                                    className: 'btn-success'
                                },
                                cancel: {
                                    label: 'No',
                                    className: 'btn-danger'
                                }
                            },
                            message: "¿Realmente desea eliminar el Empleado?",
                            callback: function (result) {
                                if (result) {
                                    console.log('Eliminando Empleado');

                                    var tr = $(e.target).closest("tr"); // get the current table row (tr)
                                    grid.removeRow(tr);
                                }
                            }
                        });
                    },
                    text: "Eliminar"
                }]
            }],
            dataSource: dataSource_grdEmpleados(),
            editable: {
                confirmation: false,
                mode: 'inline'
            },
            filterable: { mode: "row" },
            groupable: false,
            pageable: {
                refresh: true,
                pageSizes: [5, 10, 20],
            },
            resizable: true,
            selectable: "row",
            sortable: true,
            scrollable: true,
            toolbar: [{ name: "create", text: "Crear" }]
        });
    }

    function dataSource_grdEmpleados() {
        var gridDataSource = new kendo.data.DataSource({
            serverFiltering: true,
            serverPaging: true,
            serverSorting: true,
            transport: {
                read: function (options) {
                    var filtering = [];
                    var sorting = [];

                    if (options.data.filter != undefined) {
                        $.each(options.data.filter.filters, function (indice, filtro) {
                            filtering.push({ Field: filtro.field, Value: filtro.value, Operator: filtro.operator });
                        });
                    }

                    if (options.data.sort != undefined) {
                        $.each(options.data.sort, function (indice, orden) {
                            sorting.push({ Field: orden.field, Dir: orden.dir });
                        });
                    }

                    var take = options.data.take;

                    if (take == undefined) {
                        take = 10;
                    }

                    var datosEnviar = {
                        filter: filtering,
                        sort: sorting,
                        skip: options.data.skip,
                        take: take
                    };


                    // TODO: Agregar espera
                    console.log('Consultando Empleados');

                    $.ajax({
                        url: "/Empleado/ObtenerEmpleados",
                        method: "POST",
                        dataType: "json", // Lo que recibo del servidor
                        contentType: "application/json", // Tipo de datos que envio al servidor 
                        data: JSON.stringify(datosEnviar),
                        complete: function () {
                            // TODO: Terminar espera
                        },
                        success: function (respuesta) {
                            console.log('Empleados consultados correctamente');
                            options.success(respuesta);
                        },
                        error: function (xhr) {
                            options.error();

                            bootbox.alert({
                                title: "Ocurrió un error al consultar los Empleados.",
                                message: ObtenerErrorServidor(xhr.responseText)
                            });
                        }
                    });
                },
                create: function (options) {
                    var grid = grdEmpleados.data("kendoGrid");
                    var modelo = ConvertirModelo(options.data);
                    var errores = ValidarModelo(modelo);

                    if (errores.length > 0) {
                        options.error();

                        bootbox.alert({
                            title: "Tener en cuenta lo siguiente:",
                            message: errores.join("")
                        });

                        return;
                    }

                    console.log("Creando centro");

                    $.ajax({
                        url: "/Empleado/Crear",
                        method: "POST",
                        //dataType: "json", // Lo que recibo del servidor
                        contentType: "application/json", // Tipo de datos que envio al servidor
                        data: JSON.stringify(modelo),
                        success: function (respuesta) {
                            console.log('Empleado creado correctamente');
                            options.success();

                            //$("#btnGuardar, #btnCancelar").fadeOut(250, function () {
                            //    $("#btnCrear, #btnEditar, #btnEliminar").fadeIn();
                            //});

                            grid.dataSource.read();
                        },
                        error: function (xhr) {
                            options.error();

                            bootbox.alert({
                                title: "Ocurrió un error al crear el Empleado.",
                                message: ObtenerErrorServidor(xhr.responseText)
                            });
                        }
                    });
                },
                update: function (options) {
                    var grid = grdEmpleados.data("kendoGrid");
                    var modelo = ConvertirModelo(options.data);
                    var errores = ValidarModelo(modelo);

                    if (errores.length > 0) {
                        options.error();

                        bootbox.alert({
                            title: "Tener en cuenta lo siguiente:",
                            message: errores.join("")
                        });

                        return;
                    }

                    console.log('Editando Empleado');

                    $.ajax({
                        url: "/Empleado/Editar",
                        method: "POST",
                        //dataType: "json", // Lo que recibo del servidor
                        contentType: "application/json", // Tipo de datos que envio al servidor
                        data: JSON.stringify(modelo),
                        success: function (respuesta) {
                            console.log('Empleado editado correctamente');
                            options.success();

                            //$("#btnGuardar, #btnCancelar").fadeOut(250, function () {
                            //    $("#btnCrear, #btnEditar, #btnEliminar").fadeIn();
                            //});

                            grid.dataSource.read();
                        },
                        error: function (xhr) {
                            options.error();

                            bootbox.alert({
                                title: "Ocurrió un error al crear el Empleado.",
                                message: ObtenerErrorServidor(xhr.responseText)
                            });
                        }
                    });
                },
                destroy: function (options) {
                    var grid = grdEmpleados.data("kendoGrid");

                    $.ajax({
                        url: "/Empleado/Eliminar",
                        method: "POST",
                        //dataType: "json", // Lo que recibo del servidor
                        contentType: "application/json", // Tipo de datos que envio al servidor
                        data: JSON.stringify({
                            id: options.data.EmpleadoId
                        }),
                        success: function (respuesta) {
                            console.log('Empleado eliminado correctamente');
                            options.success();

                            //$("#btnGuardar, #btnCancelar").fadeOut(250, function () {
                            //    $("#btnCrear, #btnEditar, #btnEliminar").fadeIn();
                            //});

                            grid.dataSource.read();
                        },
                        error: function (xhr) {
                            options.error();

                            bootbox.alert({
                                title: "Ocurrió un error al crear el Empleado.",
                                message: ObtenerErrorServidor(xhr.responseText)
                            });
                        }
                    });
                }
            },
            pageSize: 10,
            schema: {
                data: 'value',
                model: {
                    id: "EmpleadoId",
                    fields: {
                        EmpleadoId: { editable: false, type: "number" },
                        EmpleadoFicha: { editable: true, nullable: false, type: "string" },
                        EmpleadoNombreCompleto: { editable: true, nullable: false, type: "string" },
                        CentroId: { editable: true, nullable: false, type: "number" },
                        CentroSiglas: { editable: true, nullable: false, type: "string" },
                        CentroDescripcion: { editable: true, nullable: false, type: "string" },
                        //FechaCreado: { editable: false, format: '{0:dd/MMM/yyyy HH:mm tt}', nullable: true, type: 'date' },
                        //FechaModificado: { editable: false, format: '{0:dd/MMM/yyyy HH:mm tt}', nullable: true, type: 'date' }
                    }
                },
                total: 'count',
            }
        });

        return gridDataSource;
    }

    function ValidarModelo(modelo) {
        var mensajes = [];

        if (modelo.CentroId == null || (modelo.CentroId != null && modelo.CentroId == 0)) {
            mensajes.push("<p>El Centro no puese estár vacío.</p>");
        }

        if (modelo.Ficha == null || (modelo.Ficha != null && modelo.Ficha.replace(/\s+/, "").length == 0)) {
            mensajes.push("<p>La Ficha no puede estar vacía</p>");
        } else if (modelo.Ficha.length > 10) {
            mensajes.push("<p>La Ficha puede contener hasta 10 caracteres</p>");
        }

        if (modelo.NombreCompleto == null || (modelo.NombreCompleto != null && modelo.NombreCompleto.replace(/\s+/, "").length == 0)) {
            mensajes.push("<p>El Nombre Completo no puede estar vacía</p>");
        } else if (modelo.NombreCompleto.length > 300) {
            mensajes.push("<p>El Nombre Completo puede contener hasta 300 caracteres</p>");
        }

        return mensajes;
    }

    function ConvertirModelo(modelo) {
        var nuevoModelo = {
            CentroId: modelo.CentroId
        };

        if (modelo.EmpleadoId !== undefined) {
            nuevoModelo.Id = modelo.EmpleadoId;
        }

        if (modelo.EmpleadoFicha !== undefined) {
            nuevoModelo.Ficha = modelo.EmpleadoFicha;
        }

        if (modelo.EmpleadoNombreCompleto !== undefined) {
            nuevoModelo.NombreCompleto = modelo.EmpleadoNombreCompleto;
        }

        return nuevoModelo;
    }
});