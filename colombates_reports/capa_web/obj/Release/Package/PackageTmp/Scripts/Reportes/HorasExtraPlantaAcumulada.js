﻿$(function () {

    $("#grafica1").kendoChart({
        categoryAxis: {
            majorGridLines: {
                visible: false
            },
            axisCrossingValues: [10, 0]
        },
        legend: {
            position: "bottom"
        },
        title: {
            background: "rgb(0,112,192)",
            color: "#fff",
            font: "bold 16px Arial,Helvetica,sans-serif",
            text: "HORAS EXTRA PLANTA ACUMULADAS AÑO"
        },
        seriesDefaults: {
            type: "column"
        },
        series:
        [{
            axis: "suma_horas",
            categoryField: "CausalExtra",
            color: "rgb(91, 155, 213)",
            field: "SumaHorasExtra",
            labels: {
                visible: true,
                format: "{0:n2}"
            },
            name: "Suma Horas Extra",
            overlay: {
                gradient: "none"
            },
            tooltip: {
                visible: true,
                format: "n2"
            }
        },
        {
            axis: "frecuencua_acum",
            categoryField: "CausalExtra",
            color: "rgb(0,32,96)",
            field: "FrecuenciaAcum",
            name: "Frecuencia Acumulada",
            type: "line",
            width: 2.25
        }],
        valueAxes: [{
            labels: {
                format: "p0"
            },
            majorGridLines: {
                visible: false
            },
            majorUnit: 0.1,
            min: 0,
            max: 1,
            name: "frecuencua_acum"
        },
        {
            labels: {
                format: "n0"
            },
            majorGridLines: {
                visible: false
            },
            majorUnit: 1000,
            name: "suma_horas"
        }]
    });
});