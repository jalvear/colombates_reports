﻿$(function () {

    $("#grafica1").kendoChart({
        categoryAxis: {
            majorGridLines: {
                visible: false
            },
            axisCrossingValues: [0, 10]
        },
        legend: {
            position: "bottom"
        },
        title: {
            background: "rgb(0,112,192)",
            color: "#fff",
            font: "bold 16px Arial,Helvetica,sans-serif",
            text: "HORAS HOMBRE PLANTA SACOS"
        },
        series: [
            {
                axis: "objetivo",
                categoryField: "Objetivo_Nombre",
                color: "rgb(91, 155, 213)",
                field: "Objetivo_Valor",
                labels: {
                    background: "rgb(91, 155, 213)",
                    format: "n0",
                    position: "center",
                    rotation: "-90",
                    visible: function (dato) {
                        return dato.value !== undefined && dato.value !== null;
                    }
                },
                name: "Objetivo",
                overlay: {
                    gradient: "none"
                },
                tooltip: {
                    visible: true,
                    format: "n0"
                }
            },
            {
                axis: "objetivo",
                categoryField: "Mes_Nombre",
                color: "rgb(0,32,96)",
                field: "Mes_Valor",
                labels: {
                    visible: true,
                    format: "n0"
                },
                missingValues: "gap",
                name: "Mes",
                type: "line",
                width: 2.25
            }
        ],
        seriesDefaults: {
            gap: 1,
            spacing: 0.2,
            type: "column"
        },
        valueAxes: [
            {
                labels: {
                    format: "n0"
                },
                majorGridLines: {
                    visible: false
                },
                majorUnit: 100,
                min: 0,
                name: "objetivo"
            }/*,
            {
                labels: {
                    format: "n0"
                },
                majorGridLines: {
                    visible: false
                },
                majorUnit: 100,
                min: 0,
                name: "mes"
            }*/
        ]
    });
});