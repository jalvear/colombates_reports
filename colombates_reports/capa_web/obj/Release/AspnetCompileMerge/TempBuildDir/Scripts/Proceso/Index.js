﻿$(document).ready(function () {
    /* Variables */

    var grdProcesos = $("#grdProcesos");

    crearGrid_grdProcesos();



    /* Métodos */

    function crearGrid_grdProcesos() {

        grdProcesos.kendoGrid({
            columns: [
                {
                    editor: function (container, options) {
                        var contenedor_input = $(div_contenedor_input);
                        var input = $.Editor({
                            atributos: {
                                id: "txtDescripcion",
                                maxlength: "60"
                            },
                            tipo: "texto"
                        });

                        if (options.model.Descripcion != null) {
                            input.val(options.model.Descripcion);
                        }

                        contenedor_input.append(input);
                        contenedor_input.appendTo(container);

                        input.change(function () {
                            options.model.Descripcion = input.val();
                            options.model.dirty = true;
                        });
                    },
                    field: "Descripcion",
                    title: "Descripción",
                    width: "300px"
                },
                {
                    command: [{
                        name: "edit",
                        text: { edit: "Modificar", cancel: "Cancelar", update: "Guardar" }
                    },
                    {
                        name: "eliminar",
                        click: function (e) {
                            e.preventDefault();

                            var grid = this;

                            bootbox.confirm({
                                buttons: {
                                    confirm: {
                                        label: 'Si',
                                        className: 'btn-success'
                                    },
                                    cancel: {
                                        label: 'No',
                                        className: 'btn-danger'
                                    }
                                },
                                message: "¿Realmente desea eliminar el Proceso?",
                                callback: function (result) {
                                    if (result) {
                                        var tr = $(e.target).closest("tr"); // get the current table row (tr)

                                        grid.removeRow(tr);
                                    }
                                }
                            });
                        },
                        text: "Eliminar"
                    }]
                }],
            dataSource: dataSource_grdProcesos(),
            editable: {
                confirmation: false,
                mode: 'inline'
            },
            filterable: {
                mode: "row"
            },
            groupable: false,
            pageable: {
                refresh: true,
                pageSizes: [5, 10, 20],
            },
            resizable: true,
            selectable: "row",
            sortable: true,
            scrollable: true,
            toolbar: [{ name: "create", text: "Crear" }]
        });
    }

    function dataSource_grdProcesos() {
        var gridDataSource = new kendo.data.DataSource({
            serverFiltering: false,
            serverPaging: false,
            serverSorting: false,
            transport: {
                read: function (options) {
                    // TODO: Agregar espera
                    console.log('Consultando Procesos');

                    $.ajax({
                        url: "/Proceso/ObtenerProcesos",
                        method: "POST",
                        dataType: "json", // Lo que recibo del servidor
                        /*contentType: "application/json", // Tipo de datos que envio al servidor */
                        /*data: JSON.stringify(datosEnviar),*/
                        complete: function () {
                            // TODO: Terminar espera
                        },
                        success: function (respuesta) {
                            console.log('Procesos consultados correctamente');
                            options.success(respuesta);
                        },
                        error: function (xhr) {
                            options.error();

                            bootbox.alert({
                                title: "Ocurrió un error al consultar los Procesos.",
                                message: ObtenerErrorServidor(xhr.responseText)
                            });
                        }
                    });
                },
                create: function (options) {
                    var grid = grdProcesos.data("kendoGrid");
                    var errores = ValidarModelo(options.data);

                    if (errores.length > 0) {
                        options.error();

                        bootbox.alert({
                            title: "Tener en cuenta lo siguiente:",
                            message: errores.join("")
                        });

                        return;
                    }

                    console.log("Creando Proceso");

                    $.ajax({
                        url: "/Proceso/Crear",
                        method: "POST",
                        //dataType: "json", // Lo que recibo del servidor
                        contentType: "application/json", // Tipo de datos que envio al servidor
                        data: JSON.stringify(options.data),
                        success: function (respuesta) {
                            console.log('Proceso creado correctamente');

                            options.success();

                            grid.dataSource.read();
                        },
                        error: function (xhr) {
                            options.error();

                            bootbox.alert({
                                title: "Ocurrió un error al crear el Proceso.",
                                message: ObtenerErrorServidor(xhr.responseText)
                            });
                        }
                    });
                },
                update: function (options) {
                    var grid = grdProcesos.data("kendoGrid");
                    var errores = ValidarModelo(options.data);

                    if (errores.length > 0) {
                        options.error();

                        bootbox.alert({
                            title: "Tener en cuenta lo siguiente:",
                            message: errores.join("")
                        });

                        return;
                    }

                    console.log('Editando Proceso');

                    $.ajax({
                        url: "/Proceso/Editar",
                        method: "POST",
                        //dataType: "json", // Lo que recibo del servidor
                        contentType: "application/json", // Tipo de datos que envio al servidor
                        data: JSON.stringify(options.data),
                        success: function (respuesta) {
                            console.log('Proceso editado correctamente');

                            options.success();

                            grid.dataSource.read();
                        },
                        error: function (xhr) {
                            options.error();

                            bootbox.alert({
                                title: "Ocurrió un error al crear el Proceso.",
                                message: ObtenerErrorServidor(xhr.responseText)
                            });
                        }
                    });
                },
                destroy: function (options) {
                    var grid = grdProcesos.data("kendoGrid");

                    $.ajax({
                        url: "/Proceso/Eliminar",
                        method: "POST",
                        //dataType: "json", // Lo que recibo del servidor
                        contentType: "application/json", // Tipo de datos que envio al servidor
                        data: JSON.stringify({
                            id: options.data.Id
                        }),
                        success: function (respuesta) {
                            console.log('Proceso eliminado correctamente');

                            options.success();

                            grid.dataSource.read();
                        },
                        error: function (xhr) {
                            options.error();

                            bootbox.alert({
                                title: "Ocurrió un error al crear el Proceso.",
                                message: ObtenerErrorServidor(xhr.responseText)
                            });
                        }
                    });
                }
            },
            pageSize: 5,
            schema: {
                model: {
                    id: "Id",
                    fields: {
                        Id: { editable: false, type: "number" },
                        Descripcion: { editable: true, nullable: false, type: "string" },
                        FechaCreado: { editable: false, format: '{0:dd/MMM/yyyy HH:mm tt}', nullable: true, type: 'date' },
                        FechaModificado: { editable: false, format: '{0:dd/MMM/yyyy HH:mm tt}', nullable: true, type: 'date' }
                    }
                }
            }
        });

        return gridDataSource;
    }

    function ValidarModelo(modelo) {
        var mensajes = [];

        if (modelo.Descripcion == null || modelo.Descripcion.replace(/\s+/, "").length == 0) {
            mensajes.push("<p>La descripción no puede estar vacía.</p>");
        } else if (modelo.Descripcion.length > 60) {
            mensajes.push("<p>La descripción puede contener hasta 60 caracteres</p>");
        }

        return mensajes;
    }
});