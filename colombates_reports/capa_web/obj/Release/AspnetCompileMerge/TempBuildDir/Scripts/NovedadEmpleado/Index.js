﻿$(document).ready(function () {
    /* Variables */

    var grdNovedadEmpleados = $("#grdNovedadEmpleados");

    crearGrid_grdEmpleados();



    /* Métodos */

    function crearGrid_grdEmpleados() {

        grdNovedadEmpleados.kendoGrid({
            columns: [{
                editor: function (container, options) {
                    var grid = grdNovedadEmpleados.data("kendoGrid");
                    var input = $('<input id="txtFechaRegistro" />')
                        .appendTo(container)
                        .kendoDatePicker({
                            change: function () {
                                var fila = grdNovedadEmpleados.find("tr.k-grid-edit-row")
                                    .first();
                                var registroEditando = grid.dataItem(fila);

                                registroEditando.NovedadEmpleadoFechaRegistro = this.value();
                                registroEditando.dirty = true;
                            },
                            format: '{0:dd/MMM/yyyy}'
                        }).data("kendoDatePicker");

                    if (options.model.NovedadEmpleadoFechaRegistro != null) {
                        input.value(options.model.NovedadEmpleadoFechaRegistro);
                    }
                    else {
                        var fecha = new Date();
                        var fila = grdNovedadEmpleados.find("tr.k-grid-edit-row")
                            .first();
                        var registroEditando = grid.dataItem(fila);

                        input.value(fecha);
                        registroEditando.NovedadEmpleadoFechaRegistro = fecha;
                        registroEditando.dirty = true;
                    }
                },
                field: "NovedadEmpleadoFechaRegistro",
                format: "{0:dd/MMM/yyyy}",
                title: "Fecha Registro",
                type: "date",
                width: "200px",
            },
            {
                editor: function (container, options) {
                    var contenedor_input = $(div_contenedor_input);
                    var input = $('<div id="cmbEmpleados" style="width: 100%;"></div>');

                    contenedor_input.append(input);
                    contenedor_input.appendTo(container);

                    if (options.model.EmpleadoId === 0) {
                        input.kendoMultiSelect({
                            autoBind: false,
                            dataSource: {
                                transport: {
                                    read: function (options) {
                                        var consultar = "";

                                        if (options.data.filter != undefined && options.data.filter.filters.length > 0) {
                                            consultar = options.data.filter.filters[0].value;
                                        }

                                        var datosEnviar = JSON.stringify({
                                            filtro: consultar
                                        });

                                        $.ajax({
                                            url: "/Empleado/FiltrarPor_Ficha_Nombre",
                                            data: datosEnviar,
                                            contentType: "application/json; charset=utf-8",
                                            dataType: "json",
                                            processData: false,
                                            method: "post",
                                            success: function (empleados) {
                                                console.log("Se consultaron los Empleados correctamente");

                                                options.success(empleados);

                                                //setTimeout(function () {
                                                //    if (consultar.indexOf(" ") > -1) {

                                                //        var cmbEmpleados = $("#cmbEmpleados").data("kendoMultiSelect");

                                                //        $.each(cmbEmpleados.items(), function (indice, elemento) {
                                                //            $(cmbEmpleados.items()).click();
                                                //        });
                                                //    }
                                                //}, 200);
                                            },
                                            error: function (xhr) {
                                                options.error();

                                                bootbox.alert({
                                                    title: "No se pudo consultar los Empleados.",
                                                    message: ObtenerErrorServidor(xhr.responseText)
                                                });
                                            }
                                        });
                                    }
                                },
                                serverFiltering: true
                            },
                            dataTextField: "EmpleadoFicha",
                            dataValueField: "EmpleadoId",
                            //highlightFirst: true,
                            delay: 300,
                            filter: 'contains',
                            minLength: 2,
                            placeholder: "Asociar Empleados",
                            valuePrimitive: false
                        });
                    }
                    else {
                        input.kendoComboBox({
                            dataSource: {
                                transport: {
                                    read: function (options) {
                                        var consultar = "";

                                        if (options.data.filter != undefined && options.data.filter.filters.length > 0) {
                                            consultar = options.data.filter.filters[0].value;
                                        }

                                        var datosEnviar = JSON.stringify({
                                            filtro: consultar
                                        });

                                        $.ajax({
                                            url: "/Empleado/FiltrarPor_Ficha_Nombre",
                                            data: datosEnviar,
                                            contentType: "application/json; charset=utf-8",
                                            dataType: "json",
                                            processData: false,
                                            method: "post",
                                            success: function (empleados) {
                                                console.log("Se consultaron los Empleados correctamente");
                                                options.success(empleados);
                                            },
                                            error: function (xhr) {
                                                options.error();

                                                bootbox.alert({
                                                    title: "No se pudo consultar los Empleados.",
                                                    message: ObtenerErrorServidor(xhr.responseText)
                                                });
                                            }
                                        });
                                    }
                                },
                                serverFiltering: true
                            },
                            dataTextField: "EmpleadoFicha",
                            dataValueField: "EmpleadoId",
                            delay: 300,
                            filter: 'contains',
                            minLength: 1,
                            placeholder: "Asociar Empleado",
                            select: function (e) {
                                if (e.item != null) {
                                    var grid = grdNovedadEmpleados.data("kendoGrid");
                                    var fila = grdNovedadEmpleados.find("tr.k-grid-edit-row")
                                        .first();
                                    var registroEditando = grid.dataItem(fila);
                                    var empleadoSeleccionado = this.dataItem(e.item.index());

                                    registroEditando.EmpleadoId = empleadoSeleccionado.EmpleadoId;
                                    registroEditando.EmpleadoNombreCompleto = empleadoSeleccionado.EmpleadoNombreCompleto;
                                    registroEditando.CentroId = empleadoSeleccionado.CentroId;
                                    registroEditando.CentroDescripcion = empleadoSeleccionado.CentroDescripcion;
                                    registroEditando.CentroSiglas = empleadoSeleccionado.CentroSiglas;
                                    registroEditando.dirty = true;

                                    $("#cmbCentros").data("kendoComboBox").value(empleadoSeleccionado.CentroSiglas);
                                    $("#txtNombreCompleto").val(empleadoSeleccionado.EmpleadoNombreCompleto);
                                }
                            },
                            valuePrimitive: true
                        }).data("kendoComboBox");

                        input.data("kendoComboBox").value(options.model.EmpleadoFicha);
                    }
                },
                field: "EmpleadoFicha",
                filterable: {
                    cell: {
                        operator: "contains",
                        showOperators: false,
                    }
                },
                title: "Ficha",
                width: "200px"
            },
            {
                editor: function (container, options) {
                    var contenedor_input = $(div_contenedor_input);
                    var input = $.Editor({
                        atributos: {
                            id: "txtNombreCompleto",
                            maxlength: "300",
                            readonly: "true"
                        },
                        tipo: "texto"
                    });

                    if (options.model.EmpleadoNombreCompleto != null) {
                        input.val(options.model.EmpleadoNombreCompleto);
                    }

                    contenedor_input.append(input);
                    contenedor_input.appendTo(container);
                },
                field: "EmpleadoNombreCompleto",
                filterable: {
                    cell: {
                        operator: "contains",
                        showOperators: false,
                    }
                },
                title: "Nombre Completo",
                width: "300px"
            },
            {
                editor: function (container, options) {
                    var contenedor_input = $(div_contenedor_input);
                    var input = $('<div id="cmbCentros" style="width: 100%;"></div>');

                    contenedor_input.append(input);
                    contenedor_input.appendTo(container);

                    input.kendoComboBox({
                        dataSource: {
                            transport: {
                                read: function (options) {
                                    /*var consultar = "";

                                    if (options.data.filter != undefined && options.data.filter.filters.length > 0) {
                                        consultar = options.data.filter.filters[0].value;
                                    }

                                    var datosEnviar = JSON.stringify({
                                        estado: "A",
                                        filtro: consultar,
                                        codigosOmitir: cmbLaboratorioComparte.value()
                                    });
                                    */
                                    $.ajax({
                                        url: "/Centro/ObtenerCentros",
                                        //data: datosEnviar,
                                        //contentType: "application/json; charset=utf-8",
                                        dataType: "json",
                                        processData: false,
                                        method: "post",
                                        success: function (centros) {
                                            console.log("Se consultaron los Centros correctamente");
                                            options.success(centros);
                                        },
                                        error: function (xhr) {
                                            options.error();

                                            bootbox.alert({
                                                title: "No se pudo consultar los Centros.",
                                                message: ObtenerErrorServidor(xhr.responseText)
                                            });
                                        }
                                    });
                                }
                            },
                            //serverFiltering: true
                        },
                        dataTextField: "Descripcion_Siglas",
                        dataValueField: "Id",
                        delay: 300,
                        filter: 'contains',
                        minLength: 1,
                        placeholder: "Asociar Centro",
                        select: function (e) {
                            var grid = grdNovedadEmpleados.data("kendoGrid");
                            var fila = grdNovedadEmpleados.find("tr.k-grid-edit-row")
                                .first();
                            var registroEditando = grid.dataItem(fila);
                            var centroSeleccionado = this.dataItem(e.item.index());

                            registroEditando.CentroId = centroSeleccionado.Id;
                            registroEditando.CentroSiglas = centroSeleccionado.Siglas;
                            registroEditando.CentroDescripcion = centroSeleccionado.Descripcion;
                            registroEditando.dirty = true;
                        },
                        valuePrimitive: true
                    }).data("kendoComboBox");

                    input.data("kendoComboBox").value(options.model.CentroSiglas);
                },
                field: "CentroId",
                filterable: {
                    cell: {
                        operator: "eq",
                        showOperators: false,
                        template: function (args) {
                            args.element.kendoComboBox({
                                dataSource: {
                                    transport: {
                                        read: function (options_filter) {
                                            /*var consultar = "";
        
                                            if (options.data.filter != undefined && options.data.filter.filters.length > 0) {
                                                consultar = options.data.filter.filters[0].value;
                                            }
        
                                            var datosEnviar = JSON.stringify({
                                                estado: "A",
                                                filtro: consultar,
                                                codigosOmitir: cmbLaboratorioComparte.value()
                                            });
                                            */
                                            $.ajax({
                                                url: "/Centro/ObtenerCentros",
                                                //data: datosEnviar,
                                                //contentType: "application/json; charset=utf-8",
                                                dataType: "json",
                                                processData: false,
                                                method: "post",
                                                success: function (centros) {
                                                    console.log("Se consultaron los Centros correctamente");
                                                    options_filter.success(centros);
                                                },
                                                error: function (xhr) {
                                                    options_filter.error();

                                                    bootbox.alert({
                                                        title: "No se pudo consultar los Centros.",
                                                        message: ObtenerErrorServidor(xhr.responseText)
                                                    });
                                                }
                                            });
                                        }
                                    },
                                    //serverFiltering: true
                                },
                                dataTextField: "Descripcion_Siglas",
                                dataValueField: "Id",
                                delay: 300,
                                filter: 'contains',
                                minLength: 1,
                                //placeholder: "Filtrar por Centro",
                                valuePrimitive: true
                            });
                        }
                    }
                },
                template: "#= CentroSiglas #",
                title: "Centro",
                width: "180px"
            },
            {
                editor: function (container, options) {
                    var contenedor_input = $(div_contenedor_input);
                    var input = $('<div name="cmbTipoHoras" style="width: 100%;"></div>');

                    contenedor_input.append(input);
                    contenedor_input.appendTo(container);

                    input.kendoComboBox({
                        dataSource: {
                            transport: {
                                read: function (options) {
                                    /*var consultar = "";

                                    if (options.data.filter != undefined && options.data.filter.filters.length > 0) {
                                        consultar = options.data.filter.filters[0].value;
                                    }

                                    var datosEnviar = JSON.stringify({
                                        estado: "A",
                                        filtro: consultar,
                                        codigosOmitir: cmbLaboratorioComparte.value()
                                    });
                                    */
                                    $.ajax({
                                        url: "/TipoHora/ObtenerTiposHora",
                                        //data: datosEnviar,
                                        //contentType: "application/json; charset=utf-8",
                                        dataType: "json",
                                        processData: false,
                                        method: "post",
                                        success: function (centros) {
                                            console.log("Se consultaron los Tipos de Hora correctamente");
                                            options.success(centros);
                                        },
                                        error: function (xhr) {
                                            options.error();

                                            bootbox.alert({
                                                title: "No se pudo consultar los Tipos de Hora.",
                                                message: ObtenerErrorServidor(xhr.responseText)
                                            });
                                        }
                                    });
                                }
                            },
                            //serverFiltering: true
                        },
                        dataTextField: "Siglas",
                        dataValueField: "Id",
                        delay: 300,
                        filter: 'contains',
                        minLength: 1,
                        placeholder: "Asociar Tipo de Hora",
                        select: function (e) {
                            var grid = grdNovedadEmpleados.data("kendoGrid");
                            var fila = grdNovedadEmpleados.find("tr.k-grid-edit-row")
                                .first();
                            var registroEditando = grid.dataItem(fila);
                            var tipoHoraSeleccionado = this.dataItem(e.item.index());

                            registroEditando.TipoHoraId = tipoHoraSeleccionado.Id;
                            registroEditando.TipoHoraSiglas = tipoHoraSeleccionado.Siglas;
                            registroEditando.TipoHoraDescripcion = tipoHoraSeleccionado.Descripcion;
                            registroEditando.dirty = true;
                        },
                        valuePrimitive: true
                    }).data("kendoComboBox");

                    if (options.model.TipoHoraId > 0) {
                        input.data("kendoComboBox").value(options.model.TipoHoraId);
                    }
                },
                field: "TipoHoraId",
                filterable: {
                    cell: {
                        operator: "eq",
                        showOperators: false,
                        template: function (args) {
                            args.element.kendoComboBox({
                                dataSource: {
                                    transport: {
                                        read: function (options_filter) {
                                            /*var consultar = "";
        
                                            if (options.data.filter != undefined && options.data.filter.filters.length > 0) {
                                                consultar = options.data.filter.filters[0].value;
                                            }
        
                                            var datosEnviar = JSON.stringify({
                                                estado: "A",
                                                filtro: consultar,
                                                codigosOmitir: cmbLaboratorioComparte.value()
                                            });
                                            */
                                            $.ajax({
                                                url: "/TipoHora/ObtenerTiposHora",
                                                //data: datosEnviar,
                                                //contentType: "application/json; charset=utf-8",
                                                dataType: "json",
                                                processData: false,
                                                method: "post",
                                                success: function (centros) {
                                                    console.log("Se consultaron los Tipos de Hora correctamente");
                                                    options_filter.success(centros);
                                                },
                                                error: function (xhr) {
                                                    options_filter.error();

                                                    bootbox.alert({
                                                        title: "No se pudo consultar los Tipos de Hora.",
                                                        message: ObtenerErrorServidor(xhr.responseText)
                                                    });
                                                }
                                            });
                                        }
                                    },
                                    //serverFiltering: true
                                },
                                dataTextField: "Siglas",
                                dataValueField: "Id",
                                delay: 300,
                                filter: 'contains',
                                minLength: 1,
                                //placeholder: "Filtrar por Centro",
                                valuePrimitive: true
                            });
                        }
                    }
                },
                template: "#= TipoHoraSiglas #",
                title: "Tipos Horas",
                width: "180px"
            },
            {
                editor: function (container, options) {
                    var contenedor_input = $(div_contenedor_input);
                    var input = $('<div id="cmbTurnos" style="width: 100%;"></div>');

                    contenedor_input.append(input);
                    contenedor_input.appendTo(container);

                    input.kendoComboBox({
                        dataSource: {
                            transport: {
                                read: function (options) {
                                    /*var consultar = "";

                                    if (options.data.filter != undefined && options.data.filter.filters.length > 0) {
                                        consultar = options.data.filter.filters[0].value;
                                    }

                                    var datosEnviar = JSON.stringify({
                                        estado: "A",
                                        filtro: consultar,
                                        codigosOmitir: cmbLaboratorioComparte.value()
                                    });
                                    */
                                    $.ajax({
                                        url: "/Turno/ObtenerTurnos",
                                        //data: datosEnviar,
                                        //contentType: "application/json; charset=utf-8",
                                        dataType: "json",
                                        processData: false,
                                        method: "post",
                                        success: function (centros) {
                                            console.log("Se consultaron los Turnos correctamente");
                                            options.success(centros);
                                        },
                                        error: function (xhr) {
                                            options.error();

                                            bootbox.alert({
                                                title: "No se pudo consultar los Turnos.",
                                                message: ObtenerErrorServidor(xhr.responseText)
                                            });
                                        }
                                    });
                                }
                            },
                            //serverFiltering: true
                        },
                        dataTextField: "Siglas",
                        dataValueField: "Id",
                        delay: 300,
                        filter: 'contains',
                        minLength: 1,
                        placeholder: "Asociar Turno",
                        select: function (e) {
                            var grid = grdNovedadEmpleados.data("kendoGrid");
                            var fila = grdNovedadEmpleados.find("tr.k-grid-edit-row")
                                .first();
                            var registroEditando = grid.dataItem(fila);
                            var turnoSeleccionado = this.dataItem(e.item.index());

                            registroEditando.TurnoId = turnoSeleccionado.Id;
                            registroEditando.TurnoSiglas = turnoSeleccionado.Siglas;
                            registroEditando.TurnoDescripcion = turnoSeleccionado.Descripcion;
                            registroEditando.dirty = true;
                        },
                        valuePrimitive: true
                    }).data("kendoComboBox");

                    if (options.model.TurnoId != 0) {
                        input.data("kendoComboBox").value(options.model.TurnoId);
                    }
                },
                field: "TurnoId",
                filterable: {
                    cell: {
                        operator: "eq",
                        showOperators: false,
                        template: function (args) {
                            args.element.kendoComboBox({
                                dataSource: {
                                    transport: {
                                        read: function (options_filter) {
                                            /*var consultar = "";
        
                                            if (options.data.filter != undefined && options.data.filter.filters.length > 0) {
                                                consultar = options.data.filter.filters[0].value;
                                            }
        
                                            var datosEnviar = JSON.stringify({
                                                estado: "A",
                                                filtro: consultar,
                                                codigosOmitir: cmbLaboratorioComparte.value()
                                            });
                                            */
                                            $.ajax({
                                                url: "/Turno/ObtenerTurnos",
                                                //data: datosEnviar,
                                                //contentType: "application/json; charset=utf-8",
                                                dataType: "json",
                                                processData: false,
                                                method: "post",
                                                success: function (centros) {
                                                    console.log("Se consultaron los Turnos correctamente");
                                                    options_filter.success(centros);
                                                },
                                                error: function (xhr) {
                                                    options_filter.error();

                                                    bootbox.alert({
                                                        title: "No se pudo consultar los Turnos.",
                                                        message: ObtenerErrorServidor(xhr.responseText)
                                                    });
                                                }
                                            });
                                        }
                                    },
                                    //serverFiltering: true
                                },
                                dataTextField: "Siglas",
                                dataValueField: "Id",
                                delay: 300,
                                filter: 'contains',
                                minLength: 1,
                                //placeholder: "Filtrar por Centro",
                                valuePrimitive: true
                            });
                        }
                    }
                },
                template: "#= TurnoSiglas #",
                title: "Turno",
                width: "180px"
            },
            {
                editor: function (container, options) {
                    var contenedor_input = $(div_contenedor_input);
                    var input = $.Editor({
                        atributos: {
                            id: "txtDiasHabiles",
                            min: "1"
                        },
                        tipo: "numero"
                    });

                    if (options.model.NovedadEmpleadoDiasHabiles != 0) {
                        input.val(options.model.NovedadEmpleadoDiasHabiles);
                    }

                    contenedor_input.append(input);
                    contenedor_input.appendTo(container);

                    input.change(function () {
                        //$("#txtHorasProgramadas").val(input.val() * 8);

                        var grid = grdNovedadEmpleados.data("kendoGrid");
                        var fila = grdNovedadEmpleados.find("tr.k-grid-edit-row")
                            .first();
                        var registroEditando = grid.dataItem(fila);

                        registroEditando.NovedadEmpleadoDiasHabiles = input.val();
                        registroEditando.dirty = true;
                    });
                },
                field: "NovedadEmpleadoDiasHabiles",
                filterable: false,
                title: "Días Hábiles",
                width: "110px"
            },
            {
                field: "Semana",
                filterable: false,
                title: "Semana",
                width: "90px"
            },
            {
                command: [{
                    name: "edit",
                    text: { edit: "Modificar", cancel: "Cancelar", update: "Guardar" }
                },
                {
                    name: "eliminar",
                    click: function (e) {
                        e.preventDefault();

                        var grid = this;


                        bootbox.confirm({
                            buttons: {
                                confirm: {
                                    label: 'Si',
                                    className: 'btn-success'
                                },
                                cancel: {
                                    label: 'No',
                                    className: 'btn-danger'
                                }
                            },
                            message: "¿Realmente desea eliminar la Novedad?",
                            callback: function (result) {
                                if (result) {
                                    console.log('Eliminando Empleado');

                                    var tr = $(e.target).closest("tr"); // get the current table row (tr)
                                    grid.removeRow(tr);
                                }
                            }
                        });
                    },
                    text: "Eliminar"
                }],
                width: "200px"
            }],
            dataSource: dataSource_grdEmpleados(),
            editable: {
                confirmation: false,
                mode: 'inline'
            },
            filterable: { mode: "row" },
            groupable: false,
            pageable: {
                refresh: true,
                pageSizes: [5, 10, 20],
            },
            resizable: true,
            selectable: "row",
            sortable: true,
            scrollable: true,
            toolbar: [{ name: "create", text: "Crear" }]
        });
    }

    function dataSource_grdEmpleados() {
        var gridDataSource = new kendo.data.DataSource({
            serverFiltering: true,
            serverPaging: true,
            serverSorting: true,
            transport: {
                read: function (options) {
                    var filtering = [];
                    var sorting = [];

                    if (options.data.filter != undefined) {
                        $.each(options.data.filter.filters, function (indice, filtro) {
                            filtering.push({ Field: filtro.field, Value: filtro.value, Operator: filtro.operator });
                        });
                    }

                    if (options.data.sort != undefined) {
                        $.each(options.data.sort, function (indice, orden) {
                            sorting.push({ Field: orden.field, Dir: orden.dir });
                        });
                    }

                    var take = options.data.take;

                    if (take == undefined) {
                        take = 10;
                    }

                    var datosEnviar = {
                        filter: filtering,
                        sort: sorting,
                        skip: options.data.skip,
                        take: take
                    };


                    // TODO: Agregar espera
                    console.log('Consultando Novedades');

                    $.ajax({
                        url: "/NovedadEmpleado/ObtenerNovedades",
                        method: "POST",
                        dataType: "json", // Lo que recibo del servidor
                        contentType: "application/json", // Tipo de datos que envio al servidor 
                        data: JSON.stringify(datosEnviar),
                        complete: function () {
                            // TODO: Terminar espera
                        },
                        success: function (respuesta) {
                            console.log('Novedades consultadas correctamente');
                            options.success(respuesta);
                        },
                        error: function (xhr) {
                            options.error();

                            bootbox.alert({
                                title: "Ocurrió un error al consultar los Novedades.",
                                message: ObtenerErrorServidor(xhr.responseText)
                            });
                        }
                    });
                },
                create: function (options) {
                    var grid = grdNovedadEmpleados.data("kendoGrid");
                    var modelo = ConvertirModelo(options.data);
                    var errores = ValidarModelo(modelo);

                    if (errores.length > 0) {
                        options.error();

                        bootbox.alert({
                            title: "Tener en cuenta lo siguiente:",
                            message: errores.join("")
                        });

                        return;
                    }

                    console.log("Creando Novedad");

                    $.ajax({
                        url: "/NovedadEmpleado/Crear",
                        method: "POST",
                        //dataType: "json", // Lo que recibo del servidor
                        contentType: "application/json", // Tipo de datos que envio al servidor
                        data: JSON.stringify(modelo),
                        success: function (respuesta) {
                            console.log('Novedad creado correctamente');
                            options.success();

                            //$("#btnGuardar, #btnCancelar").fadeOut(250, function () {
                            //    $("#btnCrear, #btnEditar, #btnEliminar").fadeIn();
                            //});

                            grid.dataSource.read();
                        },
                        error: function (xhr) {
                            options.error();

                            bootbox.alert({
                                title: "Ocurrió un error al crear la Novedad.",
                                message: ObtenerErrorServidor(xhr.responseText)
                            });
                        }
                    });
                },
                update: function (options) {
                    var grid = grdNovedadEmpleados.data("kendoGrid");
                    var modelo = ConvertirModelo(options.data);
                    var errores = ValidarModelo(modelo);

                    if (errores.length > 0) {
                        options.error();

                        bootbox.alert({
                            title: "Tener en cuenta lo siguiente:",
                            message: errores.join("")
                        });

                        return;
                    }

                    console.log('Editando Novedad');

                    $.ajax({
                        url: "/NovedadEmpleado/Editar",
                        method: "POST",
                        //dataType: "json", // Lo que recibo del servidor
                        contentType: "application/json", // Tipo de datos que envio al servidor
                        data: JSON.stringify(modelo),
                        success: function (respuesta) {
                            console.log('Novedad editada correctamente');
                            options.success();

                            //$("#btnGuardar, #btnCancelar").fadeOut(250, function () {
                            //    $("#btnCrear, #btnEditar, #btnEliminar").fadeIn();
                            //});

                            grid.dataSource.read();
                        },
                        error: function (xhr) {
                            options.error();

                            bootbox.alert({
                                title: "Ocurrió un error al editar la Novedad.",
                                message: ObtenerErrorServidor(xhr.responseText)
                            });
                        }
                    });
                },
                destroy: function (options) {
                    var grid = grdNovedadEmpleados.data("kendoGrid");

                    $.ajax({
                        url: "/NovedadEmpleado/Eliminar",
                        method: "POST",
                        //dataType: "json", // Lo que recibo del servidor
                        contentType: "application/json", // Tipo de datos que envio al servidor
                        data: JSON.stringify({
                            id: options.data.NovedadEmpleadoId
                        }),
                        success: function (respuesta) {
                            console.log('Novedad eliminado correctamente');
                            options.success();

                            //$("#btnGuardar, #btnCancelar").fadeOut(250, function () {
                            //    $("#btnCrear, #btnEditar, #btnEliminar").fadeIn();
                            //});

                            grid.dataSource.read();
                        },
                        error: function (xhr) {
                            options.error();

                            bootbox.alert({
                                title: "Ocurrió un error al eliminar la Novedad.",
                                message: ObtenerErrorServidor(xhr.responseText)
                            });
                        }
                    });
                }
            },
            pageSize: 10,
            schema: {
                data: 'value',
                model: {
                    id: "NovedadEmpleadoId",
                    fields: {
                        NovedadEmpleadoId: { editable: false, type: "number" },
                        EmpleadoId: { editable: false, type: "number" },
                        EmpleadoFicha: { editable: true, nullable: false, type: "string" },
                        EmpleadoNombreCompleto: { editable: true, nullable: false, type: "string" },
                        CentroId: { editable: true, nullable: false, type: "number" },
                        CentroSiglas: { editable: true, nullable: false, type: "string" },
                        CentroDescripcion: { editable: true, nullable: false, type: "string" },
                        Semana: { defaultValue: 1, editable: false, nullable: true, type: "number" },
                        Mes: { defaultValue: 1, editable: false, nullable: true, type: "number" },
                        NovedadEmpleadoDiasHabiles: { defaultValue: 1, editable: true, nullable: false, type: "number" },
                        NovedadEmpleadoHorasProgramadas: { defaultValue: 8, editable: false, nullable: true, type: "number" },
                        TurnoId: { editable: true, nullable: false, type: "number" },
                        TurnoSiglas: { editable: true, nullable: false, type: "string" },
                        TurnoDescripcion: { editable: true, nullable: false, type: "string" },
                        TipoHoraId: { editable: true, nullable: false, type: "number" },
                        TipoHoraSiglas: { editable: true, nullable: false, type: "string" },
                        TipoHoraDescripcion: { editable: true, nullable: false, type: "string" },
                        NovedadEmpleadoFechaRegistro: { editable: true, format: '{0:dd/MMM/yyyy HH:mm tt}', nullable: true, type: 'date' },
                        //FechaCreado: { editable: false, format: '{0:dd/MMM/yyyy HH:mm tt}', nullable: true, type: 'date' },
                        //FechaModificado: { editable: false, format: '{0:dd/MMM/yyyy HH:mm tt}', nullable: true, type: 'date' }
                    }
                },
                total: 'count',
            }
        });

        return gridDataSource;
    }

    function ValidarModelo(modelo) {
        var mensajes = [];

        if (modelo.CentroId == null || (modelo.CentroId != null && modelo.CentroId == 0)) {
            mensajes.push("<p>El Centro no puese estár vacío.</p>");
        }

        if (modelo.TurnoId == null || (modelo.TurnoId != null && modelo.TurnoId == 0)) {
            mensajes.push("<p>El Turno no puese estár vacío.</p>");
        }

        if (modelo.TipoHoraId == null || (modelo.TipoHoraId != null && modelo.TipoHoraId == 0)) {
            mensajes.push("<p>El Tipo de Hora no puese estár vacío.</p>");
        }

        if (modelo.VariosId.length == 0 &&
            (modelo.EmpleadoId == null || (modelo.EmpleadoId != null && modelo.EmpleadoId == 0))) {
            mensajes.push("<p>El Empleado no puese estár vacío.</p>");
        }

        if (modelo.FechaRegistro == null) {
            mensajes.push("<p>La Fecha de Registro no puede estar vacía</p>");
        }

        if (modelo.DiasHabiles == null || (modelo.DiasHabiles != null && modelo.DiasHabiles == 0)) {
            mensajes.push("<p>Los Días Hábiles no pueden estar vacías</p>");
        }

        return mensajes;
    }

    function ConvertirModelo(modelo) {
        var nuevoModelo = {
            CentroId: modelo.CentroId,
            TurnoId: modelo.TurnoId,
            TipoHoraId: modelo.TipoHoraId,
            EmpleadoId: modelo.EmpleadoId
        };

        if (modelo.NovedadEmpleadoId !== undefined) {
            nuevoModelo.Id = modelo.NovedadEmpleadoId;
        }

        if (modelo.NovedadEmpleadoDiasHabiles !== undefined) {
            nuevoModelo.DiasHabiles = modelo.NovedadEmpleadoDiasHabiles;
        }

        if (modelo.NovedadEmpleadoFechaRegistro !== undefined) {
            nuevoModelo.FechaRegistro = modelo.NovedadEmpleadoFechaRegistro;
        }

        var cmbEmpleados = $("#cmbEmpleados").data("kendoMultiSelect");

        if (cmbEmpleados !== undefined) {
            nuevoModelo.VariosId = cmbEmpleados.value().slice(1);
        }

        return nuevoModelo;
    }
});