﻿$(document).ready(function () {
    /* Variables */

    var grdHorasProgramasMes = $("#grdHorasProgramasMes");

    crearGrid_grdHorasProgramasMes();



    /* Métodos */

    function Asignar_Anio(anio) {
        var grid = grdHorasProgramasMes.data("kendoGrid");
        var fila = grdHorasProgramasMes.find("tr.k-grid-edit-row")
            .first();
        var registroEditando = grid.dataItem(fila);

        registroEditando.Anio = anio;
        registroEditando.dirty = true;
    }

    function Asignar_Mes(mes) {
        var grid = grdHorasProgramasMes.data("kendoGrid");
        var fila = grdHorasProgramasMes.find("tr.k-grid-edit-row")
            .first();
        var registroEditando = grid.dataItem(fila);

        registroEditando.Mes = mes;
        registroEditando.dirty = true;
    }

    function crearGrid_grdHorasProgramasMes() {

        grdHorasProgramasMes.kendoGrid({
            columns: [
                {
                    editor: function (container, options) {
                        var contenedor_input = $(div_contenedor_input);
                        var input = $.Editor({
                            atributos: {
                                id: "txtAnio",
                                min: "1"
                            },
                            tipo: "numero"
                        });

                        var anio = options.model.Anio;

                        if (options.model.Anio === 0) {
                            anio = new Date().getFullYear();
                        }

                        input.val(anio);
                        Asignar_Anio(anio);

                        contenedor_input.append(input);
                        contenedor_input.appendTo(container);

                        input.change(function () {
                            //$("#txtHorasProgramadas").val(input.val() * 8);

                            Asignar_Anio(input.val());
                        });
                    },
                    field: "Anio",
                    filterable: {
                        cell: {
                            //operator: "eq",
                            //showOperators: false,
                            template: function (args) {
                                args.element.kendoDropDownList({
                                    dataSource: args.dataSource,
                                    dataTextField: "Anio",
                                    dataValueField: "Anio",
                                    valuePrimitive: true
                                });
                            }
                        }
                    },
                    title: "Año",
                    width: "120px"
                },
                {
                    editor: function (container, options) {
                        var contenedor_input = $(div_contenedor_input);
                        var input = $.Editor({
                            atributos: {
                                id: "txtMes",
                                min: "1",
                                max: "12"
                            },
                            tipo: "numero"
                        });

                        var mes = options.model.Mes;

                        if (options.model.Mes === 0) {
                            mes = new Date().getMonth() + 1;
                        }

                        input.val(mes);
                        Asignar_Mes(mes);

                        contenedor_input.append(input);
                        contenedor_input.appendTo(container);

                        input.change(function () {
                            Asignar_Mes(input.val());
                        });
                    },
                    field: "Mes",
                    filterable: {
                        cell: {
                            //operator: "eq",
                            //showOperators: false,
                            template: function (args) {
                                args.element.kendoDropDownList({
                                    dataSource: args.dataSource,
                                    dataTextField: "Mes",
                                    dataValueField: "Mes",
                                    valuePrimitive: true
                                });
                            }
                        }
                    },
                    title: "Mes",
                    width: "120px"
                },
                {
                    editor: function (container, options) {
                        var contenedor_input = $(div_contenedor_input);
                        var input = $.Editor({
                            atributos: {
                                id: "txtHoras",
                                min: "1"
                            },
                            tipo: "numero"
                        });

                        if (options.model.Horas > 0) {
                            input.val(options.model.Horas);
                        }

                        contenedor_input.append(input);
                        contenedor_input.appendTo(container);

                        input.change(function () {
                            //$("#txtHorasProgramadas").val(input.val() * 8);

                            var grid = grdHorasProgramasMes.data("kendoGrid");
                            var fila = grdHorasProgramasMes.find("tr.k-grid-edit-row")
                                .first();
                            var registroEditando = grid.dataItem(fila);

                            registroEditando.Horas = input.val();
                            registroEditando.dirty = true;
                        });
                    },
                    field: "Horas",
                    filterable: true,
                    title: "Horas",
                    width: "120px"
                },
                {
                    editor: function (container, options) {
                        var contenedor_input = $(div_contenedor_input);
                        var input = $('<div id="cmbCentros" style="width: 100%;"></div>');

                        contenedor_input.append(input);
                        contenedor_input.appendTo(container);

                        input.kendoComboBox({
                            dataSource: {
                                transport: {
                                    read: function (options) {
                                        $.ajax({
                                            url: "/Centro/ObtenerCentros",
                                            //data: datosEnviar,
                                            //contentType: "application/json; charset=utf-8",
                                            dataType: "json",
                                            processData: false,
                                            method: "post",
                                            success: function (centros) {
                                                console.log("Se consultaron los Centros correctamente");
                                                options.success(centros);
                                            },
                                            error: function (xhr) {
                                                options.error();

                                                bootbox.alert({
                                                    title: "No se pudo consultar los Centros.",
                                                    message: ObtenerErrorServidor(xhr.responseText)
                                                });
                                            }
                                        });
                                    }
                                }
                                //serverFiltering: true
                            },
                            dataTextField: "Descripcion_Siglas",
                            dataValueField: "Id",
                            delay: 300,
                            filter: 'contains',
                            minLength: 1,
                            placeholder: "Asociar Centro",
                            select: function (e) {
                                var grid = grdHorasProgramasMes.data("kendoGrid");
                                var fila = grdHorasProgramasMes.find("tr.k-grid-edit-row")
                                    .first();
                                var registroEditando = grid.dataItem(fila);
                                var centroSeleccionado = this.dataItem(e.item.index());

                                registroEditando.CentroId = centroSeleccionado.Id;
                                registroEditando.CentroSiglas = centroSeleccionado.Siglas;
                                registroEditando.CentroDescripcion = centroSeleccionado.Descripcion;
                                registroEditando.dirty = true;
                            },
                            valuePrimitive: true
                        }).data("kendoComboBox");

                        input.data("kendoComboBox").value(options.model.CentroSiglas);
                    },
                    field: "CentroId",
                    filterable: {
                        cell: {
                            operator: "eq",
                            showOperators: false,
                            template: function (args) {
                                args.element.kendoComboBox({
                                    dataSource: {
                                        transport: {
                                            read: function (options_filter) {
                                                $.ajax({
                                                    url: "/Centro/ObtenerCentros",
                                                    //data: datosEnviar,
                                                    //contentType: "application/json; charset=utf-8",
                                                    dataType: "json",
                                                    processData: false,
                                                    method: "post",
                                                    success: function (centros) {
                                                        console.log("Se consultaron los Centros correctamente");
                                                        options_filter.success(centros);
                                                    },
                                                    error: function (xhr) {
                                                        options_filter.error();

                                                        bootbox.alert({
                                                            title: "No se pudo consultar los Centros.",
                                                            message: ObtenerErrorServidor(xhr.responseText)
                                                        });
                                                    }
                                                });
                                            }
                                        }
                                        //serverFiltering: true
                                    },
                                    dataTextField: "Descripcion_Siglas",
                                    dataValueField: "Id",
                                    delay: 300,
                                    filter: 'contains',
                                    minLength: 1,
                                    //placeholder: "Filtrar por Centro",
                                    valuePrimitive: true
                                });
                            }
                        }
                    },
                    template: "#= CentroSiglas #",
                    title: "Centro",
                    width: "180px"
                },
                {
                    command: [{
                        name: "edit",
                        text: { edit: "Modificar", cancel: "Cancelar", update: "Guardar" }
                    },
                    {
                        name: "eliminar",
                        click: function (e) {
                            e.preventDefault();

                            var grid = this;


                            bootbox.confirm({
                                buttons: {
                                    confirm: {
                                        label: 'Si',
                                        className: 'btn-success'
                                    },
                                    cancel: {
                                        label: 'No',
                                        className: 'btn-danger'
                                    }
                                },
                                message: "¿Realmente desea eliminar este registro de Extras?",
                                callback: function (result) {
                                    if (result) {
                                        console.log('Eliminando registro de Extras');

                                        var tr = $(e.target).closest("tr"); // get the current table row (tr)
                                        grid.removeRow(tr);
                                    }
                                }
                            });
                        },
                        text: "Eliminar"
                    }],
                    width: "200px"
                }],
            dataSource: dataSource_grdHorasProgramasMes(),
            editable: {
                confirmation: false,
                mode: 'inline'
            },
            filterable: { mode: "row" },
            groupable: false,
            pageable: {
                refresh: true,
                pageSizes: [5, 12, 24]
            },
            resizable: true,
            selectable: "row",
            sortable: true,
            scrollable: true,
            toolbar: [{ name: "create", text: "Crear" }]
        });
    }

    function dataSource_grdHorasProgramasMes() {
        var gridDataSource = new kendo.data.DataSource({
            serverFiltering: true,
            serverPaging: true,
            serverSorting: true,
            transport: {
                read: function (options) {
                    var filtering = [];
                    var sorting = [];

                    if (options.data.filter !== undefined) {
                        $.each(options.data.filter.filters, function (indice, filtro) {
                            filtering.push({ Field: filtro.field, Value: filtro.value, Operator: filtro.operator });
                        });
                    }

                    if (options.data.sort !== undefined) {
                        $.each(options.data.sort, function (indice, orden) {
                            sorting.push({ Field: orden.field, Dir: orden.dir });
                        });
                    }

                    var take = options.data.take;

                    if (take === undefined) {
                        take = 10;
                    }

                    var datosEnviar = {
                        filter: filtering,
                        sort: sorting,
                        skip: options.data.skip,
                        take: take
                    };


                    // TODO: Agregar espera
                    console.log('Consultando Horas Programadas por Mes');

                    $.ajax({
                        url: "/HorasProgramadasMes/ObtenerHorasProgramadas",
                        method: "POST",
                        dataType: "json", // Lo que recibo del servidor
                        contentType: "application/json", // Tipo de datos que envio al servidor 
                        data: JSON.stringify(datosEnviar),
                        complete: function () {
                            // TODO: Terminar espera
                        },
                        success: function (respuesta) {
                            console.log('Horas Programadas por Mes consultadas correctamente');
                            options.success(respuesta);
                        },
                        error: function (xhr) {
                            options.error();

                            bootbox.alert({
                                title: "Ocurrió un error al consultar las Horas Programadas por Mes.",
                                message: ObtenerErrorServidor(xhr.responseText)
                            });
                        }
                    });
                },
                create: function (options) {
                    var grid = grdHorasProgramasMes.data("kendoGrid");
                    var modelo = options.data;
                    var errores = ValidarModelo(modelo);

                    if (errores.length > 0) {
                        options.error();

                        bootbox.alert({
                            title: "Tener en cuenta lo siguiente:",
                            message: errores.join("")
                        });

                        return;
                    }

                    console.log("Creando Registro");

                    $.ajax({
                        url: "/HorasProgramadasMes/Crear",
                        method: "POST",
                        //dataType: "json", // Lo que recibo del servidor
                        contentType: "application/json", // Tipo de datos que envio al servidor
                        data: JSON.stringify(modelo),
                        success: function (respuesta) {
                            console.log('Registro creado correctamente');

                            options.success();

                            grid.dataSource.read();
                        },
                        error: function (xhr) {
                            options.error();

                            bootbox.alert({
                                title: "Ocurrió un error al crear el registro.",
                                message: ObtenerErrorServidor(xhr.responseText)
                            });
                        }
                    });
                },
                update: function (options) {
                    var grid = grdHorasProgramasMes.data("kendoGrid");
                    var modelo = options.data;
                    var errores = ValidarModelo(modelo);

                    if (errores.length > 0) {
                        options.error();

                        bootbox.alert({
                            title: "Tener en cuenta lo siguiente:",
                            message: errores.join("")
                        });

                        return;
                    }

                    console.log('Editando Registro');

                    $.ajax({
                        url: "/HorasProgramadasMes/Editar",
                        method: "POST",
                        //dataType: "json", // Lo que recibo del servidor
                        contentType: "application/json", // Tipo de datos que envio al servidor
                        data: JSON.stringify(modelo),
                        success: function (respuesta) {
                            console.log('Registro editado correctamente');

                            options.success();

                            grid.dataSource.read();
                        },
                        error: function (xhr) {
                            options.error();

                            bootbox.alert({
                                title: "Ocurrió un error al editar el registro.",
                                message: ObtenerErrorServidor(xhr.responseText)
                            });
                        }
                    });
                },
                destroy: function (options) {
                    var grid = grdHorasProgramasMes.data("kendoGrid");

                    $.ajax({
                        url: "/HorasProgramadasMes/Eliminar",
                        method: "POST",
                        //dataType: "json", // Lo que recibo del servidor
                        contentType: "application/json", // Tipo de datos que envio al servidor
                        data: JSON.stringify({
                            id: options.data.Id
                        }),
                        success: function (respuesta) {
                            console.log('Registro eliminado correctamente');
                            options.success();

                            grid.dataSource.read();
                        },
                        error: function (xhr) {
                            options.error();

                            bootbox.alert({
                                title: "Ocurrió un error al eliminar el registro.",
                                message: ObtenerErrorServidor(xhr.responseText)
                            });
                        }
                    });
                }
            },
            pageSize: 12,
            schema: {
                data: 'value',
                model: {
                    id: "Id",
                    fields: {
                        Id: { editable: false, type: "number" },
                        Anio: { defaultValue: 0, editable: true, nullable: false, type: "number" },
                        Mes: { defaultValue: 0, editable: true, nullable: false, type: "number" },
                        Horas: { defaultValue: 0, editable: true, nullable: false, type: "number" },
                        CentroId: { editable: true, nullable: false, type: "number" },
                        CentroSiglas: { editable: true, nullable: true, type: "string" }
                        //FechaCreado: { editable: false, format: '{0:dd/MMM/yyyy HH:mm tt}', nullable: true, type: 'date' },
                        //FechaModificado: { editable: false, format: '{0:dd/MMM/yyyy HH:mm tt}', nullable: true, type: 'date' }
                    }
                },
                total: 'count'
            }
        });

        return gridDataSource;
    }

    function ValidarModelo(modelo) {
        var mensajes = [];

        if (modelo.Anio === null || (modelo.Anio !== null && modelo.Anio === 0)) {
            mensajes.push("<p>El Año no puede estár vacío.</p>");
        }

        if (modelo.Mes === null || (modelo.Mes !== null && modelo.Mes === 0)) {
            mensajes.push("<p>El Mes no puede estár vacío.</p>");
        } else if (modelo.Mes > 12) {
            mensajes.push("<p>El Mes no puesd ser mayór a 12 (Diciembre).</p>");
        }

        if (modelo.Horas === null || (modelo.Horas !== null && modelo.Horas === 0)) {
            mensajes.push("<p>Las Horas Programadas no pueden estar vacías</p>");
        }

        if (modelo.CentroId === null || (modelo.CentroId !== null && modelo.CentroId === 0)) {
            mensajes.push("<p>El Centro no puede estár vacío.</p>");
        }

        return mensajes;
    }
});