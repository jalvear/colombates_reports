﻿$(function () {

    $("#grafica1").kendoChart({
        categoryAxis: {
            majorGridLines: {
                visible: false
            }
        },
        legend: {
            position: "bottom"
        },
        title: {
            background: "rgb(0,112,192)",
            color: "#fff",
            font: "bold 16px Arial,Helvetica,sans-serif",
            text: "HORAS EXTRA MES"
        },
        series:
        [{
            categoryField: "Mes",
            color: "rgb(91, 155, 213)",
            field: "ExtrasNormales",
            labels: {
                background: "rgb(91, 155, 213)",
                format: "{0:n2}",
                position: "center",
                rotation: "-90",
                visible: true
            },
            name: "Extras Normales",
            overlay: {
                gradient: "none"
            },
            tooltip: {
                visible: true,
                format: "N2"
            }
        },
        {
            categoryField: "Mes",
            color: "rgb(237, 125, 49)",
            field: "Festivas",
            labels: {
                visible: true,
                format: "{0:n1}"
            },
            name: "Festivas",
            overlay: {
                gradient: "none"
            },
            tooltip: {
                visible: true,
                format: "N1"
            }
        }],
        seriesDefaults: {
            gap: 1,
            spacing: 0.2,
            type: "column"
        },
        valueAxis: {
            majorGridLines: {
                visible: false
            },
            labels: {
                format: "N0"
            },
            //majorUnit: 100,
            line: {
                visible: false
            }
        }
    });
});