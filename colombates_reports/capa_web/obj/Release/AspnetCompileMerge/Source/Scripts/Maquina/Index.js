﻿$(document).ready(function () {
    /* Variables */

    var grdMaquinas = $("#grdMaquinas");

    crearGrid_grdMaquinas();



    /* Métodos */

    function crearGrid_grdMaquinas() {

        grdMaquinas.kendoGrid({
            columns: [
                {
                    editor: function (container, options) {
                        var contenedor_input = $(div_contenedor_input);
                        var input = $.Editor({
                            atributos: {
                                id: "txtCodigo",
                                maxlength: "20"
                            },
                            tipo: "texto"
                        });

                        if (options.model.MaquinaCodigo !== null) {
                            input.val(options.model.MaquinaCodigo);
                        }

                        contenedor_input.append(input);
                        contenedor_input.appendTo(container);

                        input.change(function () {
                            options.model.MaquinaCodigo = input.val();
                            options.model.dirty = true;
                        });
                    },
                    field: "MaquinaCodigo",
                    filterable: {
                        cell: {
                            operator: "contains",
                            showOperators: false
                        }
                    },
                    title: "Código",
                    width: "120px"
                },
                {
                    editor: function (container, options) {
                        var contenedor_input = $(div_contenedor_input);
                        var input = $.Editor({
                            atributos: {
                                id: "txtDescripcion",
                                maxlength: "100"
                            },
                            tipo: "texto"
                        });

                        if (options.model.MaquinaDescripcion !== null) {
                            input.val(options.model.MaquinaDescripcion);
                        }

                        contenedor_input.append(input);
                        contenedor_input.appendTo(container);

                        input.change(function () {
                            options.model.MaquinaDescripcion = input.val();
                            options.model.dirty = true;
                        });
                    },
                    field: "MaquinaDescripcion",
                    filterable: {
                        cell: {
                            operator: "contains",
                            showOperators: false
                        }
                    },
                    title: "Descripción",
                    width: "200px"
                },
                {
                    editor: function (container, options) {
                        var contenedor_input = $(div_contenedor_input);
                        var input = $('<div id="cmbProcesos" style="width: 100%;"></div>');

                        contenedor_input.append(input);
                        contenedor_input.appendTo(container);

                        input.kendoComboBox({
                            dataSource: {
                                transport: {
                                    read: function (options) {
                                        $.ajax({
                                            url: "/Proceso/ObtenerProcesos",
                                            //data: datosEnviar,
                                            //contentType: "application/json; charset=utf-8",
                                            dataType: "json",
                                            processData: false,
                                            method: "post",
                                            success: function (centros) {
                                                console.log("Se consultaron los Procesos correctamente");
                                                options.success(centros);
                                            },
                                            error: function (xhr) {
                                                options.error();

                                                bootbox.alert({
                                                    title: "No se pudo consultar los Procesos.",
                                                    message: ObtenerErrorServidor(xhr.responseText)
                                                });
                                            }
                                        });
                                    }
                                }
                                //serverFiltering: true
                            },
                            dataTextField: "Descripcion",
                            dataValueField: "Id",
                            delay: 300,
                            filter: 'contains',
                            minLength: 1,
                            placeholder: "Asociar Proceso",
                            select: function (e) {
                                var grid = grdMaquinas.data("kendoGrid");
                                var fila = grdMaquinas.find("tr.k-grid-edit-row")
                                    .first();
                                var registroEditando = grid.dataItem(fila);
                                var procesoSeleccionado = this.dataItem(e.item.index());

                                registroEditando.ProcesoId = procesoSeleccionado.Id;
                                registroEditando.ProcesoDescripcion = procesoSeleccionado.Descripcion;
                                registroEditando.dirty = true;
                            },
                            valuePrimitive: true
                        }).data("kendoComboBox");

                        input.data("kendoComboBox").value(options.model.ProcesoDescripcion);
                    },
                    field: "ProcesoId",
                    filterable: {
                        cell: {
                            operator: "eq",
                            showOperators: false,
                            template: function (args) {
                                args.element.kendoComboBox({
                                    dataSource: {
                                        transport: {
                                            read: function (options_filter) {
                                                $.ajax({
                                                    url: "/Proceso/ObtenerProcesos",
                                                    //data: datosEnviar,
                                                    //contentType: "application/json; charset=utf-8",
                                                    dataType: "json",
                                                    processData: false,
                                                    method: "post",
                                                    success: function (centros) {
                                                        console.log("Se consultaron los Procesos correctamente");
                                                        options_filter.success(centros);
                                                    },
                                                    error: function (xhr) {
                                                        options_filter.error();

                                                        bootbox.alert({
                                                            title: "No se pudo consultar los Procesos.",
                                                            message: ObtenerErrorServidor(xhr.responseText)
                                                        });
                                                    }
                                                });
                                            }
                                        }
                                        //serverFiltering: true
                                    },
                                    dataTextField: "Descripcion",
                                    dataValueField: "Id",
                                    delay: 300,
                                    filter: 'contains',
                                    minLength: 1,
                                    //placeholder: "Filtrar por Centro",
                                    valuePrimitive: true
                                });
                            }
                        }
                    },
                    template: "#= ProcesoDescripcion #",
                    title: "Proceso",
                    width: "180px"
                },
                {
                    editor: function (container, options) {
                        var contenedor_input = $(div_contenedor_input);
                        var input = $('<div id="cmbCentros" style="width: 100%;"></div>');

                        contenedor_input.append(input);
                        contenedor_input.appendTo(container);

                        input.kendoComboBox({
                            dataSource: {
                                transport: {
                                    read: function (options) {
                                        $.ajax({
                                            url: "/Centro/ObtenerCentros",
                                            //data: datosEnviar,
                                            //contentType: "application/json; charset=utf-8",
                                            dataType: "json",
                                            processData: false,
                                            method: "post",
                                            success: function (centros) {
                                                console.log("Se consultaron los Centros correctamente");
                                                options.success(centros);
                                            },
                                            error: function (xhr) {
                                                options.error();

                                                bootbox.alert({
                                                    title: "No se pudo consultar los Centros.",
                                                    message: ObtenerErrorServidor(xhr.responseText)
                                                });
                                            }
                                        });
                                    }
                                }
                                //serverFiltering: true
                            },
                            dataTextField: "Descripcion_Siglas",
                            dataValueField: "Id",
                            delay: 300,
                            filter: 'contains',
                            minLength: 1,
                            placeholder: "Asociar Centro",
                            select: function (e) {
                                var grid = grdMaquinas.data("kendoGrid");
                                var fila = grdMaquinas.find("tr.k-grid-edit-row")
                                    .first();
                                var registroEditando = grid.dataItem(fila);
                                var centroSeleccionado = this.dataItem(e.item.index());

                                registroEditando.CentroId = centroSeleccionado.Id;
                                registroEditando.CentroSiglas = centroSeleccionado.Siglas;
                                registroEditando.CentroDescripcion = centroSeleccionado.Descripcion;
                                registroEditando.dirty = true;
                            },
                            valuePrimitive: true
                        }).data("kendoComboBox");

                        input.data("kendoComboBox").value(options.model.CentroSiglas);
                    },
                    field: "CentroId",
                    filterable: {
                        cell: {
                            operator: "eq",
                            showOperators: false,
                            template: function (args) {
                                args.element.kendoComboBox({
                                    dataSource: {
                                        transport: {
                                            read: function (options_filter) {
                                                $.ajax({
                                                    url: "/Centro/ObtenerCentros",
                                                    //data: datosEnviar,
                                                    //contentType: "application/json; charset=utf-8",
                                                    dataType: "json",
                                                    processData: false,
                                                    method: "post",
                                                    success: function (centros) {
                                                        console.log("Se consultaron los Centros correctamente");
                                                        options_filter.success(centros);
                                                    },
                                                    error: function (xhr) {
                                                        options_filter.error();

                                                        bootbox.alert({
                                                            title: "No se pudo consultar los Centros.",
                                                            message: ObtenerErrorServidor(xhr.responseText)
                                                        });
                                                    }
                                                });
                                            }
                                        }
                                        //serverFiltering: true
                                    },
                                    dataTextField: "Descripcion_Siglas",
                                    dataValueField: "Id",
                                    delay: 300,
                                    filter: 'contains',
                                    minLength: 1,
                                    //placeholder: "Filtrar por Centro",
                                    valuePrimitive: true
                                });
                            }
                        }
                    },
                    template: "#= CentroSiglas #",
                    title: "Centro",
                    width: "180px"
                },
                {
                    editor: function (container, options) {
                        var contenedor_input = $(div_contenedor_input);
                        var input = $.Editor({
                            atributos: {
                                id: "txtVelocidadDisenoSacosHora"
                            },
                            tipo: "numero_entero"
                        });

                        if (options.model.VelocidadDisenoSacosHora !== null) {
                            input.val(options.model.VelocidadDisenoSacosHora);
                        }

                        contenedor_input.append(input);
                        contenedor_input.appendTo(container);

                        input.change(function () {
                            var valor = input.val();

                            if (valor.replace(/\s+/, "").length === 0) {
                                valor = null;
                            }

                            options.model.VelocidadDisenoSacosHora = valor;
                            options.model.dirty = true;
                        });
                    },
                    field: "VelocidadDisenoSacosHora",
                    filterable: false,
                    title: "Velocidad de diseño Sacos/hora",
                    width: "200px"
                },
                {
                    command: [{
                        name: "edit",
                        text: { edit: "Modificar", cancel: "Cancelar", update: "Guardar" }
                    },
                    {
                        name: "eliminar",
                        click: function (e) {
                            e.preventDefault();

                            var grid = this;


                            bootbox.confirm({
                                buttons: {
                                    confirm: {
                                        label: 'Si',
                                        className: 'btn-success'
                                    },
                                    cancel: {
                                        label: 'No',
                                        className: 'btn-danger'
                                    }
                                },
                                message: "¿Realmente desea eliminar la Maquina?",
                                callback: function (result) {
                                    if (result) {
                                        console.log('Eliminando Maquina');

                                        var tr = $(e.target).closest("tr"); // get the current table row (tr)
                                        grid.removeRow(tr);
                                    }
                                }
                            });
                        },
                        text: "Eliminar"
                    }]
                }],
            dataSource: dataSource_grdMaquinas(),
            editable: {
                confirmation: false,
                mode: 'inline'
            },
            filterable: { mode: "row" },
            groupable: false,
            pageable: {
                refresh: true,
                pageSizes: [5, 10, 20]
            },
            resizable: true,
            selectable: "row",
            sortable: true,
            scrollable: true,
            toolbar: [{ name: "create", text: "Crear" }]
        });
    }

    function dataSource_grdMaquinas() {
        var gridDataSource = new kendo.data.DataSource({
            serverFiltering: true,
            serverPaging: true,
            serverSorting: true,
            transport: {
                read: function (options) {
                    var filtering = [];
                    var sorting = [];

                    if (options.data.filter !== undefined) {
                        $.each(options.data.filter.filters, function (indice, filtro) {
                            filtering.push({ Field: filtro.field, Value: filtro.value, Operator: filtro.operator });
                        });
                    }

                    if (options.data.sort !== undefined) {
                        $.each(options.data.sort, function (indice, orden) {
                            sorting.push({ Field: orden.field, Dir: orden.dir });
                        });
                    }

                    var take = options.data.take;

                    if (take === undefined) {
                        take = 10;
                    }

                    var datosEnviar = {
                        filter: filtering,
                        sort: sorting,
                        skip: options.data.skip,
                        take: take
                    };


                    // TODO: Agregar espera
                    console.log('Consultando Maquinas');

                    $.ajax({
                        url: "/Maquina/ObtenerMaquinas",
                        method: "POST",
                        dataType: "json", // Lo que recibo del servidor
                        contentType: "application/json", // Tipo de datos que envio al servidor 
                        data: JSON.stringify(datosEnviar),
                        complete: function () {
                            // TODO: Terminar espera
                        },
                        success: function (respuesta) {
                            console.log('Maquinas consultadas correctamente');
                            options.success(respuesta);
                        },
                        error: function (xhr) {
                            options.error();

                            bootbox.alert({
                                title: "Ocurrió un error al consultar las Maquinas.",
                                message: ObtenerErrorServidor(xhr.responseText)
                            });
                        }
                    });
                },
                create: function (options) {
                    var grid = grdMaquinas.data("kendoGrid");
                    var modelo = ConvertirModelo(options.data);
                    var errores = ValidarModelo(modelo);

                    if (errores.length > 0) {
                        options.error();

                        bootbox.alert({
                            title: "Tener en cuenta lo siguiente:",
                            message: errores.join("")
                        });

                        return;
                    }

                    console.log("Creando Máquina");

                    $.ajax({
                        url: "/Maquina/Crear",
                        method: "POST",
                        //dataType: "json", // Lo que recibo del servidor
                        contentType: "application/json", // Tipo de datos que envio al servidor
                        data: JSON.stringify(modelo),
                        success: function (respuesta) {
                            console.log('Maquina creada correctamente');

                            options.success();

                            grid.dataSource.read();
                        },
                        error: function (xhr) {
                            options.error();

                            bootbox.alert({
                                title: "Ocurrió un error al crear la Maquina.",
                                message: ObtenerErrorServidor(xhr.responseText)
                            });
                        }
                    });
                },
                update: function (options) {
                    var grid = grdMaquinas.data("kendoGrid");
                    var modelo = ConvertirModelo(options.data);
                    var errores = ValidarModelo(modelo);

                    if (errores.length > 0) {
                        options.error();

                        bootbox.alert({
                            title: "Tener en cuenta lo siguiente:",
                            message: errores.join("")
                        });

                        return;
                    }

                    console.log('Editando Maquina');

                    $.ajax({
                        url: "/Maquina/Editar",
                        method: "POST",
                        //dataType: "json", // Lo que recibo del servidor
                        contentType: "application/json", // Tipo de datos que envio al servidor
                        data: JSON.stringify(modelo),
                        success: function (respuesta) {
                            console.log('Maquina editada correctamente');

                            options.success();

                            grid.dataSource.read();
                        },
                        error: function (xhr) {
                            options.error();

                            bootbox.alert({
                                title: "Ocurrió un error al editar la Maquina.",
                                message: ObtenerErrorServidor(xhr.responseText)
                            });
                        }
                    });
                },
                destroy: function (options) {
                    var grid = grdMaquinas.data("kendoGrid");

                    $.ajax({
                        url: "/Maquina/Eliminar",
                        method: "POST",
                        //dataType: "json", // Lo que recibo del servidor
                        contentType: "application/json", // Tipo de datos que envio al servidor
                        data: JSON.stringify({
                            id: options.data.MaquinaId
                        }),
                        success: function (respuesta) {
                            console.log('Maquina eliminado correctamente');

                            options.success();

                            grid.dataSource.read();
                        },
                        error: function (xhr) {
                            options.error();

                            bootbox.alert({
                                title: "Ocurrió un error al eliminar la Maquina.",
                                message: ObtenerErrorServidor(xhr.responseText)
                            });
                        }
                    });
                }
            },
            pageSize: 10,
            schema: {
                data: 'value',
                model: {
                    id: "MaquinaId",
                    fields: {
                        MaquinaId: { editable: false, type: "number" },
                        MaquinaDescripcion: { editable: true, nullable: false, type: "string" },
                        MaquinaCodigo: { editable: true, nullable: false, type: "string" },
                        CentroId: { editable: true, nullable: false, type: "number" },
                        ProcesoId: { editable: true, nullable: false, type: "number" },
                        ProcesoDescripcion: { editable: true, nullable: true, type: "string" },
                        CentroSiglas: { editable: true, nullable: true, type: "string" },
                        VelocidadDisenoSacosHora: { editable: true, nullable: true, type: "number" }
                        //FechaCreado: { editable: false, format: '{0:dd/MMM/yyyy HH:mm tt}', nullable: true, type: 'date' },
                        //FechaModificado: { editable: false, format: '{0:dd/MMM/yyyy HH:mm tt}', nullable: true, type: 'date' }
                    }
                },
                total: 'count'
            }
        });

        return gridDataSource;
    }

    function ValidarModelo(modelo) {
        var mensajes = [];

        if (modelo.Codigo === null || (modelo.Codigo !== null && modelo.Codigo.replace(/\s+/, "").length === 0)) {
            mensajes.push("<p>El Código no puede estar vacío</p>");
        } else if (modelo.Codigo.length > 20) {
            mensajes.push("<p>El Código puede contener hasta 20 caracteres</p>");
        }

        if (modelo.Descripcion === null || (modelo.Descripcion !== null && modelo.Descripcion.replace(/\s+/, "").length === 0)) {
            mensajes.push("<p>La Descripción no puede estar vacía</p>");
        } else if (modelo.Descripcion.length > 100) {
            mensajes.push("<p>La Descripción puede contener hasta 100 caracteres</p>");
        }

        if (modelo.ProcesoId === null || (modelo.ProcesoId !== null && modelo.ProcesoId === 0)) {
            mensajes.push("<p>El Proceso no puede estár vacío.</p>");
        }

        if (modelo.CentroId === null || (modelo.CentroId !== null && modelo.CentroId === 0)) {
            mensajes.push("<p>El Centro no puede estár vacío.</p>");
        }

        return mensajes;
    }

    function ConvertirModelo(modelo) {
        var nuevoModelo = {
            CentroId: modelo.CentroId,
            ProcesoId: modelo.ProcesoId,
            Id: modelo.MaquinaId,
            VelocidadDisenoSacosHora: modelo.VelocidadDisenoSacosHora
        };

        if (modelo.MaquinaDescripcion !== undefined) {
            nuevoModelo.Descripcion = modelo.MaquinaDescripcion;
        }

        if (modelo.MaquinaCodigo !== undefined) {
            nuevoModelo.Codigo = modelo.MaquinaCodigo;
        }

        return nuevoModelo;
    }
});