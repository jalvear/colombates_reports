﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace capa_web
{
    public class KendoFilter
    {
        public String Field { get; set; }
        public Object Value { get; set; }
        public string Operator { get; set; }

        public static String GetFormatedFilter(List<KendoFilter> filter, List<EnumFormat> enumToCompare)
        {
            String Filtro = String.Empty;

            if (filter != null && filter.Count > 0)
            {
                foreach (KendoFilter kf in filter)
                {
                    if (kf.Value != null && !String.IsNullOrEmpty(kf.Value.ToString()))
                    {
                        foreach (var me in enumToCompare)
                        {
                            if (kf.Field == me.Field)
                            {
                                if (me.Tipo == typeof(DateTime) || me.Tipo == typeof(DateTime?))
                                {
                                    DateTime fecha = Convert.ToDateTime(kf.Value.ToString());

                                    switch (kf.Operator)
                                    {
                                        case "eq":
                                            // Igual a
                                            Filtro += " TO_TIMESTAMP( CONCAT( TO_CHAR( " + me.Value + ", 'dd-MM-yyyy' ), '00:00:00' ), 'DD-MM-RRRR HH24:MI:SS') = TO_TIMESTAMP('" + fecha.ToString("dd-MM-yyyy") + " 00:00:00', 'DD-MM-RRRR HH24:MI:SS') AND";
                                            break;
                                        case "neq":
                                            // No es igual a
                                            Filtro += " TO_TIMESTAMP( CONCAT( TO_CHAR( " + me.Value + ", 'dd-MM-yyyy' ), '00:00:00' ), 'DD-MM-RRRR HH24:MI:SS') != TO_TIMESTAMP('" + fecha.ToString("dd-MM-yyyy") + " 00:00:00', 'DD-MM-RRRR HH24:MI:SS') AND";
                                            break;
                                        case "gte":
                                            // Posterior o igual a
                                            Filtro += " " + me.Value + " >= TO_TIMESTAMP('" + fecha.ToString("dd-MM-yyyy") + " 00:00:00', 'DD-MM-RRRR HH24:MI:SS') AND";
                                            break;
                                        case "gt":
                                            // Es posterior
                                            Filtro += " " + me.Value + " > TO_TIMESTAMP('" + fecha.ToString("dd-MM-yyyy") + " 00:00:00', 'DD-MM-RRRR HH24:MI:SS') AND";
                                            break;
                                        case "lte":
                                            // Anterior o igual a
                                            Filtro += " " + me.Value + " <= TO_TIMESTAMP('" + fecha.ToString("dd-MM-yyyy") + " 00:00:00', 'DD-MM-RRRR HH24:MI:SS') AND";
                                            break;
                                        case "lt":
                                            // Es anterior
                                            Filtro += " " + me.Value + " < TO_TIMESTAMP('" + fecha.ToString("dd-MM-yyyy") + " 00:00:00', 'DD-MM-RRRR HH24:MI:SS') AND";
                                            break;
                                        case "isnull":
                                            // Es null
                                            Filtro += " " + me.Value + " IS NULL AND";
                                            break;
                                        case "isnotnull":
                                            // No es null
                                            Filtro += " " + me.Value + " IS NOT NULL AND";
                                            break;
                                    }
                                    //Filtro += " " + me.Value + " BETWEEN TO_TIMESTAMP('" + fecha.ToString("dd-MM-yyyy") + " 00:00:00', 'DD-MM-RRRR HH24:MI:SS') AND TO_TIMESTAMP('" + fecha.ToString("dd-MM-yyyy") + " 23:59:59', 'DD-MM-RRRR HH24:MI:SS') AND";
                                }
                                else if (me.Tipo == typeof(int))
                                {
                                    int codigo = Convert.ToInt32(kf.Value);
                                    Filtro += " " + me.Value + " = " + codigo + " AND";
                                }
                                else
                                {
                                    Filtro += " LOWER(" + me.Value + ") like('%" + kf.Value.ToString().ToLower() + "%') AND";
                                }
                            }
                        }
                    }
                }

                if (!String.IsNullOrEmpty(Filtro))
                {
                    Filtro = "WHERE " + Filtro;
                    Filtro = Filtro.Substring(0, Filtro.Length - 3);
                }
            }

            return Filtro;

        }
    }

    public class KendoSort
    {
        public String Field { get; set; }
        public Object Dir { get; set; }

        public static String GetFormatedSort(List<KendoSort> sort, List<EnumFormat> enumToCompare)
        {
            String Sort = String.Empty;

            if (sort != null && sort.Count > 0)
            {
                foreach (KendoSort ds in sort)
                {
                    foreach (var me in enumToCompare)
                    {
                        if (ds.Field == me.Field)
                        {

                            Sort += " " + me.Value + " " + ds.Dir.ToString().ToLower() + ",";

                        }
                    }

                }

                if (!String.IsNullOrEmpty(Sort))
                {
                    Sort = "ORDER BY " + Sort;
                    Sort = Sort.Substring(0, Sort.Length - 1);
                }
            }

            return Sort;
        }
    }

    public class EnumFormat
    {
        public String Field { get; set; }
        public Object Value { get; set; }
        public Type Tipo { get; set; }

        public static readonly List<EnumFormat> ENUM_EMPLEADOS = new List<EnumFormat>() {
            new EnumFormat { Field = "EmpleadoFicha", Value = "EmpleadoFicha", Tipo = typeof(string) },
            new EnumFormat { Field = "EmpleadoNombreCompleto", Value = "EmpleadoNombreCompleto", Tipo = typeof(string) },
            new EnumFormat { Field = "CentroId", Value = "CentroId", Tipo = typeof(int) }
        };

        public static readonly List<EnumFormat> ENUM_NOVEDADES = new List<EnumFormat>() {
            new EnumFormat { Field = "EmpleadoFicha", Value = "EmpleadoFicha", Tipo = typeof(string) },
            new EnumFormat { Field = "EmpleadoNombreCompleto", Value = "EmpleadoNombreCompleto", Tipo = typeof(string) },
            new EnumFormat { Field = "Semana", Value = "Semana", Tipo = typeof(int) },
            new EnumFormat { Field = "TipoHoraId", Value = "TipoHoraId", Tipo = typeof(int) },
            new EnumFormat { Field = "TurnoId", Value = "TurnoId", Tipo = typeof(int) },
            new EnumFormat { Field = "CentroId", Value = "CentroId", Tipo = typeof(int) },
            new EnumFormat { Field = "Mes", Value = "Mes", Tipo = typeof(int) },
            new EnumFormat { Field = "NovedadEmpleadoDiasHabiles", Value = "NovedadEmpleadoDiasHabiles", Tipo = typeof(int) },
            new EnumFormat { Field = "NovedadEmpleadoHorasProgramadas", Value = "NovedadEmpleadoHorasProgramadas", Tipo = typeof(int) },
        };

        public static readonly List<EnumFormat> ENUM_EXTRAS = new List<EnumFormat>() {
            new EnumFormat { Field = "Semana", Value = "Semana", Tipo = typeof(int) },
            new EnumFormat { Field = "TipoHoraId", Value = "TipoHoraId", Tipo = typeof(int) },
            new EnumFormat { Field = "CausalExtraId", Value = "CausalExtraId", Tipo = typeof(int) },
            new EnumFormat { Field = "CentroId", Value = "CentroId", Tipo = typeof(int) },
            new EnumFormat { Field = "Mes", Value = "Mes", Tipo = typeof(int) },
            new EnumFormat { Field = "ExtraHorasExtra", Value = "ExtraHorasExtra", Tipo = typeof(int) },
            new EnumFormat { Field = "ExtraFechaRegistro", Value = "ExtraFechaRegistro", Tipo = typeof(DateTime) }
        };

        public static readonly List<EnumFormat> ENUM_MAQUINAS = new List<EnumFormat>() {
            new EnumFormat { Field = "MaquinaCodigo", Value = "MaquinaCodigo", Tipo = typeof(string) },
            new EnumFormat { Field = "MaquinaDescripcion", Value = "MaquinaDescripcion", Tipo = typeof(string) },
            new EnumFormat { Field = "CentroId", Value = "CentroId", Tipo = typeof(int) },
            new EnumFormat { Field = "ProcesoId", Value = "ProcesoId", Tipo = typeof(int) }
        };

        public static readonly List<EnumFormat> ENUM_HORAS_PROGRAMADAS = new List<EnumFormat>() {
            new EnumFormat { Field = "Anio", Value = "Anio", Tipo = typeof(int) },
            new EnumFormat { Field = "Mes", Value = "Mes", Tipo = typeof(int) },
            new EnumFormat { Field = "Horas", Value = "Horas", Tipo = typeof(int) },
            new EnumFormat { Field = "CentroId", Value = "CentroId", Tipo = typeof(int) }
        };

        public static readonly List<EnumFormat> ENUM_OBJETIVOS = new List<EnumFormat>() {
            new EnumFormat { Field = "ObjetivoNombre", Value = "ObjetivoNombre", Tipo = typeof(string) },
            new EnumFormat { Field = "ReporteId", Value = "ReporteId", Tipo = typeof(int) }
        };

        public static readonly List<EnumFormat> ENUM_CAUSALES_EXTRAS = new List<EnumFormat>() {
            new EnumFormat { Field = "CausalDescripcion", Value = "CausalDescripcion", Tipo = typeof(string) },
            new EnumFormat { Field = "GrupoId", Value = "GrupoId", Tipo = typeof(int) }
        };

        public static readonly List<EnumFormat> ENUM_EMPLEADOS_SEMANA = new List<EnumFormat>() {
            new EnumFormat { Field = "Anio", Value = "Anio", Tipo = typeof(int) },
            new EnumFormat { Field = "Semana", Value = "Semana", Tipo = typeof(int) },
            new EnumFormat { Field = "CentroId", Value = "CentroId", Tipo = typeof(int) }
        };

        public static readonly List<EnumFormat> ENUM_CORTE_NOMINA_PLANTA = new List<EnumFormat>() {
            new EnumFormat { Field = "Anio", Value = "anio", Tipo = typeof(int) },
            new EnumFormat { Field = "Mes", Value = "mes", Tipo = typeof(int) },
            new EnumFormat { Field = "Periodo", Value = "periodo", Tipo = typeof(int) },
            new EnumFormat { Field = "FechaInicio", Value = "fecha_inicio", Tipo = typeof(DateTime) },
            new EnumFormat { Field = "FechaFin", Value = "fecha_fin", Tipo = typeof(DateTime) }
        };
    }

    public class PaginingDataSourceKendo
    {
        public Object value { get; set; }
        public int count { get; set; }
    }
}
