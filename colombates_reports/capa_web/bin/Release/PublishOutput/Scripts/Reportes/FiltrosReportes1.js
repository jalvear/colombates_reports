﻿$(function () {
    // Variables
    var reporte = $("#btnGenerarReporte").data("reporte");

    // Controles
    var btnGenerarReporte = $("#btnGenerarReporte");
    var btnExportarPDF = $("#btnExportarPDF");
    var btnExportarImagen = $("#btnExportarImagen");


    var txtFechaInicio = $("#txtFechaInicio")
        .kendoDatePicker({ format: '{0:dd/MMM/yyyy}', value: adicionar_meses_a_fecha_inicio_mes(new Date(), -1) })
        .data("kendoDatePicker");

    var txtFechaFin = $("#txtFechaFin")
        .kendoDatePicker({ format: '{0:dd/MMM/yyyy}', value: new Date() })
        .data("kendoDatePicker");

    // Eventos
    btnGenerarReporte.click(function () {
        var errores = validar_antes_generar();

        if (errores.length > 0) {

            bootbox.alert({
                title: "Tener en cuenta lo siguiente:",
                message: errores.join("")
            });

            return;
        }

        var fechaInicio = new Date(
            txtFechaInicio.value().getFullYear(),
            txtFechaInicio.value().getMonth(),
            txtFechaInicio.value().getDate(),
            0, 0, 0);

        var fechaFin = new Date(
            txtFechaFin.value().getFullYear(),
            txtFechaFin.value().getMonth(),
            txtFechaFin.value().getDate(),
            23, 59, 59);

        var datosEnviar = {
            fechaInicio: fechaInicio,
            fechaFin: fechaFin
        };

        var url = "/" + reporte + "/ObtenerDatos";

        // TODO: Agregar espera
        console.log('Generando Reporte');

        $.ajax({
            url: url,
            method: "POST",
            dataType: "json", // Lo que recibo del servidor
            contentType: "application/json", // Tipo de datos que envio al servidor 
            data: JSON.stringify(datosEnviar),
            complete: function () {
                // TODO: Terminar espera
            },
            success: function (respuesta) {
                console.log('Reporte generado correctamente');

                var grafica = $("#grafica1").data("kendoChart");

                grafica.dataSource.data(respuesta);
            },
            error: function (xhr) {

                bootbox.alert({
                    title: "Ocurrió un error al generar el reporte.",
                    message: ObtenerErrorServidor(xhr.responseText)
                });
            }
        });
    });

    btnExportarPDF.click(function () {
        var grafica = $("#grafica1").data("kendoChart");

        if (grafica.dataSource.data().length === 0) {
            bootbox.alert({
                title: "Alerta",
                message: "Por favor genere primero el reporte."
            });

            return;
        }

        grafica.exportPDF(/*{ paperSize: "A5", landscape: true }*/).done(function (data) {
            kendo.saveAs({
                dataURI: data,
                fileName: reporte + "_" + kendo.format("{0:ddMMyyyy_hhmmss}", new Date()) + ".pdf"
            });
        });
    });

    btnExportarImagen.click(function () {
        var grafica = $("#grafica1").data("kendoChart");

        if (grafica.dataSource.data().length === 0) {
            bootbox.alert({
                title: "Alerta",
                message: "Por favor genere primero el reporte."
            });

            return;
        }

        grafica.exportImage().done(function (data) {
            kendo.saveAs({
                dataURI: data,
                fileName: reporte + "_" + kendo.format("{0:ddMMyyyy_hhmmss}", new Date()) + ".png"
            });
        });
    });

    // Funciones
    function validar_antes_generar() {
        var errores = [];

        if (reporte === undefined || reporte === null) {
            errores.push("<p>No se especificó el reporte a generar.</p>");
        }

        if (txtFechaInicio.value() === null) {
            errores.push("<p>La Fecha de Inicio no puede estar vacía.</p>");
        }

        if (txtFechaFin.value() === null) {
            errores.push("<p>La Fecha de Fin no puede estar vacía.</p>");
        }

        return errores;
    }
});