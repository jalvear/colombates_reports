﻿using capa_logica_negocio.Maestras;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace capa_web.Controllers
{
    // Sólo pueden acceder usuarios que han iniciado sesión
    [Authorize]
    public class TipoHoraController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult ObtenerTiposHora()
        {
            return Json(TipoHoraModel.ObtenerTiposHora());
        }

        public void Crear(TipoHoraModel registroNuevo)
        {
            TipoHoraModel.Crear(registroNuevo);
        }

        public void Editar(TipoHoraModel registroEditar)
        {
            TipoHoraModel.Editar(registroEditar);
        }

        public void Eliminar(int id)
        {
            TipoHoraModel.Eliminar(id);
        }
    }
}
