﻿using capa_logica_negocio.Maestras;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace capa_web.Controllers
{
    // Sólo pueden acceder usuarios que han iniciado sesión
    [Authorize]
    public class ExtraController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult ObtenerExtras(int skip, int take, List<KendoFilter> filter, List<KendoSort> sort)
        {
            string filtroKendo = KendoFilter.GetFormatedFilter(filter, EnumFormat.ENUM_EMPLEADOS);
            string ordenadoKendo = KendoSort.GetFormatedSort(sort, EnumFormat.ENUM_EMPLEADOS);

            PaginingDataSourceKendo pdsk = new PaginingDataSourceKendo()
            {
                value = ExtraModel.ObtenerExtras(filtroKendo, ordenadoKendo, skip, take),
                count = ExtraModel.ObtenerCantidadExtras(filtroKendo)
            };

            return Json(pdsk);
        }

        public void Crear(ExtraModel registroNuevo)
        {
            ExtraModel.Crear(registroNuevo);
        }

        public void Editar(ExtraModel registroEditar)
        {
            ExtraModel.Editar(registroEditar);
        }

        public void Eliminar(int id)
        {
            ExtraModel.Eliminar(id);
        }
    }
}
