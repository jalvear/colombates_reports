﻿using capa_logica_negocio.Maestras;
using System.Collections.Generic;
using System.Web.Mvc;

namespace capa_web.Controllers
{
    // Sólo pueden acceder usuarios que han iniciado sesión
    [Authorize]
    public class CorteNominaPlantaController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult ObtenerCorteNominaPlanta(int skip, int take, List<KendoFilter> filter, List<KendoSort> sort)
        {
            string filtroKendo = KendoFilter.GetFormatedFilter(filter, EnumFormat.ENUM_CORTE_NOMINA_PLANTA);
            string ordenadoKendo = KendoSort.GetFormatedSort(sort, EnumFormat.ENUM_CORTE_NOMINA_PLANTA);

            PaginingDataSourceKendo pdsk = new PaginingDataSourceKendo()
            {
                value = CorteNominaPlantaModel.ObtenerCorteNominaPlanta(filtroKendo, ordenadoKendo, skip, take),
                count = CorteNominaPlantaModel.ObtenerCantidadCorteNominaPlanta(filtroKendo)
            };

            return Json(pdsk);
        }

        public void Crear(CorteNominaPlantaModel registroNuevo)
        {
            CorteNominaPlantaModel.Crear(registroNuevo);
        }

        public void Editar(CorteNominaPlantaModel registroEditar)
        {
            CorteNominaPlantaModel.Editar(registroEditar);
        }

        public void Eliminar(int id)
        {
            CorteNominaPlantaModel.Eliminar(id);
        }
    }
}
