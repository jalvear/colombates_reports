﻿using capa_logica_negocio.Maestras;
using System.Collections.Generic;
using System.Web.Mvc;

namespace capa_web.Controllers
{
    // Sólo pueden acceder usuarios que han iniciado sesión
    [Authorize]
    public class EmpleadoSemanaController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult ObtenerEmpleadosSemana(int skip, int take, List<KendoFilter> filter, List<KendoSort> sort)
        {
            string filtroKendo = KendoFilter.GetFormatedFilter(filter, EnumFormat.ENUM_EMPLEADOS_SEMANA);
            string ordenadoKendo = KendoSort.GetFormatedSort(sort, EnumFormat.ENUM_EMPLEADOS_SEMANA);

            PaginingDataSourceKendo pdsk = new PaginingDataSourceKendo()
            {
                value = EmpleadoSemanaModel.ObtenerEmpleadosSemana(filtroKendo, ordenadoKendo, skip, take),
                count = EmpleadoSemanaModel.ObtenerCantidadEmpleadosSemana(filtroKendo)
            };

            return Json(pdsk);
        }

        public void Crear(EmpleadoSemanaModel registroNuevo)
        {
            EmpleadoSemanaModel.Crear(registroNuevo);
        }

        public void Editar(EmpleadoSemanaModel registroEditar)
        {
            EmpleadoSemanaModel.Editar(registroEditar);
        }

        public void Eliminar(int id)
        {
            EmpleadoSemanaModel.Eliminar(id);
        }
    }
}
