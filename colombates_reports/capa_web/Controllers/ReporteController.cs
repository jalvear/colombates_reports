﻿using capa_logica_negocio.Maestras;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace capa_web.Controllers
{
    public class ReporteController : Controller
    {
        // GET: Reporte
        public ActionResult Index()
        {
            return View("~/Views/Shared/NoHayNadaAqui");
        }

        [HttpPost]
        public JsonResult ObtenerNombresReportes()
        {
            return Json(ReporteModel.ObtenerReportes());
        }

        public ActionResult HorasExtraMes()
        {
            return View();
        }

        public ActionResult HorasExtraPlantaAcumulada()
        {
            return View();
        }

        public ActionResult HorasHombrePlantaSacos()
        {
            return View();
        }

        public ViewResult General()
        {
            return View();
        }
    }
}