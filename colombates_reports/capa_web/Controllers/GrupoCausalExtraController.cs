﻿using capa_logica_negocio.Maestras;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace capa_web.Controllers
{
    // Sólo pueden acceder usuarios que han iniciado sesión
    [Authorize]
    public class GrupoCausalExtraController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult ObtenerGruposCausalesExtras()
        {
            return Json(GrupoCausalExtraModel.ObtenerGruposCausalesExtras());
        }

        public void Crear(GrupoCausalExtraModel registroNuevo)
        {
            GrupoCausalExtraModel.Crear(registroNuevo);
        }

        public void Editar(GrupoCausalExtraModel registroEditar)
        {
            GrupoCausalExtraModel.Editar(registroEditar);
        }

        public void Eliminar(int id)
        {
            GrupoCausalExtraModel.Eliminar(id);
        }
    }
}
