﻿using capa_logica_negocio.Maestras;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace capa_web.Controllers
{
    // Sólo pueden acceder usuarios que han iniciado sesión
    [Authorize]
    public class ObjetivoController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult ObtenerObjetivos(int skip, int take, List<KendoFilter> filter, List<KendoSort> sort)
        {
            string filtroKendo = KendoFilter.GetFormatedFilter(filter, EnumFormat.ENUM_OBJETIVOS);
            string ordenadoKendo = KendoSort.GetFormatedSort(sort, EnumFormat.ENUM_OBJETIVOS);

            PaginingDataSourceKendo pdsk = new PaginingDataSourceKendo()
            {
                value = ObjetivoModel.ObtenerObjetivos(filtroKendo, ordenadoKendo, skip, take),
                count = ObjetivoModel.ObtenerCantidadObjetivos(filtroKendo)
            };

            return Json(pdsk);
        }

        public void Crear(ObjetivoModel registroNuevo)
        {
            ObjetivoModel.Crear(registroNuevo);
        }

        public void Editar(ObjetivoModel registroEditar)
        {
            ObjetivoModel.Editar(registroEditar);
        }

        public void Eliminar(int id)
        {
            ObjetivoModel.Eliminar(id);
        }
    }
}
