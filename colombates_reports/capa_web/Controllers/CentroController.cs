﻿using capa_logica_negocio.Maestras;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace capa_web.Controllers
{
    // Sólo pueden acceder usuarios que han iniciado sesión
    [Authorize]
    public class CentroController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        public JsonResult ObtenerCentros()
        {
            return Json(CentroModel.ObtenerCentros());
        }

        public void Crear(CentroModel registroNuevo)
        {
            CentroModel.Crear(registroNuevo);
        }

        public void Editar(CentroModel registroEditar)
        {
            CentroModel.Editar(registroEditar);
        }

        public void Eliminar(int id)
        {
            CentroModel.Eliminar(id);
        }
    }
}
