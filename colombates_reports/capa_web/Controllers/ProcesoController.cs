﻿using capa_logica_negocio.Maestras;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace capa_web.Controllers
{
    // Sólo pueden acceder usuarios que han iniciado sesión
    [Authorize]
    public class ProcesoController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult ObtenerProcesos()
        {
            return Json(ProcesoModel.ObtenerProcesos());
        }

        public void Crear(ProcesoModel registroNuevo)
        {
            ProcesoModel.Crear(registroNuevo);
        }

        public void Editar(ProcesoModel registroEditar)
        {
            ProcesoModel.Editar(registroEditar);
        }

        public void Eliminar(int id)
        {
            ProcesoModel.Eliminar(id);
        }
    }
}
