﻿using capa_logica_negocio.Maestras;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace capa_web.Controllers
{
    // Sólo pueden acceder usuarios que han iniciado sesión
    [Authorize]
    public class CausalExtraController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult ObtenerCausalesExtras(int skip, int take, List<KendoFilter> filter, List<KendoSort> sort)
        {
            string filtroKendo = KendoFilter.GetFormatedFilter(filter, EnumFormat.ENUM_CAUSALES_EXTRAS);
            string ordenadoKendo = KendoSort.GetFormatedSort(sort, EnumFormat.ENUM_CAUSALES_EXTRAS);

            PaginingDataSourceKendo pdsk = new PaginingDataSourceKendo()
            {
                value = CausalExtraModel.ObtenerCausalesExtras(filtroKendo, ordenadoKendo, skip, take),
                count = CausalExtraModel.ObtenerCantidadCausalesExtras(filtroKendo)
            };

            return Json(pdsk);
        }

        public void Crear(CausalExtraModel registroNuevo)
        {
            CausalExtraModel.Crear(registroNuevo);
        }

        public void Editar(CausalExtraModel registroEditar)
        {
            CausalExtraModel.Editar(registroEditar);
        }

        public void Eliminar(int id)
        {
            CausalExtraModel.Eliminar(id);
        }
    }
}
