﻿using capa_logica_negocio.Maestras;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace capa_web.Controllers
{
    // Sólo pueden acceder usuarios que han iniciado sesión
    [Authorize]
    public class MaquinaController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        public JsonResult ObtenerMaquinas(int skip, int take, List<KendoFilter> filter, List<KendoSort> sort)
        {
            string filtroKendo = KendoFilter.GetFormatedFilter(filter, EnumFormat.ENUM_MAQUINAS);
            string ordenadoKendo = KendoSort.GetFormatedSort(sort, EnumFormat.ENUM_MAQUINAS);

            PaginingDataSourceKendo pdsk = new PaginingDataSourceKendo()
            {
                value = MaquinaModel.ObtenerMaquinas(filtroKendo, ordenadoKendo, skip, take),
                count = MaquinaModel.ObtenerCantidadMaquinas(filtroKendo)
            };

            return Json(pdsk);
        }

        public void Crear(MaquinaModel registroNuevo)
        {
            MaquinaModel.Crear(registroNuevo);
        }

        public void Editar(MaquinaModel registroEditar)
        {
            MaquinaModel.Editar(registroEditar);
        }

        public void Eliminar(int id)
        {
            MaquinaModel.Eliminar(id);
        }
    }
}
