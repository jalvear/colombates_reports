﻿using capa_logica_negocio.Maestras;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace capa_web.Controllers
{
    // Sólo pueden acceder usuarios que han iniciado sesión
    [Authorize]
    public class TurnoController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult ObtenerTurnos()
        {
            return Json(TurnoModel.ObtenerTurnos());
        }

        public void Crear(TurnoModel registroNuevo)
        {
            TurnoModel.Crear(registroNuevo);
        }

        public void Editar(TurnoModel registroEditar)
        {
            TurnoModel.Editar(registroEditar);
        }

        public void Eliminar(int id)
        {
            TurnoModel.Eliminar(id);
        }
    }
}
