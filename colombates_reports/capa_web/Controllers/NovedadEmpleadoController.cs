﻿using capa_logica_negocio.Maestras;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace capa_web.Controllers
{
    // Sólo pueden acceder usuarios que han iniciado sesión
    [Authorize]
    public class NovedadEmpleadoController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult ObtenerNovedades(int skip, int take, List<KendoFilter> filter, List<KendoSort> sort)
        {
            //if (skip > 0)
            //{
            //    skip++;
            //    take--;
            //}

            //take = skip + take;

            string filtroKendo = KendoFilter.GetFormatedFilter(filter, EnumFormat.ENUM_EMPLEADOS);
            string ordenadoKendo = KendoSort.GetFormatedSort(sort, EnumFormat.ENUM_EMPLEADOS);

            PaginingDataSourceKendo pdsk = new PaginingDataSourceKendo()
            {
                value = NovedadEmpleadoModel.ObtenerNovedades(filtroKendo, ordenadoKendo, skip, take),
                count = NovedadEmpleadoModel.ObtenerCantidadNovedades(filtroKendo)
            };

            return Json(pdsk);
        }

        public void Crear(int[] variosId, NovedadEmpleadoModel registroNuevo)
        {
            NovedadEmpleadoModel.Crear(variosId, registroNuevo);
        }

        public void Editar(NovedadEmpleadoModel registroEditar)
        {
            NovedadEmpleadoModel.Editar(registroEditar);
        }

        public void Eliminar(int id)
        {
            NovedadEmpleadoModel.Eliminar(id);
        }
    }
}
