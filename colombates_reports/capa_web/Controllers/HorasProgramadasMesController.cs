﻿using capa_logica_negocio.Maestras;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace capa_web.Controllers
{
    [Authorize]
    // Sólo pueden acceder usuarios que han iniciado sesión
    public class HorasProgramadasMesController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult ObtenerHorasProgramadas(int skip, int take, List<KendoFilter> filter, List<KendoSort> sort)
        {
            string filtroKendo = KendoFilter.GetFormatedFilter(filter, EnumFormat.ENUM_HORAS_PROGRAMADAS);
            string ordenadoKendo = KendoSort.GetFormatedSort(sort, EnumFormat.ENUM_HORAS_PROGRAMADAS);

            PaginingDataSourceKendo pdsk = new PaginingDataSourceKendo()
            {
                value = HorasProgramadasMesModel.ObtenerHorasProgramadas(filtroKendo, ordenadoKendo, skip, take),
                count = HorasProgramadasMesModel.ObtenerCantidadHorasProgramadas(filtroKendo)
            };

            return Json(pdsk);
        }

        public void Crear(HorasProgramadasMesModel registroNuevo)
        {
            HorasProgramadasMesModel.Crear(registroNuevo);
        }

        public void Editar(HorasProgramadasMesModel registroEditar)
        {
            HorasProgramadasMesModel.Editar(registroEditar);
        }

        public void Eliminar(int id)
        {
            HorasProgramadasMesModel.Eliminar(id);
        }
    }
}