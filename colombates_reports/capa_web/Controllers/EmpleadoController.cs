﻿using capa_logica_negocio.Maestras;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace capa_web.Controllers
{
    // Sólo pueden acceder usuarios que han iniciado sesión
    [Authorize]
    public class EmpleadoController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult ObtenerEmpleados(int skip, int take, List<KendoFilter> filter, List<KendoSort> sort)
        {
            string filtroKendo = KendoFilter.GetFormatedFilter(filter, EnumFormat.ENUM_EMPLEADOS);
            string ordenadoKendo = KendoSort.GetFormatedSort(sort, EnumFormat.ENUM_EMPLEADOS);

            PaginingDataSourceKendo pdsk = new PaginingDataSourceKendo()
            {
                value = EmpleadoModel.ObtenerEmpleados(filtroKendo, ordenadoKendo, skip, take),
                count = EmpleadoModel.ObtenerCantidadEmpleados(filtroKendo)
            };

            return Json(pdsk);
        }

        [HttpPost]
        public JsonResult FiltrarPor_Ficha_Nombre(string filtro)
        {
            return Json(EmpleadoModel.FiltrarPor_Ficha_Nombre(filtro));
        }

        public void Crear(EmpleadoModel registroNuevo)
        {
            EmpleadoModel.Crear(registroNuevo);
        }

        public void Editar(EmpleadoModel registroEditar)
        {
            EmpleadoModel.Editar(registroEditar);
        }

        public void Eliminar(int id)
        {
            EmpleadoModel.Eliminar(id);
        }
    }
}
