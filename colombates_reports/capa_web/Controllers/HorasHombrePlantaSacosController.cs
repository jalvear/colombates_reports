﻿using capa_logica_negocio.Reportes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace capa_web.Controllers
{
    public class HorasHombrePlantaSacosController : Controller
    {
        [HttpPost]
        public JsonResult ObtenerDatosMensual(int anio)
        {
            return Json(HorasHombrePlantaSacos.ObtenerDatosMensual(anio, true));
        }

        [HttpPost]
        public JsonResult ObtenerDatosSemanal(int anio, int[] semanas)
        {
            if (semanas == null)
            {
                throw new Exception("Por favor especifique las semenas a consultar");
            }

            return Json(HorasHombrePlantaSacos.ObtenerDatosSemanal(anio, semanas));
        }
    }
}