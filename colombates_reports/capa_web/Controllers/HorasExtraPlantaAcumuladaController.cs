﻿using capa_logica_negocio.Reportes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace capa_web.Controllers
{
    public class HorasExtraPlantaAcumuladaController : Controller
    {
        [HttpPost]
        public JsonResult ObtenerDatos(DateTime fechaInicio, DateTime fechaFin)
        {
            return Json(HorasExtraPlantaAcumulada.ObtenerDatos(fechaInicio, fechaFin));
        }
    }
}