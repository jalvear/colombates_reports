﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(capa_web.Startup))]
namespace capa_web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
