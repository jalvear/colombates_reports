﻿using capa_datos;
using capa_logica_negocio.Utilidades;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace capa_logica_negocio.Reportes
{
    public class HorasHombrePlantaSacos
    {
        public string Mes_Nombre { get; set; }
        public decimal? Mes_Valor { get; set; }
        public string Objetivo_Nombre { get; set; }
        public decimal? Objetivo_Valor { get; set; }

        /// <summary>
        /// Consulta la información para mostrar en los reportes 
        /// HORAS HOMBRE PLANTA SACOS. Se hace Mensual.
        /// </summary>
        public static List<HorasHombrePlantaSacos> ObtenerDatosMensual(int anio, bool calcularObjetivos = false)
        {
            using (Entities contexto = new Entities())
            {
                string nombreObjetivo = string.Format("Objetivo {0}", anio);

                List<HorasHombrePlantaSacos> datos = new List<HorasHombrePlantaSacos>();

                string[] siglasTurno = new string[] { "I", "V", "P" };
                string[] procesos = new string[]
                {
                    "Fondeadoras", "Cosedoras", "Doble Fold"
                };

                List<IGrouping<int?, vw_novedad_empleados>> novedadesAnio =
                    (from vw in contexto.vw_novedad_empleados
                     where vw.NovedadEmpleadoFechaRegistro.Year == anio
                         && vw.CentroSiglas.ToUpper().Equals("S")
                         && siglasTurno.Contains(vw.TurnoSiglas.ToUpper())
                     group vw by vw.Mes).ToList();


                var extrasAnio = (from ex in contexto.vw_extras
                                  where ex.ExtraFechaRegistro.Year == anio
                                    && ex.CentroSiglas.ToUpper().Equals("S")
                                  select ex).ToList();

                var produccionAnio = (from pr in contexto.Tiempo_Maquina
                                      join maq in contexto.vw_maquinas
                                        on pr.recurso.ToUpper() equals maq.MaquinaDescripcion.ToUpper()
                                      where pr.fecha.Year == anio
                                        && procesos.Contains(maq.ProcesoDescripcion)
                                      select pr).ToList();

                foreach (var grupo in novedadesAnio)
                {
                    var registro = new HorasHombrePlantaSacos()
                    {
                        Objetivo_Nombre = nombreObjetivo,
                        Objetivo_Valor = 0
                    };

                    registro.Mes_Nombre = ModificacionTexto.PrimeraLetraMayuscula(
                            grupo.FirstOrDefault().NovedadEmpleadoFechaRegistro
                            .ToString("MMMM", CultureInfo.GetCultureInfo("es-CO"))
                        );

                    double horasProgramadas = (from h in contexto.vw_horas_programadas_mes
                                               where h.CentroSiglas.ToUpper().Equals("S")
                                                && h.Mes == grupo.Key.Value
                                               select h.Horas).FirstOrDefault();

                    int horasNovedades = grupo.Sum(h => h.NovedadEmpleadoHorasProgramadas.Value);

                    if (horasNovedades > horasProgramadas)
                    {
                        throw new Exception(string.Format("Las horas de novedades son mayores a las programadas para el mes de {0}", registro.Mes_Nombre));
                    }

                    double horasNormales = horasProgramadas - horasNovedades;
                    double extrasNormales = 0;
                    double extrasFestivas = 0;

                    if (extrasAnio.Count > 0)
                    {
                        extrasNormales = (from ex in extrasAnio
                                          where ex.Mes == grupo.Key
                                             && ex.TipoHoraSiglas.ToUpper().Equals("N")
                                          select ex.ExtraHorasExtra).Sum();

                        extrasFestivas = (from ex in extrasAnio
                                          where ex.Mes == grupo.Key
                                             && ex.TipoHoraSiglas.ToUpper().Equals("F")
                                          select ex.ExtraHorasExtra).Sum();
                    }

                    double horasHombre = horasNormales + extrasNormales + extrasFestivas;

                    // Pendiente averiguar dato de producción
                    decimal produccion = (from pr in produccionAnio
                                          where pr.fecha.Month == grupo.Key
                                          select pr.t_unit).Sum();

                    decimal horasHombrePlanta = 0;

                    if (produccion > 0)
                    {
                        horasHombrePlanta = produccion / decimal.Parse(horasHombre.ToString("0.###"));
                    }

                    registro.Mes_Valor = horasHombrePlanta;

                    datos.Add(registro);
                }

                if (calcularObjetivos)
                {
                    string nombre_mes = "Enero";
                    decimal valor_mes = 0;

                    if (datos.Count > 0)
                    {
                        nombre_mes = datos.First().Mes_Nombre;
                        valor_mes = datos.First().Mes_Valor.Value;
                    }

                    HorasHombrePlantaSacos realAnioAnterior = new HorasHombrePlantaSacos()
                    {
                        Mes_Nombre = nombre_mes,
                        Mes_Valor = valor_mes
                    };

                    HorasHombrePlantaSacos objetivoAnioActual = new HorasHombrePlantaSacos()
                    {
                        Mes_Nombre = nombre_mes,
                        Mes_Valor = valor_mes
                    };

                    HorasHombrePlantaSacos realAnioActual = new HorasHombrePlantaSacos()
                    {
                        Mes_Nombre = nombre_mes,
                        Mes_Valor = valor_mes
                    };

                    if (anio > 2018)
                    {
                        int anioAnterior = anio - 1;

                        List<HorasHombrePlantaSacos> datosAnioAnterior = ObtenerDatosMensual(anioAnterior);

                        if (datosAnioAnterior.Count > 0)
                        {
                            realAnioAnterior.Objetivo_Valor = datosAnioAnterior
                                .Average(a => a.Mes_Valor);
                        }

                        realAnioAnterior.Objetivo_Nombre = string.Format("Real {0}", anioAnterior);
                    }
                    else
                    {
                        realAnioAnterior.Objetivo_Nombre = "Real 2017";
                        realAnioAnterior.Objetivo_Valor = (
                            from ob in contexto.vw_objetivos
                            where ob.ObjetivoNombre.ToLower().Equals("Real 2017")
                             && ob.ReporteNombre.Equals("HORAS HOMBRE PLANTA SACOS")
                            select ob.ObjetivoValor).FirstOrDefault();
                    }

                    objetivoAnioActual.Objetivo_Nombre = nombreObjetivo;
                    objetivoAnioActual.Objetivo_Valor = (
                            from ob in contexto.vw_objetivos
                            where ob.ObjetivoNombre.ToLower().Equals(nombreObjetivo)
                             && ob.ReporteNombre.Equals("HORAS HOMBRE PLANTA SACOS")
                            select ob.ObjetivoValor).FirstOrDefault();


                    nombreObjetivo = string.Format("Real {0}", anio);

                    realAnioActual.Objetivo_Nombre = nombreObjetivo;

                    if (datos.Count > 0)
                    {
                        realAnioActual.Objetivo_Valor = datos.Average(a => a.Mes_Valor);
                    }

                    datos.Add(realAnioAnterior);
                    datos.Add(objetivoAnioActual);
                    datos.Add(realAnioActual);
                }

                return datos;
            }
        }

        /// <summary>
        /// Consulta la información para mostrar en los reportes 
        /// HORAS HOMBRE PLANTA SEMANAL SACOS. Se hace Semanal.
        /// </summary>
        public static List<HorasHombrePlantaSacos> ObtenerDatosSemanal(int anio, int[] semanas)
        {
            using (Entities contexto = new Entities())
            {
                string nombreObjetivo = string.Format("Objetivo {0}", anio);

                List<HorasHombrePlantaSacos> datos = new List<HorasHombrePlantaSacos>();

                string[] siglasTurno = new string[] { "I", "V", "P" };
                string[] procesos = new string[]
                {
                    "Fondeadoras", "Cosedoras", "Doble Fold"
                };

                List<IGrouping<int?, vw_novedad_empleados>> novedadesAnio =
                    (from vw in contexto.vw_novedad_empleados
                     where vw.NovedadEmpleadoFechaRegistro.Year == anio
                         && (vw.CentroSiglas.ToUpper().Equals("S") || vw.CentroSiglas.ToUpper().Equals("E"))
                     group vw by vw.Semana).ToList();


                var extrasAnio = (from ex in contexto.vw_extras
                                  where ex.ExtraFechaRegistro.Year == anio
                                    && ex.CentroSiglas.ToUpper().Equals("S")
                                  select ex).ToList();

                var produccionAnio = (from pr in contexto.Tiempo_Maquina
                                      join maq in contexto.vw_maquinas
                                        on pr.recurso.ToUpper() equals maq.MaquinaDescripcion.ToUpper()
                                      where pr.fecha.Year == anio
                                        && procesos.Contains(maq.ProcesoDescripcion)
                                      select pr).ToList();

                var empleados_anual = (from em in contexto.vw_empleado_semana
                                       where em.Anio == anio
                                       select em).ToList();

                foreach (var grupo in novedadesAnio)
                {
                    var registro = new HorasHombrePlantaSacos()
                    {
                        Objetivo_Nombre = nombreObjetivo,
                        Objetivo_Valor = 0
                    };

                    registro.Mes_Nombre = grupo.Key.ToString();

                    // Semanal
                    int novedades_centro_c = (from n in grupo
                                              where n.TipoHoraSiglas.ToUpper().Equals("N")
                                                && n.CentroSiglas.ToUpper().Equals("C")
                                              select n.NovedadEmpleadoHorasProgramadas.Value).Sum();

                    int novedades_centro_e = (from n in grupo
                                              where n.TipoHoraSiglas.ToUpper().Equals("N")
                                                && n.CentroSiglas.ToUpper().Equals("E")
                                              select n.NovedadEmpleadoHorasProgramadas.Value).Sum();

                    double horasProgramadas = novedades_centro_c + (0.7 * novedades_centro_e);

                    int horasNovedades = (from g in grupo
                                          where siglasTurno.Contains(g.TurnoSiglas.ToUpper())
                                          select g.NovedadEmpleadoHorasProgramadas.Value).Sum();


                    if (horasNovedades > horasProgramadas)
                    {
                        throw new Exception(string.Format("Las horas de novedades son mayores a las programadas para la semana {0}", registro.Mes_Nombre));
                    }

                    double horasNormales = horasProgramadas - horasNovedades;
                    double extrasNormales = 0;
                    double extrasFestivas = 0;

                    if (extrasAnio.Count > 0)
                    {
                        extrasNormales = (from ex in extrasAnio
                                          where ex.Semana == grupo.Key
                                             && ex.TipoHoraSiglas.ToUpper().Equals("N")
                                          select ex.ExtraHorasExtra).Sum();

                        extrasFestivas = (from ex in extrasAnio
                                          where ex.Semana == grupo.Key
                                             && ex.TipoHoraSiglas.ToUpper().Equals("F")
                                          select ex.ExtraHorasExtra).Sum();
                    }

                    var obj_empleados_semana = (from em in empleados_anual
                                                where em.Semana == em.Semana
                                                select em).FirstOrDefault();

                    int empleados_semana = 0;
                    int? tubos = 0;

                    if (obj_empleados_semana != null)
                    {
                        empleados_semana = obj_empleados_semana.Cantidad;
                        tubos = obj_empleados_semana.Tubos ?? 0;
                    }


                    double horasHombre = horasNormales + extrasNormales + extrasFestivas + empleados_semana;

                    // Pendiente averiguar dato de producción
                    decimal produccion = (from pr in produccionAnio
                                          where TratamientoFecha.GetWeekOfYear(pr.fecha) == grupo.Key
                                          select pr.t_unit).Sum();

                    decimal horasHombrePlanta = 0;

                    if (produccion > 0)
                    {
                        horasHombrePlanta = (produccion + tubos.Value) / decimal.Parse(horasHombre.ToString("0.###"));
                    }

                    registro.Mes_Valor = horasHombrePlanta;

                    datos.Add(registro);
                }

                string nombre_mes = semanas[0].ToString();
                decimal valor_mes = 0;

                if (datos.Count > 0)
                {
                    nombre_mes = datos.First().Mes_Nombre;
                    valor_mes = datos.First().Mes_Valor.Value;
                }

                HorasHombrePlantaSacos realAnioAnterior = new HorasHombrePlantaSacos()
                {
                    Mes_Nombre = nombre_mes,
                    Mes_Valor = valor_mes
                };

                HorasHombrePlantaSacos objetivoAnioActual = new HorasHombrePlantaSacos()
                {
                    Mes_Nombre = nombre_mes,
                    Mes_Valor = valor_mes
                };

                HorasHombrePlantaSacos realAnioActual = new HorasHombrePlantaSacos()
                {
                    Mes_Nombre = nombre_mes,
                    Mes_Valor = valor_mes
                };

                if (anio > 2018)
                {
                    int anioAnterior = anio - 1;

                    List<HorasHombrePlantaSacos> datosAnioAnterior = ObtenerDatosMensual(anioAnterior);

                    if (datosAnioAnterior.Count > 0)
                    {
                        realAnioAnterior.Objetivo_Valor = datosAnioAnterior
                            .Average(a => a.Mes_Valor);
                    }

                    realAnioAnterior.Objetivo_Nombre = string.Format("Real {0}", anioAnterior);
                }
                else
                {
                    realAnioAnterior.Objetivo_Nombre = "Real 2017";
                    realAnioAnterior.Objetivo_Valor = (
                        from ob in contexto.vw_objetivos
                        where ob.ObjetivoNombre.ToLower().Equals("Real 2017")
                         && ob.ReporteNombre.Equals("HORAS HOMBRE PLANTA SEMANAL SACOS")
                        select ob.ObjetivoValor).FirstOrDefault();
                }

                objetivoAnioActual.Objetivo_Nombre = nombreObjetivo;
                objetivoAnioActual.Objetivo_Valor = (
                        from ob in contexto.vw_objetivos
                        where ob.ObjetivoNombre.ToLower().Equals(nombreObjetivo)
                         && ob.ReporteNombre.Equals("HORAS HOMBRE PLANTA SEMANAL SACOS")
                        select ob.ObjetivoValor).FirstOrDefault();


                nombreObjetivo = string.Format("Real {0}", anio);

                realAnioActual.Objetivo_Nombre = nombreObjetivo;

                if (datos.Count > 0)
                {
                    realAnioActual.Objetivo_Valor = datos.Average(a => a.Mes_Valor);
                }

                datos.Add(realAnioAnterior);
                datos.Add(objetivoAnioActual);
                datos.Add(realAnioActual);

                return datos;
            }
        }
    }
}
