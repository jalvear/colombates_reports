﻿using capa_datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace capa_logica_negocio.Reportes
{
    public class HorasExtraPlantaAcumulada
    {
        public string CausalExtra { get; set; }
        public double SumaHorasExtra { get; set; }
        public double Frecuencia { get; set; }
        public double? FrecuenciaAcum { get; set; }

        public static List<HorasExtraPlantaAcumulada> ObtenerDatos(DateTime fechaInicio, DateTime fechaFin)
        {
            using (Entities contexto = new Entities())
            {
                List<HorasExtraPlantaAcumulada> datos = new List<HorasExtraPlantaAcumulada>();

                var consultaAgruapda = contexto.vw_extras
                    .Where(e => e.ExtraFechaRegistro >= fechaInicio
                        && e.ExtraFechaRegistro <= fechaFin)
                    .GroupBy(e => e.TipoHoraDescripcion);

                double totalHorasExtras = consultaAgruapda.Sum(t => t.Sum(h => h.ExtraHorasExtra));
                double? frecuenciaAcum = null;
                bool primeraVez = true;

                foreach (var grupo in consultaAgruapda)
                {
                    HorasExtraPlantaAcumulada registro = new HorasExtraPlantaAcumulada()
                    {
                        CausalExtra = grupo.Key,
                        SumaHorasExtra = grupo.Sum(h => h.ExtraHorasExtra)
                    };

                    registro.Frecuencia = registro.SumaHorasExtra / totalHorasExtras;

                    if (!primeraVez)
                    {
                        frecuenciaAcum += registro.Frecuencia;
                        registro.FrecuenciaAcum = frecuenciaAcum;
                    }
                    else
                    {
                        frecuenciaAcum = 0;
                    }

                    datos.Add(registro);

                    primeraVez = false;
                }

                return datos;
            }
        }
    }
}
