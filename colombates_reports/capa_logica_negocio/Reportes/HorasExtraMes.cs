﻿using capa_datos;
using capa_logica_negocio.Utilidades;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace capa_logica_negocio.Reportes
{
    public class HorasExtraMes
    {
        public string Mes { get; set; }
        public double ExtrasNormales { get; set; }
        public double Festivas { get; set; }

        public static List<HorasExtraMes> ObtenerDatos(DateTime fechaInicio, DateTime fechaFin)
        {
            using (Entities contexto = new Entities())
            {
                List<HorasExtraMes> datos = new List<HorasExtraMes>();

                var consultaAgruapda = contexto.vw_extras
                    .Where(e => e.ExtraFechaRegistro >= fechaInicio
                        && e.ExtraFechaRegistro <= fechaFin)
                    .GroupBy(e => e.Mes);

                foreach (var grupo in consultaAgruapda)
                {
                    datos.Add(new HorasExtraMes()
                    {
                        Mes = ModificacionTexto.PrimeraLetraMayuscula(grupo.FirstOrDefault().ExtraFechaRegistro.ToString("MMMM", CultureInfo.GetCultureInfo("es-CO"))),
                        ExtrasNormales = grupo.Where(t => t.TipoHoraSiglas == "N").Sum(t => t.ExtraHorasExtra),
                        Festivas = grupo.Where(t => t.TipoHoraSiglas == "F").Sum(t => t.ExtraHorasExtra)
                    });
                }

                return datos;
            }
        }
    }
}