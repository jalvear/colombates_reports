﻿using capa_datos;
using capa_datos.Repositorios;
using capa_logica_negocio.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace capa_logica_negocio.Maestras
{
    public class ExtraModel
    {
        public int Id { get; set; }
        public int CausalExtraId { get; set; }
        public int CentroId { get; set; }
        public int TipoHoraId { get; set; }
        public double HorasExtra { get; set; }
        public DateTime FechaRegistro { get; set; }
        public DateTime FechaCreado { get; set; }
        public DateTime FechaModificado { get; set; }

        public static List<ExtraViewModel> ObtenerExtras(string filtroKendo, string ordenadoKendo, int skip = 0, int take = 10)
        {
            using (Entities contexto = new Entities())
            {
                if (string.IsNullOrEmpty(ordenadoKendo))
                {
                    ordenadoKendo = "ORDER BY ExtraFechaRegistro ";
                }

                string consulta = "SELECT * FROM vw_extras " + filtroKendo +
                    ordenadoKendo + " OFFSET @skip ROWS FETCH NEXT @take ROWS ONLY ";

                SqlParameter[] parametros = new SqlParameter[]
                {
                new SqlParameter("@skip", skip),
                new SqlParameter("@take", take)
                };

                return contexto.Database.SqlQuery<ExtraViewModel>(consulta, parametros).ToList();
            }
        }

        public static int ObtenerCantidadExtras(string filtroKendo)
        {
            using (Entities contexto = new Entities())
            {
                string consulta = "SELECT COUNT(ExtraId) FROM vw_extras " + filtroKendo;

                return contexto.Database.SqlQuery<int>(consulta).FirstOrDefault();
            }
        }

        public static void Crear(ExtraModel registroNuevo)
        {
            ExtraRepositorio repositorio = new ExtraRepositorio();

            repositorio.Crear(new Extra()
            {
                centro_id = registroNuevo.CentroId,
                tipo_hora_id = registroNuevo.TipoHoraId,
                causal_id = registroNuevo.CausalExtraId,
                horas_extra = registroNuevo.HorasExtra,
                fecha_registro = registroNuevo.FechaRegistro
            });

            repositorio.GuardarCambios();
        }

        public static void Editar(ExtraModel registroEditar)
        {
            ExtraRepositorio repositorio = new ExtraRepositorio();

            Extra registroNovedad = repositorio.ObtenerPorId(registroEditar.Id);

            if (registroNovedad == null)
            {
                throw new Exception("Este registro de Extras no existe");
            }
            else
            {
                registroNovedad.centro_id = registroEditar.CentroId;
                registroNovedad.causal_id = registroEditar.CausalExtraId;
                registroNovedad.horas_extra = registroEditar.HorasExtra;
                registroNovedad.fecha_registro = registroEditar.FechaRegistro;
                registroNovedad.tipo_hora_id = registroEditar.TipoHoraId;

                repositorio.Editar(registroNovedad);
                repositorio.GuardarCambios();
            }
        }

        public static void Eliminar(int id)
        {
            ExtraRepositorio repositorio = new ExtraRepositorio();

            Extra registroExtra =
                repositorio.ObtenerPorId(id);

            if (registroExtra != null)
            {
                repositorio.Eliminar(registroExtra);
                repositorio.GuardarCambios();
            }
        }
    }
}
