﻿using capa_datos;
using capa_datos.Repositorios;
using capa_logica_negocio.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace capa_logica_negocio.Maestras
{
    public class CausalExtraModel
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
        public int GrupoId { get; set; }
        public DateTime FechaCreado { get; set; }
        public DateTime FechaModificado { get; set; }

        public static List<CausalExtraViewModel> ObtenerCausalesExtras(string filtroKendo, string ordenadoKendo, int skip = 0, int take = 10)
        {
            using (Entities contexto = new Entities())
            {
                if (string.IsNullOrEmpty(ordenadoKendo))
                {
                    ordenadoKendo = "ORDER BY CausalDescripcion ";
                }

                string consulta = "SELECT * FROM vw_causales " + filtroKendo +
                    ordenadoKendo + " OFFSET @skip ROWS FETCH NEXT @take ROWS ONLY ";

                SqlParameter[] parametros = new SqlParameter[]
                {
                new SqlParameter("@skip", skip),
                new SqlParameter("@take", take)
                };

                return contexto.Database.SqlQuery<CausalExtraViewModel>(consulta, parametros).ToList();
            }
        }

        public static int ObtenerCantidadCausalesExtras(string filtroKendo)
        {
            using (Entities contexto = new Entities())
            {
                string consulta = "SELECT COUNT(CausalId) FROM vw_causales " + filtroKendo;

                return contexto.Database.SqlQuery<int>(consulta).FirstOrDefault();
            }
        }

        public static void Crear(CausalExtraModel registroNuevo)
        {
            CausalExtraRepositorio repositorio = new CausalExtraRepositorio();

            Causal_Extra registroExistente =
                repositorio.ObtenerUno(c => registroNuevo.Descripcion.ToLower().Equals(c.descripcion.ToLower()));

            if (registroExistente != null)
            {
                throw new Exception("Ya existe este Causal");
            }

            repositorio.Crear(new Causal_Extra()
            {
                descripcion = registroNuevo.Descripcion,
                grupo_id = registroNuevo.GrupoId
            });

            repositorio.GuardarCambios();
        }

        public static void Editar(CausalExtraModel registroEditar)
        {
            CausalExtraRepositorio repositorio = new CausalExtraRepositorio();

            Causal_Extra registroExistente =
                repositorio.ObtenerUno(c => c.id != registroEditar.Id &&
                registroEditar.Descripcion.ToLower().Equals(c.descripcion.ToLower()));

            if (registroExistente != null)
            {
                throw new Exception("Ya existe este Causal");
            }

            registroExistente = repositorio.ObtenerPorId(registroEditar.Id);

            if (registroExistente == null)
            {
                throw new Exception("El Causal no existe");
            }
            else
            {
                registroExistente.descripcion = registroEditar.Descripcion;
                registroExistente.grupo_id = registroEditar.GrupoId;

                repositorio.Editar(registroExistente);
                repositorio.GuardarCambios();
            }
        }

        public static void Eliminar(int id)
        {
            CausalExtraRepositorio repositorio = new CausalExtraRepositorio();

            Causal_Extra registroExistente =
                repositorio.ObtenerPorId(id);

            if (registroExistente != null)
            {
                repositorio.Eliminar(registroExistente);
                repositorio.GuardarCambios();
            }
        }
    }
}
