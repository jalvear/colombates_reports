﻿using capa_datos;
using capa_datos.Repositorios;
using capa_logica_negocio.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace capa_logica_negocio.Maestras
{
    public class HorasProgramadasMesModel
    {
        public int Id { get; set; }
        public int Anio { get; set; }
        public int Mes { get; set; }
        public int Horas { get; set; }
        public int CentroId { get; set; }
        public DateTime FechaCreado { get; set; }
        public DateTime FechaModificado { get; set; }

        public static List<HorasProgramadasMesViewModel> ObtenerHorasProgramadas(string filtroKendo, string ordenadoKendo, int skip = 0, int take = 10)
        {
            using (Entities contexto = new Entities())
            {
                if (string.IsNullOrEmpty(ordenadoKendo))
                {
                    ordenadoKendo = "ORDER BY Anio DESC, Mes DESC";
                }

                string consulta = "SELECT * FROM vw_horas_programadas_mes " + filtroKendo +
                    ordenadoKendo + " OFFSET @skip ROWS FETCH NEXT @take ROWS ONLY ";

                SqlParameter[] parametros = new SqlParameter[]
                {
                new SqlParameter("@skip", skip),
                new SqlParameter("@take", take)
                };

                return contexto.Database.SqlQuery<HorasProgramadasMesViewModel>(consulta, parametros).ToList();
            }
        }

        public static int ObtenerCantidadHorasProgramadas(string filtroKendo)
        {
            using (Entities contexto = new Entities())
            {
                string consulta = "SELECT COUNT(Id) FROM vw_horas_programadas_mes " + filtroKendo;

                return contexto.Database.SqlQuery<int>(consulta).FirstOrDefault();
            }
        }

        public static void Crear(HorasProgramadasMesModel registroNuevo)
        {
            HorasProgramadasMesRepositorio repositorio = new HorasProgramadasMesRepositorio();

            Horas_Programadas_Mes registroExistente =
                repositorio.ObtenerUno(c => c.anio == registroNuevo.Anio && c.mes == registroNuevo.Mes
                && c.centro_id == registroNuevo.CentroId);

            if (registroExistente != null)
            {
                throw new Exception("Ya existe el valor de Horas Programadas para el Mes y Año especificado");
            }

            repositorio.Crear(new Horas_Programadas_Mes()
            {
                anio = registroNuevo.Anio,
                mes = registroNuevo.Mes,
                horas = registroNuevo.Horas,
                centro_id = registroNuevo.CentroId
            });

            repositorio.GuardarCambios();
        }

        public static void Editar(HorasProgramadasMesModel registroEditar)
        {
            HorasProgramadasMesRepositorio repositorio = new HorasProgramadasMesRepositorio();

            Horas_Programadas_Mes registroExistente =
                repositorio.ObtenerUno(c => c.id != registroEditar.Id &&
                    (c.anio == registroEditar.Anio && c.mes == registroEditar.Mes
                    && c.centro_id == registroEditar.CentroId)
                );

            if (registroExistente != null)
            {
                throw new Exception("Ya existe el valor de Horas Programadas para el Mes y Año especificado");
            }

            registroExistente = repositorio.ObtenerPorId(registroEditar.Id);

            if (registroExistente == null)
            {
                Crear(registroEditar);
            }
            else
            {
                registroExistente.anio = registroEditar.Anio;
                registroExistente.mes = registroEditar.Mes;
                registroExistente.horas = registroEditar.Horas;
                registroExistente.centro_id = registroEditar.CentroId;

                repositorio.Editar(registroExistente);

                repositorio.GuardarCambios();
            }
        }

        public static void Eliminar(int id)
        {
            HorasProgramadasMesRepositorio repositorio = new HorasProgramadasMesRepositorio();

            Horas_Programadas_Mes registroExistente =
                repositorio.ObtenerPorId(id);

            if (registroExistente != null)
            {
                repositorio.Eliminar(registroExistente);
                repositorio.GuardarCambios();
            }
        }
    }
}
