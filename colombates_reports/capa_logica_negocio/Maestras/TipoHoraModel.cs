﻿using capa_datos;
using capa_datos.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace capa_logica_negocio.Maestras
{
    public class TipoHoraModel
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
        public string Siglas { get; set; }
        public DateTime FechaCreado { get; set; }
        public DateTime FechaModificado { get; set; }

        public static List<TipoHoraModel> ObtenerTiposHora()
        {
            using (Entities contexto = new Entities())
            {
                return contexto.TipoHora.OrderBy(c => c.descripcion).Select(c => new TipoHoraModel
                {
                    Id = c.id,
                    Descripcion = c.descripcion,
                    FechaCreado = c.fecha_creado,
                    FechaModificado = c.fecha_modificado,
                    Siglas = c.siglas
                }).ToList();
            }
        }

        public static void Crear(TipoHoraModel registroNuevo)
        {
            TipoHoraRepositorio repositorio = new TipoHoraRepositorio();

            TipoHora tipoHoraExistente =
                repositorio.ObtenerUno(c => registroNuevo.Siglas.ToLower().Equals(c.siglas.ToLower())
                || registroNuevo.Descripcion.ToLower().Equals(c.descripcion.ToLower()));

            if (tipoHoraExistente != null)
            {
                throw new Exception("Ya existe este Tipo de Hora");
            }

            repositorio.Crear(new TipoHora()
            {
                descripcion = registroNuevo.Descripcion,
                siglas = registroNuevo.Siglas
            });

            repositorio.GuardarCambios();
        }

        public static void Editar(TipoHoraModel registroEditar)
        {
            TipoHoraRepositorio repositorio = new TipoHoraRepositorio();

            TipoHora tipoHoraExistente =
                repositorio.ObtenerUno(c => c.id != registroEditar.Id &&
                (registroEditar.Siglas.ToLower().Equals(c.siglas.ToLower())
                || registroEditar.Descripcion.ToLower().Equals(c.descripcion.ToLower())));

            if (tipoHoraExistente != null)
            {
                throw new Exception("Ya existe este Tipo de Hora");
            }

            tipoHoraExistente = repositorio.ObtenerPorId(registroEditar.Id);

            if (tipoHoraExistente == null)
            {
                throw new Exception("El Tipo de Hora no existe");
            }
            else
            {
                tipoHoraExistente.descripcion = registroEditar.Descripcion;
                tipoHoraExistente.siglas = registroEditar.Siglas;

                repositorio.Editar(tipoHoraExistente);

                repositorio.GuardarCambios();
            }
        }

        public static void Eliminar(int id)
        {
            TipoHoraRepositorio repositorio = new TipoHoraRepositorio();

            TipoHora tipoHoraExistente =
                repositorio.ObtenerPorId(id);

            if (tipoHoraExistente != null)
            {
                repositorio.Eliminar(tipoHoraExistente);
                repositorio.GuardarCambios();
            }
        }
    }
}
