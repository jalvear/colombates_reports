﻿using capa_datos;
using capa_datos.Repositorios;
using capa_logica_negocio.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace capa_logica_negocio.Maestras
{
    public class EmpleadoModel
    {
        public int Id { get; set; }
        public string Ficha { get; set; }
        public string NombreCompleto { get; set; }
        public int CentroId { get; set; }
        public DateTime FechaCreado { get; set; }
        public DateTime FechaModificado { get; set; }

        public static List<EmpleadoViewModel> ObtenerEmpleados(string filtroKendo, string ordenadoKendo, int skip = 0, int take = 10)
        {
            using (Entities contexto = new Entities())
            {
                if (string.IsNullOrEmpty(ordenadoKendo))
                {
                    ordenadoKendo = "ORDER BY EmpleadoNombreCompleto ";
                }

                string consulta = "SELECT * FROM vw_empleados " + filtroKendo +
                    ordenadoKendo + " OFFSET @skip ROWS FETCH NEXT @take ROWS ONLY ";

                SqlParameter[] parametros = new SqlParameter[]
                {
                new SqlParameter("@skip", skip),
                new SqlParameter("@take", take)
                };

                return contexto.Database.SqlQuery<EmpleadoViewModel>(consulta, parametros).ToList();
            }
        }

        public static int ObtenerCantidadEmpleados(string filtroKendo)
        {
            using (Entities contexto = new Entities())
            {
                string consulta = "SELECT COUNT(EmpleadoId) FROM vw_empleados " + filtroKendo;

                return contexto.Database.SqlQuery<int>(consulta).FirstOrDefault();
            }
        }

        public static List<EmpleadoViewModel> FiltrarPor_Ficha_Nombre(string filtro)
        {
            EmpleadoRepositorio repositorio = new EmpleadoRepositorio();

            List<EmpleadoViewModel> empleados = repositorio.FiltrarPor_Ficha_Nombre(filtro)
                .Select(e => new EmpleadoViewModel()
                {
                    CentroDescripcion = e.CentroDescripcion,
                    CentroId = e.CentroId,
                    CentroSiglas = e.CentroSiglas,
                    EmpleadoFicha = e.EmpleadoFicha,
                    EmpleadoId = e.EmpleadoId,
                    EmpleadoNombreCompleto = e.EmpleadoNombreCompleto
                }).ToList();

            return empleados;
        }

        public static void Crear(EmpleadoModel registroNuevo)
        {
            EmpleadoRepositorio repositorio = new EmpleadoRepositorio();

            Empleado empleadoExistente =
                repositorio.ObtenerUno(c => registroNuevo.Ficha.ToLower().Equals(c.ficha.ToLower()));

            if (empleadoExistente != null)
            {
                throw new Exception("Ya existe un Empleado con la misma Ficha");
            }

            repositorio.Crear(new Empleado()
            {
                centro_id = registroNuevo.CentroId,
                ficha = registroNuevo.Ficha,
                nombre_completo = registroNuevo.NombreCompleto
            });

            repositorio.GuardarCambios();
        }

        public static void Editar(EmpleadoModel registroEditar)
        {
            EmpleadoRepositorio repositorio = new EmpleadoRepositorio();

            Empleado empleadoExistente =
                repositorio.ObtenerUno(c => c.id != registroEditar.Id &&
                registroEditar.Ficha.ToLower().Equals(c.ficha.ToLower()));

            if (empleadoExistente != null)
            {
                throw new Exception("Ya existe un Empleado con la misma Ficha");
            }

            empleadoExistente = repositorio.ObtenerPorId(registroEditar.Id);

            if (empleadoExistente == null)
            {
                throw new Exception("El Empleado no existe");
            }
            else
            {
                empleadoExistente.centro_id = registroEditar.CentroId;
                empleadoExistente.ficha = registroEditar.Ficha;
                empleadoExistente.nombre_completo = registroEditar.NombreCompleto;

                repositorio.Editar(empleadoExistente);

                repositorio.GuardarCambios();
            }
        }

        public static void Eliminar(int id)
        {
            EmpleadoRepositorio repositorio = new EmpleadoRepositorio();

            Empleado empleadoExistente =
                repositorio.ObtenerPorId(id);

            if (empleadoExistente != null)
            {
                repositorio.Eliminar(empleadoExistente);
                repositorio.GuardarCambios();
            }
        }
    }
}
