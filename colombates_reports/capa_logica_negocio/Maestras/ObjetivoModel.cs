﻿using capa_datos;
using capa_datos.Repositorios;
using capa_logica_negocio.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace capa_logica_negocio.Maestras
{
    public class ObjetivoModel
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public decimal Valor { get; set; }
        public int ReporteId { get; set; }
        public DateTime FechaCreado { get; set; }
        public DateTime FechaModificado { get; set; }

        public static List<ObjetivoViewModel> ObtenerObjetivos(string filtroKendo, string ordenadoKendo, int skip = 0, int take = 10)
        {
            using (Entities contexto = new Entities())
            {
                if (string.IsNullOrEmpty(ordenadoKendo))
                {
                    ordenadoKendo = "ORDER BY ReporteNombre, ObjetivoNombre ";
                }

                string consulta = "SELECT * FROM vw_objetivos " + filtroKendo +
                    ordenadoKendo + " OFFSET @skip ROWS FETCH NEXT @take ROWS ONLY ";

                SqlParameter[] parametros = new SqlParameter[]
                {
                new SqlParameter("@skip", skip),
                new SqlParameter("@take", take)
                };

                return contexto.Database.SqlQuery<ObjetivoViewModel>(consulta, parametros).ToList();
            }
        }

        public static int ObtenerCantidadObjetivos(string filtroKendo)
        {
            using (Entities contexto = new Entities())
            {
                string consulta = "SELECT COUNT(ReporteId) FROM vw_objetivos " + filtroKendo;

                return contexto.Database.SqlQuery<int>(consulta).FirstOrDefault();
            }
        }

        public static void Crear(ObjetivoModel registroNuevo)
        {
            ObjetivoRepositorio repositorio = new ObjetivoRepositorio();

            Objetivo registroExistente =
                repositorio.ObtenerUno(c => registroNuevo.Nombre.ToLower().Equals(c.nombre_objetivo.ToLower())
                && c.reporte_id == registroNuevo.ReporteId
            );

            if (registroExistente != null)
            {
                throw new Exception("Ya existe este Objetivo");
            }

            repositorio.Crear(new Objetivo()
            {
                nombre_objetivo = registroNuevo.Nombre,
                valor = registroNuevo.Valor,
                reporte_id = registroNuevo.ReporteId
            });

            repositorio.GuardarCambios();
        }

        public static void Editar(ObjetivoModel registroEditar)
        {
            ObjetivoRepositorio repositorio = new ObjetivoRepositorio();

            Objetivo registroExistente =
                repositorio.ObtenerUno(c => c.id != registroEditar.Id &&
                (registroEditar.ReporteId == c.reporte_id
                 &&registroEditar.Nombre.ToLower().Equals(c.nombre_objetivo.ToLower()))
            );

            if (registroExistente != null)
            {
                throw new Exception("Ya existe este Objetivo");
            }

            registroExistente = repositorio.ObtenerPorId(registroEditar.Id);

            if (registroExistente == null)
            {
                throw new Exception("El Objetivo no existe");
            }
            else
            {
                registroExistente.nombre_objetivo = registroEditar.Nombre;
                registroExistente.valor = registroEditar.Valor;
                registroExistente.reporte_id = registroEditar.ReporteId;

                repositorio.Editar(registroExistente);
                repositorio.GuardarCambios();
            }
        }

        public static void Eliminar(int id)
        {
            ObjetivoRepositorio repositorio = new ObjetivoRepositorio();

            Objetivo registroExistente =
                repositorio.ObtenerPorId(id);

            if (registroExistente != null)
            {
                repositorio.Eliminar(registroExistente);
                repositorio.GuardarCambios();
            }
        }
    }
}
