﻿using capa_datos;
using capa_datos.Repositorios;
using capa_logica_negocio.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace capa_logica_negocio.Maestras
{
    public class MaquinaModel
    {
        public int Id { get; set; }
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public int CentroId { get; set; }
        public int ProcesoId { get; set; }
        public int? VelocidadDisenoSacosHora { get; set; }
        public DateTime FechaCreado { get; set; }
        public DateTime FechaModificado { get; set; }

        public static List<MaquinaViewModel> ObtenerMaquinas(string filtroKendo, string ordenadoKendo, int skip = 0, int take = 10)
        {
            using (Entities contexto = new Entities())
            {
                if (string.IsNullOrEmpty(ordenadoKendo))
                {
                    ordenadoKendo = "ORDER BY MaquinaDescripcion ";
                }

                string consulta = "SELECT * FROM vw_maquinas " + filtroKendo +
                    ordenadoKendo + " OFFSET @skip ROWS FETCH NEXT @take ROWS ONLY ";

                SqlParameter[] parametros = new SqlParameter[]
                {
                new SqlParameter("@skip", skip),
                new SqlParameter("@take", take)
                };

                return contexto.Database.SqlQuery<MaquinaViewModel>(consulta, parametros).ToList();
            }
        }

        public static int ObtenerCantidadMaquinas(string filtroKendo)
        {
            using (Entities contexto = new Entities())
            {
                string consulta = "SELECT COUNT(MaquinaId) FROM vw_maquinas " + filtroKendo;

                return contexto.Database.SqlQuery<int>(consulta).FirstOrDefault();
            }
        }

        public static void Crear(MaquinaModel registroNuevo)
        {
            MaquinaRepositorio repositorio = new MaquinaRepositorio();

            Maquina registroExistente =
                repositorio.ObtenerUno(c => registroNuevo.Codigo.ToLower().Equals(c.codigo.ToLower()));

            if (registroExistente != null)
            {
                throw new Exception("Ya existe una Maquina con el mismo Código");
            }

            repositorio.Crear(new Maquina()
            {
                centro_id = registroNuevo.CentroId,
                codigo = registroNuevo.Codigo,
                descripcion = registroNuevo.Descripcion,
                proceso_id = registroNuevo.ProcesoId,
                velocidad_diseno_sacos_hora = registroNuevo.VelocidadDisenoSacosHora
            });

            repositorio.GuardarCambios();
        }

        public static void Editar(MaquinaModel registroEditar)
        {
            MaquinaRepositorio repositorio = new MaquinaRepositorio();

            Maquina registroExistente =
                repositorio.ObtenerUno(c => c.id != registroEditar.Id &&
                registroEditar.Codigo.ToLower().Equals(c.codigo.ToLower()));

            if (registroExistente != null)
            {
                throw new Exception("Ya existe una Maquina con el mismo Código");
            }

            registroExistente = repositorio.ObtenerPorId(registroEditar.Id);

            if (registroExistente == null)
            {
                throw new Exception("La Maquina no existe");
            }
            else
            {
                registroExistente.centro_id = registroEditar.CentroId;
                registroExistente.codigo = registroEditar.Codigo;
                registroExistente.descripcion = registroEditar.Descripcion;
                registroExistente.proceso_id = registroEditar.ProcesoId;
                registroExistente.velocidad_diseno_sacos_hora = registroEditar.VelocidadDisenoSacosHora;

                repositorio.Editar(registroExistente);
                repositorio.GuardarCambios();
            }
        }

        public static void Eliminar(int id)
        {
            MaquinaRepositorio repositorio = new MaquinaRepositorio();

            Maquina registroExistente =
                repositorio.ObtenerPorId(id);

            if (registroExistente != null)
            {
                repositorio.Eliminar(registroExistente);
                repositorio.GuardarCambios();
            }
        }
    }
}
