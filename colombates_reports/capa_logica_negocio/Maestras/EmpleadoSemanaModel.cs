﻿using capa_datos;
using capa_datos.Repositorios;
using capa_logica_negocio.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace capa_logica_negocio.Maestras
{
    public class EmpleadoSemanaModel
    {
        public int Id { get; set; }
        public int Anio { get; set; }
        public int Semana { get; set; }
        public int CantidadEmpleados { get; set; }
        public int? Tubos { get; set; }
        public int CentroId { get; set; }
        public DateTime FechaCreado { get; set; }
        public DateTime FechaModificado { get; set; }

        public static List<EmpleadoSemanaViewModel> ObtenerEmpleadosSemana(string filtroKendo, string ordenadoKendo, int skip = 0, int take = 10)
        {
            using (Entities contexto = new Entities())
            {
                if (string.IsNullOrEmpty(ordenadoKendo))
                {
                    ordenadoKendo = "ORDER BY Anio, Semana ";
                }

                string consulta = "SELECT * FROM vw_empleado_semana " + filtroKendo +
                    ordenadoKendo + " OFFSET @skip ROWS FETCH NEXT @take ROWS ONLY ";

                SqlParameter[] parametros = new SqlParameter[]
                {
                new SqlParameter("@skip", skip),
                new SqlParameter("@take", take)
                };

                return contexto.Database.SqlQuery<EmpleadoSemanaViewModel>(consulta, parametros).ToList();
            }
        }

        public static int ObtenerCantidadEmpleadosSemana(string filtroKendo)
        {
            using (Entities contexto = new Entities())
            {
                string consulta = "SELECT COUNT(Id) FROM vw_empleado_semana " + filtroKendo;

                return contexto.Database.SqlQuery<int>(consulta).FirstOrDefault();
            }
        }

        public static void Crear(EmpleadoSemanaModel registroNuevo)
        {
            EmpleadoSemanaRepositorio repositorio = new EmpleadoSemanaRepositorio();

            Empleado_Semana registroExistente =
                repositorio.ObtenerUno(c => c.anio == registroNuevo.Anio && c.semana == registroNuevo.Semana
                    && c.centro_id == registroNuevo.CentroId);

            if (registroExistente != null)
            {
                throw new Exception("Ya existe un registro para el Año, Semana y Centro especificado.");
            }

            repositorio.Crear(new Empleado_Semana()
            {
                anio = registroNuevo.Anio,
                semana = registroNuevo.Semana,
                centro_id = registroNuevo.CentroId,
                cantidad_empleados = registroNuevo.CantidadEmpleados,
                tubos = registroNuevo.Tubos
            });

            repositorio.GuardarCambios();
        }

        public static void Editar(EmpleadoSemanaModel registroEditar)
        {
            EmpleadoSemanaRepositorio repositorio = new EmpleadoSemanaRepositorio();

            Empleado_Semana registroExistente =
                repositorio.ObtenerUno(c => c.id != registroEditar.Id &&
                    (c.anio == registroEditar.Anio && c.semana == registroEditar.Semana
                    && c.centro_id == registroEditar.CentroId));

            if (registroExistente != null)
            {
                throw new Exception("Ya existe un registro para el Año, Semana y Centro especificado.");
            }

            registroExistente = repositorio.ObtenerPorId(registroEditar.Id);

            if (registroExistente == null)
            {
                throw new Exception("El registro no existe");
            }
            else
            {
                registroExistente.anio = registroEditar.Anio;
                registroExistente.semana = registroEditar.Semana;
                registroExistente.centro_id = registroEditar.CentroId;
                registroExistente.cantidad_empleados = registroEditar.CantidadEmpleados;
                registroExistente.tubos = registroEditar.Tubos;

                repositorio.Editar(registroExistente);
                repositorio.GuardarCambios();
            }
        }

        public static void Eliminar(int id)
        {
            EmpleadoSemanaRepositorio repositorio = new EmpleadoSemanaRepositorio();

            Empleado_Semana registroExistente =
                repositorio.ObtenerPorId(id);

            if (registroExistente != null)
            {
                repositorio.Eliminar(registroExistente);
                repositorio.GuardarCambios();
            }
        }
    }
}
