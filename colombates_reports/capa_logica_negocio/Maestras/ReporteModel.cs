﻿using capa_datos;
using capa_datos.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace capa_logica_negocio.Maestras
{
    public class ReporteModel
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public DateTime FechaCreado { get; set; }
        public DateTime FechaModificado { get; set; }

        public static List<ReporteModel> ObtenerReportes()
        {
            using (Entities contexto = new Entities())
            {
                return contexto.Reporte.OrderBy(c => c.nombre_reporte).Select(c => new ReporteModel
                {
                    Id = c.id,
                    Nombre = c.nombre_reporte,
                }).ToList();
            }
        }
    }
}
