﻿using capa_datos;
using capa_datos.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace capa_logica_negocio.Maestras
{
    public class TurnoModel
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
        public string Siglas { get; set; }
        public DateTime FechaCreado { get; set; }
        public DateTime FechaModificado { get; set; }

        public static List<TurnoModel> ObtenerTurnos()
        {
            using (Entities contexto = new Entities())
            {
                return contexto.Turno.OrderBy(c => c.descripcion).Select(c => new TurnoModel
                {
                    Id = c.id,
                    Descripcion = c.descripcion,
                    FechaCreado = c.fecha_creado,
                    FechaModificado = c.fecha_modificado,
                    Siglas = c.siglas
                }).ToList();
            }
        }

        public static void Crear(TurnoModel registroNuevo)
        {
            TurnoRepositorio repositorio = new TurnoRepositorio();

            Turno turnoExistente =
                repositorio.ObtenerUno(c => registroNuevo.Siglas.ToLower().Equals(c.siglas.ToLower())
                || registroNuevo.Descripcion.ToLower().Equals(c.descripcion.ToLower()));

            if (turnoExistente != null)
            {
                throw new Exception("Ya existe este Turno");
            }

            repositorio.Crear(new Turno()
            {
                descripcion = registroNuevo.Descripcion,
                siglas = registroNuevo.Siglas
            });

            repositorio.GuardarCambios();
        }

        public static void Editar(TurnoModel registroEditar)
        {
            TurnoRepositorio repositorio = new TurnoRepositorio();

            Turno turnoExistente =
                repositorio.ObtenerUno(c => c.id != registroEditar.Id &&
                (registroEditar.Siglas.ToLower().Equals(c.siglas.ToLower())
                || registroEditar.Descripcion.ToLower().Equals(c.descripcion.ToLower())));

            if (turnoExistente != null)
            {
                throw new Exception("Ya existe este Turno");
            }

            turnoExistente = repositorio.ObtenerPorId(registroEditar.Id);

            if (turnoExistente == null)
            {
                throw new Exception("El Turno no existe");
            }
            else
            {
                turnoExistente.descripcion = registroEditar.Descripcion;
                turnoExistente.siglas = registroEditar.Siglas;

                repositorio.Editar(turnoExistente);

                repositorio.GuardarCambios();
            }
        }

        public static void Eliminar(int id)
        {
            TurnoRepositorio repositorio = new TurnoRepositorio();

            Turno turnoExistente =
                repositorio.ObtenerPorId(id);

            if (turnoExistente != null)
            {
                repositorio.Eliminar(turnoExistente);
                repositorio.GuardarCambios();
            }
        }
    }
}
