﻿using capa_datos;
using capa_datos.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace capa_logica_negocio.Maestras
{
    public class GrupoCausalExtraModel
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
        public DateTime FechaCreado { get; set; }
        public DateTime FechaModificado { get; set; }

        public static List<GrupoCausalExtraModel> ObtenerGruposCausalesExtras()
        {
            using (Entities contexto = new Entities())
            {
                return contexto.Grupo_Causal_Extra.OrderBy(c => c.descripcion).Select(c => new GrupoCausalExtraModel
                {
                    Id = c.id,
                    Descripcion = c.descripcion,
                    FechaCreado = c.fecha_creado,
                    FechaModificado = c.fecha_modificado
                }).ToList();
            }
        }

        public static void Crear(GrupoCausalExtraModel registroNuevo)
        {
            GrupoCausalExtraRepositorio repositorio = new GrupoCausalExtraRepositorio();

            Grupo_Causal_Extra registroExistente =
                repositorio.ObtenerUno(c => registroNuevo.Descripcion.ToLower().Equals(c.descripcion.ToLower()));

            if (registroExistente != null)
            {
                throw new Exception("Ya existe este Grupo de Causal");
            }

            repositorio.Crear(new Grupo_Causal_Extra()
            {
                descripcion = registroNuevo.Descripcion
            });

            repositorio.GuardarCambios();
        }

        public static void Editar(GrupoCausalExtraModel registroEditar)
        {
            GrupoCausalExtraRepositorio repositorio = new GrupoCausalExtraRepositorio();

            Grupo_Causal_Extra registroExistente =
                repositorio.ObtenerUno(c => c.id != registroEditar.Id &&
                registroEditar.Descripcion.ToLower().Equals(c.descripcion.ToLower()));

            if (registroExistente != null)
            {
                throw new Exception("Ya existe este Grupo de Causal");
            }

            registroExistente = repositorio.ObtenerPorId(registroEditar.Id);

            if (registroExistente == null)
            {
                throw new Exception("El Grupo de Causal no existe");
            }
            else
            {
                registroExistente.descripcion = registroEditar.Descripcion;

                repositorio.Editar(registroExistente);
                repositorio.GuardarCambios();
            }
        }

        public static void Eliminar(int id)
        {
            GrupoCausalExtraRepositorio repositorio = new GrupoCausalExtraRepositorio();

            Grupo_Causal_Extra registroExistente =
                repositorio.ObtenerPorId(id);

            if (registroExistente != null)
            {
                repositorio.Eliminar(registroExistente);
                repositorio.GuardarCambios();
            }
        }
    }
}
