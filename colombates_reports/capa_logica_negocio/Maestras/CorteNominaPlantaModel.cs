﻿using capa_datos;
using capa_datos.Repositorios;
using capa_logica_negocio.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace capa_logica_negocio.Maestras
{
    public class CorteNominaPlantaModel
    {
        public int Id { get; set; }
        public int Anio { get; set; }
        public int Mes { get; set; }
        public int Periodo { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFin { get; set; }
        public DateTime FechaCreado { get; set; }
        public DateTime FechaModificado { get; set; }

        public static List<CorteNominaPlantaModel> ObtenerCorteNominaPlanta(string filtroKendo, string ordenadoKendo, int skip = 0, int take = 10)
        {
            using (Entities contexto = new Entities())
            {
                if (string.IsNullOrEmpty(ordenadoKendo))
                {
                    ordenadoKendo = "ORDER BY anio, mes ";
                }

                string consulta = "SELECT [id] AS Id, [anio] AS Anio, [mes] AS Mes, [periodo] AS Periodo, " +
                    "[fecha_inicio] AS FechaInicio, [fecha_fin] AS FechaFin, " +
                    "[fecha_creado] AS FechaCreado, [fecha_modificado] AS FechaModificado FROM [Corte_Nomina_Planta] " +
                    filtroKendo + ordenadoKendo +
                    " OFFSET @skip ROWS FETCH NEXT @take ROWS ONLY ";

                SqlParameter[] parametros = new SqlParameter[]
                {
                new SqlParameter("@skip", skip),
                new SqlParameter("@take", take)
                };

                return contexto.Database.SqlQuery<CorteNominaPlantaModel>(consulta, parametros).ToList();
            }
        }

        public static int ObtenerCantidadCorteNominaPlanta(string filtroKendo)
        {
            using (Entities contexto = new Entities())
            {
                string consulta = "SELECT COUNT(id) FROM [Corte_Nomina_Planta] " + filtroKendo;

                return contexto.Database.SqlQuery<int>(consulta).FirstOrDefault();
            }
        }

        public static void Crear(CorteNominaPlantaModel registroNuevo)
        {
            CorteNominaPlantaRepositorio repositorio = new CorteNominaPlantaRepositorio();

            Corte_Nomina_Planta registroExistente =
                repositorio.ObtenerUno(c => c.anio == registroNuevo.Anio && c.mes == registroNuevo.Mes
                    && c.periodo == registroNuevo.Periodo);

            if (registroExistente != null)
            {
                throw new Exception("Ya existe Corte para este Año, Mes y Periodo.");
            }

            repositorio.Crear(new Corte_Nomina_Planta()
            {
                anio = registroNuevo.Anio,
                mes = registroNuevo.Mes,
                periodo = registroNuevo.Periodo,
                fecha_inicio = registroNuevo.FechaInicio,
                fecha_fin = registroNuevo.FechaFin
            });

            repositorio.GuardarCambios();
        }

        public static void Editar(CorteNominaPlantaModel registroEditar)
        {
            CorteNominaPlantaRepositorio repositorio = new CorteNominaPlantaRepositorio();

            Corte_Nomina_Planta registroExistente =
                repositorio.ObtenerUno(c => c.id != registroEditar.Id &&
                    (c.anio == registroEditar.Anio && c.mes == registroEditar.Mes
                    && c.periodo == registroEditar.Periodo));

            if (registroExistente != null)
            {
                throw new Exception("Ya existe Corte para este Año, Mes y Periodo.");
            }

            registroExistente = repositorio.ObtenerPorId(registroEditar.Id);

            if (registroExistente == null)
            {
                throw new Exception("El Tipo de Hora no existe");
            }
            else
            {
                registroExistente.anio = registroEditar.Anio;
                registroExistente.mes = registroEditar.Mes;
                registroExistente.periodo = registroEditar.Periodo;
                registroExistente.fecha_inicio = registroEditar.FechaInicio;
                registroExistente.fecha_fin = registroEditar.FechaFin;

                repositorio.Editar(registroExistente);
                repositorio.GuardarCambios();
            }
        }

        public static void Eliminar(int id)
        {
            CorteNominaPlantaRepositorio repositorio = new CorteNominaPlantaRepositorio();

            Corte_Nomina_Planta registroExistente =
                repositorio.ObtenerPorId(id);

            if (registroExistente != null)
            {
                repositorio.Eliminar(registroExistente);
                repositorio.GuardarCambios();
            }
        }
    }
}
