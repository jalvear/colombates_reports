﻿using capa_datos;
using capa_datos.Repositorios;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace capa_logica_negocio.Maestras
{
    public class CentroModel
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
        public string Siglas { get; set; }
        public DateTime FechaCreado { get; set; }
        public DateTime FechaModificado { get; set; }

        public string Descripcion_Siglas
        {
            get
            {
                string descripcion_siglas = string.Empty;

                if (!string.IsNullOrEmpty(Siglas) && !string.IsNullOrEmpty(Descripcion))
                {
                    descripcion_siglas = Descripcion + " (" + Siglas + ")";
                }

                return descripcion_siglas;
            }
        }

        public static List<CentroModel> ObtenerCentros()
        {
            using (Entities contexto = new Entities())
            {
                return contexto.Centro.OrderBy(c => c.descripcion).Select(c => new CentroModel
                {
                    Id = c.id,
                    Descripcion = c.descripcion,
                    FechaCreado = c.fecha_creado,
                    FechaModificado = c.fecha_modificado,
                    Siglas = c.siglas
                }).ToList();
            }
        }

        public static void Crear(CentroModel registroNuevo)
        {
            CentroRepositorio repositorio = new CentroRepositorio();

            Centro centroExistente =
                repositorio.ObtenerUno(c => registroNuevo.Siglas.ToLower().Equals(c.siglas.ToLower())
                || registroNuevo.Descripcion.ToLower().Equals(c.descripcion.ToLower()));

            if (centroExistente != null)
            {
                throw new Exception("Ya existe este Centro");
            }

            repositorio.Crear(new Centro()
            {
                descripcion = registroNuevo.Descripcion,
                siglas = registroNuevo.Siglas
            });

            repositorio.GuardarCambios();
        }

        public static void Editar(CentroModel registroEditar)
        {
            CentroRepositorio repositorio = new CentroRepositorio();

            Centro centroExistente =
                repositorio.ObtenerUno(c => c.id != registroEditar.Id &&
                (registroEditar.Siglas.ToLower().Contains(c.siglas.ToLower())
                || registroEditar.Descripcion.ToLower().Contains(c.descripcion.ToLower())));

            if (centroExistente != null)
            {
                throw new Exception("Ya existe este Centro");
            }

            centroExistente = repositorio.ObtenerPorId(registroEditar.Id);

            if (centroExistente == null)
            {
                throw new Exception("El Centro no existe");
            }
            else
            {
                centroExistente.descripcion = registroEditar.Descripcion;
                centroExistente.siglas = registroEditar.Siglas;

                repositorio.Editar(centroExistente);

                repositorio.GuardarCambios();
            }
        }

        public static void Eliminar(int id)
        {
            CentroRepositorio repositorio = new CentroRepositorio();

            Centro centroExistente =
                repositorio.ObtenerPorId(id);

            if (centroExistente != null)
            {
                repositorio.Eliminar(centroExistente);
                repositorio.GuardarCambios();
            }
        }
    }
}
