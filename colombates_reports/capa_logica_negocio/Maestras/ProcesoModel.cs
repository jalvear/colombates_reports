﻿using capa_datos;
using capa_datos.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace capa_logica_negocio.Maestras
{
    public class ProcesoModel
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
        public DateTime FechaCreado { get; set; }
        public DateTime FechaModificado { get; set; }

        public static List<ProcesoModel> ObtenerProcesos()
        {
            using (Entities contexto = new Entities())
            {
                return contexto.Proceso.OrderBy(c => c.descripcion).Select(c => new ProcesoModel
                {
                    Id = c.id,
                    Descripcion = c.descripcion,
                    FechaCreado = c.fecha_creado,
                    FechaModificado = c.fecha_modificado,
                }).ToList();
            }
        }

        public static void Crear(ProcesoModel registroNuevo)
        {
            ProcesoRepositorio repositorio = new ProcesoRepositorio();

            Proceso registroExistente =
                repositorio.ObtenerUno(c => registroNuevo.Descripcion.ToLower().Equals(c.descripcion.ToLower()));

            if (registroExistente != null)
            {
                throw new Exception("Ya existe este Proceso");
            }

            repositorio.Crear(new Proceso()
            {
                descripcion = registroNuevo.Descripcion
            });

            repositorio.GuardarCambios();
        }

        public static void Editar(ProcesoModel registroEditar)
        {
            ProcesoRepositorio repositorio = new ProcesoRepositorio();

            Proceso registroExistente =
                repositorio.ObtenerUno(c => c.id != registroEditar.Id &&
                registroEditar.Descripcion.ToLower().Equals(c.descripcion.ToLower()));

            if (registroExistente != null)
            {
                throw new Exception("Ya existe este Proceso");
            }

            registroExistente = repositorio.ObtenerPorId(registroEditar.Id);

            if (registroExistente == null)
            {
                throw new Exception("El Proceso no existe");
            }
            else
            {
                registroExistente.descripcion = registroEditar.Descripcion;

                repositorio.Editar(registroExistente);
                repositorio.GuardarCambios();
            }
        }

        public static void Eliminar(int id)
        {
            ProcesoRepositorio repositorio = new ProcesoRepositorio();

            Proceso registroExistente =
                repositorio.ObtenerPorId(id);

            if (registroExistente != null)
            {
                repositorio.Eliminar(registroExistente);
                repositorio.GuardarCambios();
            }
        }
    }
}
