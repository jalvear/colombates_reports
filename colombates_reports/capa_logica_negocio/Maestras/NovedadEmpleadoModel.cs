﻿using capa_datos;
using capa_datos.Repositorios;
using capa_logica_negocio.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace capa_logica_negocio.Maestras
{
    public class NovedadEmpleadoModel
    {
        public int Id { get; set; }
        public int EmpleadoId { get; set; }
        public int CentroId { get; set; }
        public int TipoHoraId { get; set; }
        public int TurnoId { get; set; }
        public int DiasHabiles { get; set; }
        public int HorasProgramadas { get; set; }
        public DateTime FechaRegistro { get; set; }
        public DateTime FechaCreado { get; set; }
        public DateTime FechaModificado { get; set; }

        public static List<NovedadEmpleadoViewModel> ObtenerNovedades(string filtroKendo, string ordenadoKendo, int skip = 0, int take = 10)
        {
            using (Entities contexto = new Entities())
            {
                if (string.IsNullOrEmpty(ordenadoKendo))
                {
                    ordenadoKendo = "ORDER BY NovedadEmpleadoFechaRegistro ";
                }

                string consulta = "SELECT * FROM vw_novedad_empleados " + filtroKendo +
                    ordenadoKendo + " OFFSET @skip ROWS FETCH NEXT @take ROWS ONLY ";

                SqlParameter[] parametros = new SqlParameter[]
                {
                new SqlParameter("@skip", skip),
                new SqlParameter("@take", take)
                };

                return contexto.Database.SqlQuery<NovedadEmpleadoViewModel>(consulta, parametros).ToList();
            }
        }

        public static int ObtenerCantidadNovedades(string filtroKendo)
        {
            using (Entities contexto = new Entities())
            {
                string consulta = "SELECT COUNT(EmpleadoId) FROM vw_novedad_empleados " + filtroKendo;

                return contexto.Database.SqlQuery<int>(consulta).FirstOrDefault();
            }
        }

        public static void Crear(NovedadEmpleadoModel registroNuevo)
        {
            NovedadEmpleadoRepositorio repositorio = new NovedadEmpleadoRepositorio();

            repositorio.Crear(new Novedad_Empleado()
            {
                centro_id = registroNuevo.CentroId,
                dias_habiles = registroNuevo.DiasHabiles,
                empleado_id = registroNuevo.EmpleadoId,
                fecha_registro = registroNuevo.FechaRegistro,
                tipo_hora_id = registroNuevo.TipoHoraId,
                turno_id = registroNuevo.TurnoId
            });

            repositorio.GuardarCambios();
        }

        public static void Crear(int[] variosId, NovedadEmpleadoModel registroNuevo)
        {
            if (variosId != null && registroNuevo != null) {
                foreach (var id in variosId)
                {
                    registroNuevo.EmpleadoId = id;

                    Crear(registroNuevo);
                }
            }
        }

        public static void Editar(NovedadEmpleadoModel registroEditar)
        {
            NovedadEmpleadoRepositorio repositorio = new NovedadEmpleadoRepositorio();

            Novedad_Empleado registroNovedad = repositorio.ObtenerPorId(registroEditar.Id);

            if (registroNovedad == null)
            {
                throw new Exception("Este registro de Novedad no existe");
            }
            else
            {
                registroNovedad.centro_id = registroEditar.CentroId;
                registroNovedad.dias_habiles = registroEditar.DiasHabiles;
                registroNovedad.empleado_id = registroEditar.EmpleadoId;
                registroNovedad.fecha_registro = registroEditar.FechaRegistro;
                registroNovedad.tipo_hora_id = registroEditar.TipoHoraId;
                registroNovedad.turno_id = registroEditar.TurnoId;

                repositorio.Editar(registroNovedad);

                repositorio.GuardarCambios();
            }
        }

        public static void Eliminar(int id)
        {
            NovedadEmpleadoRepositorio repositorio = new NovedadEmpleadoRepositorio();

            Novedad_Empleado registroNovedad =
                repositorio.ObtenerPorId(id);

            if (registroNovedad != null)
            {
                repositorio.Eliminar(registroNovedad);
                repositorio.GuardarCambios();
            }
        }
    }
}
