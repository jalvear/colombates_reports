﻿using capa_datos;
using capa_datos.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace capa_logica_negocio.Interfaz
{
    public class TiempoMaquinaModel : Tiempo_Maquina
    {
        public static void Crear(TiempoMaquinaModel registroNuevo)
        {
            TiempoMaquinaRepositorio repositorio = new TiempoMaquinaRepositorio();

            repositorio.Crear(registroNuevo);
            repositorio.GuardarCambios();
        }
    }
}
