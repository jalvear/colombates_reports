﻿using capa_datos;
using capa_datos.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace capa_logica_negocio.Interfaz
{
    public class ParadaMaquinaModel : Parada_Maquina
    {
        public static void Crear(ParadaMaquinaModel registroNuevo)
        {
            ParadaMaquinaRepositorio repositorio = new ParadaMaquinaRepositorio();

            repositorio.Crear(registroNuevo);
            repositorio.GuardarCambios();
        }
    }
}
