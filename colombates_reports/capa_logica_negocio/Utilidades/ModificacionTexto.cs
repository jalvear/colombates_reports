﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace capa_logica_negocio.Utilidades
{
    public class ModificacionTexto
    {
        public static string PrimeraLetraMayuscula(string texto)
        {
            if (!string.IsNullOrEmpty(texto))
            {
                string primeraLetra = texto.Substring(0, 1).ToUpper();

                if (texto.Length > 1)
                {
                    texto = primeraLetra + texto.Substring(1, (texto.Length - 1));
                }
                else
                {
                    texto = primeraLetra;
                }
            }

            return texto;
        }
    }
}
