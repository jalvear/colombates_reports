﻿using System;
using System.Globalization;

namespace capa_logica_negocio.Utilidades
{
    public class TratamientoFecha
    {
        public static int GetWeekOfYear(DateTime fecha)
        {
            CultureInfo myCI = new CultureInfo("es-CO");
            Calendar myCal = myCI.Calendar;

            // Gets the DTFI properties required by GetWeekOfYear.
            CalendarWeekRule myCWR = myCI.DateTimeFormat.CalendarWeekRule;
            DayOfWeek myFirstDOW = myCI.DateTimeFormat.FirstDayOfWeek;

            return myCal.GetWeekOfYear(fecha, myCWR, myFirstDOW);
        }
    }
}
