﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace capa_datos.Repositorios
{
    public interface IReporteRepositorio : IRepositorio<Reporte>
    {

    }

    public class ReporteRepositorio : BaseRepositorio<Reporte>, IReporteRepositorio
    {
        public override void Crear(Reporte entidad)
        {
            entidad.fecha_creado = entidad.fecha_modificado =
                DateTime.Now;

            base.Crear(entidad);
        }

        public override void Editar(Reporte entidad)
        {
            entidad.fecha_modificado = DateTime.Now;

            base.Editar(entidad);
        }
    }
}
