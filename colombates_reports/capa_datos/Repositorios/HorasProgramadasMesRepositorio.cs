﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace capa_datos.Repositorios
{
    public interface IHorasProgramadasMesRepositorio : IRepositorio<Horas_Programadas_Mes>
    {

    }

    public class HorasProgramadasMesRepositorio : BaseRepositorio<Horas_Programadas_Mes>, IHorasProgramadasMesRepositorio
    {
        public override void Crear(Horas_Programadas_Mes entidad)
        {
            entidad.fecha_creado = entidad.fecha_modificado =
                DateTime.Now;

            base.Crear(entidad);
        }

        public override void Editar(Horas_Programadas_Mes entidad)
        {
            entidad.fecha_modificado = DateTime.Now;

            base.Editar(entidad);
        }
    }
}
