﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace capa_datos.Repositorios
{
    public interface ITurnoRepositorio : IRepositorio<Turno>
    {

    }

    public class TurnoRepositorio : BaseRepositorio<Turno>, ITurnoRepositorio
    {
        public override void Crear(Turno entidad)
        {
            entidad.fecha_creado = entidad.fecha_modificado =
                DateTime.Now;

            base.Crear(entidad);
        }

        public override void Editar(Turno entidad)
        {
            entidad.fecha_modificado = DateTime.Now;

            base.Editar(entidad);
        }
    }
}
