﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace capa_datos.Repositorios
{
    public interface IEmpleadoSemanaRepositorio : IRepositorio<Empleado_Semana>
    {
    }

    public class EmpleadoSemanaRepositorio : BaseRepositorio<Empleado_Semana>, IEmpleadoSemanaRepositorio
    {
        public override void Crear(Empleado_Semana entidad)
        {
            entidad.fecha_creado = entidad.fecha_modificado =
                DateTime.Now;

            base.Crear(entidad);
        }

        public override void Editar(Empleado_Semana entidad)
        {
            entidad.fecha_modificado = DateTime.Now;

            base.Editar(entidad);
        }
    }
}
