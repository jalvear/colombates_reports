﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace capa_datos.Repositorios
{
    public interface IRepositorio<T> where T : class
    {
        DbSet ObtenerDbSet();
        T ObtenerPorId(long id);
        T ObtenerPorId(string id);
        T ObtenerUno(Expression<Func<T, bool>> condicion);
        IEnumerable<T> ObtenerVarios(Expression<Func<T, bool>> condicion);
        void Crear(T entidad);
        void Editar(T entidad);
        void Eliminar(T entidad);
        void Eliminar(Expression<Func<T, bool>> condicion);
    }
}
