﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace capa_datos.Repositorios
{
    public interface IMaquinaRepositorio : IRepositorio<Maquina>
    {

    }

    public class MaquinaRepositorio : BaseRepositorio<Maquina>, IMaquinaRepositorio
    {
        public override void Crear(Maquina entidad)
        {
            entidad.fecha_creado = entidad.fecha_modificado =
                DateTime.Now;

            base.Crear(entidad);
        }

        public override void Editar(Maquina entidad)
        {
            entidad.fecha_modificado = DateTime.Now;

            base.Editar(entidad);
        }
    }
}
