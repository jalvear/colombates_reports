﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace capa_datos.Repositorios
{
    public interface IEmpleadoRepositorio : IRepositorio<Empleado>
    {
        List<vw_empleados> FiltrarPor_Ficha_Nombre(string filtrar);
    }

    public class EmpleadoRepositorio : BaseRepositorio<Empleado>, IEmpleadoRepositorio
    {
        public override void Crear(Empleado entidad)
        {
            entidad.fecha_creado = entidad.fecha_modificado =
                DateTime.Now;

            base.Crear(entidad);
        }

        public override void Editar(Empleado entidad)
        {
            entidad.fecha_modificado = DateTime.Now;

            base.Editar(entidad);
        }

        public List<vw_empleados> FiltrarPor_Ficha_Nombre(string filtro)
        {
            List<vw_empleados> empleados = new List<vw_empleados>();

            if (!string.IsNullOrEmpty(filtro))
            {
                filtro = filtro.ToLower();

                //string[] filtroSeparado = filtro.Replace(' ', ',').Split(',');

                empleados = ObtenerContexto().vw_empleados
                    .Where(n => n.EmpleadoNombreCompleto.ToLower().Contains(filtro)
                    || n.EmpleadoFicha.Contains(filtro))
                    .Take(30)
                    .ToList();
            }

            return empleados;
        }
    }
}
