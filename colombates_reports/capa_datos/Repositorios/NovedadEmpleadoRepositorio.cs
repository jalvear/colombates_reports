﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace capa_datos.Repositorios
{
    public interface INovedadEmpleadoRepositorio : IRepositorio<Novedad_Empleado>
    {

    }

    public class NovedadEmpleadoRepositorio : BaseRepositorio<Novedad_Empleado>, INovedadEmpleadoRepositorio
    {
        public override void Crear(Novedad_Empleado entidad)
        {
            entidad.fecha_creado = entidad.fecha_modificado =
                DateTime.Now;

            base.Crear(entidad);
        }

        public override void Editar(Novedad_Empleado entidad)
        {
            entidad.fecha_modificado = DateTime.Now;

            base.Editar(entidad);
        }
    }
}
