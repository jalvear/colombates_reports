﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace capa_datos.Repositorios
{
    public interface ITiempoMaquinaRepositorio : IRepositorio<Tiempo_Maquina>
    {

    }

    public class TiempoMaquinaRepositorio : BaseRepositorio<Tiempo_Maquina>, ITiempoMaquinaRepositorio
    {
    }
}
