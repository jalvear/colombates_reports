﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace capa_datos.Repositorios
{
    public interface ICorteNominaPlantaRepositorio : IRepositorio<Corte_Nomina_Planta>
    {

    }

    public class CorteNominaPlantaRepositorio : BaseRepositorio<Corte_Nomina_Planta>, ICorteNominaPlantaRepositorio
    {
        public override void Crear(Corte_Nomina_Planta entidad)
        {
            entidad.fecha_creado = entidad.fecha_modificado =
                DateTime.Now;

            base.Crear(entidad);
        }

        public override void Editar(Corte_Nomina_Planta entidad)
        {
            entidad.fecha_modificado = DateTime.Now;

            base.Editar(entidad);
        }
    }
}
