﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace capa_datos.Repositorios
{
    public interface IParadaMaquinaRepositorio : IRepositorio<Parada_Maquina>
    {

    }

    public class ParadaMaquinaRepositorio : BaseRepositorio<Parada_Maquina>, IParadaMaquinaRepositorio
    {
    }
}
