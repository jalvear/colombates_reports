﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace capa_datos.Repositorios
{
    public interface IProcesoRepositorio : IRepositorio<Proceso>
    {

    }

    public class ProcesoRepositorio : BaseRepositorio<Proceso>, IProcesoRepositorio
    {
        public override void Crear(Proceso entidad)
        {
            entidad.fecha_creado = entidad.fecha_modificado =
                DateTime.Now;

            base.Crear(entidad);
        }

        public override void Editar(Proceso entidad)
        {
            entidad.fecha_modificado = DateTime.Now;

            base.Editar(entidad);
        }
    }
}
