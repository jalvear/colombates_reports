﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace capa_datos.Repositorios
{
    public interface IGrupoCausalExtraRepositorio : IRepositorio<Grupo_Causal_Extra>
    {

    }

    public class GrupoCausalExtraRepositorio : BaseRepositorio<Grupo_Causal_Extra>, IGrupoCausalExtraRepositorio
    {
        public override void Crear(Grupo_Causal_Extra entidad)
        {
            entidad.fecha_creado = entidad.fecha_modificado =
                DateTime.Now;

            base.Crear(entidad);
        }

        public override void Editar(Grupo_Causal_Extra entidad)
        {
            entidad.fecha_modificado = DateTime.Now;

            base.Editar(entidad);
        }
    }
}
