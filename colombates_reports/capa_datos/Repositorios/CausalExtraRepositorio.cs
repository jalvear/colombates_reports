﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace capa_datos.Repositorios
{
    public interface ICausalExtraRepositorio : IRepositorio<Causal_Extra>
    {

    }

    public class CausalExtraRepositorio : BaseRepositorio<Causal_Extra>, ICausalExtraRepositorio
    {
        public override void Crear(Causal_Extra entidad)
        {
            entidad.fecha_creado = entidad.fecha_modificado =
                DateTime.Now;

            base.Crear(entidad);
        }

        public override void Editar(Causal_Extra entidad)
        {
            entidad.fecha_modificado = DateTime.Now;

            base.Editar(entidad);
        }
    }
}
