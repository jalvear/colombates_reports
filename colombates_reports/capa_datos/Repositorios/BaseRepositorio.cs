﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace capa_datos.Repositorios
{
    public abstract class BaseRepositorio<T> where T : class
    {
        private Entities _contexto;
        private readonly DbSet<T> _dbSet;

        protected BaseRepositorio()
        {
            _contexto = new Entities();
            _dbSet = _contexto.Set<T>();
        }

        public Entities ObtenerContexto()
        {
            return this._contexto;
        }

        public void GuardarCambios()
        {
            this._contexto.SaveChanges();
        }

        public virtual DbSet ObtenerDbSet()
        {
            return _dbSet;
        }

        /// <summary>
        /// Prepara un registro para ser guardado en la base de datos
        /// </summary>
        /// <param name="entidad">Registro que va a ser guardado</param>
        public virtual void Crear(T entidad)
        {
            _dbSet.Add(entidad);
        }

        /// <summary>
        /// Prepara un registro para ser actualziado en la base de datos
        /// </summary>
        /// <param name="entidad">Registro qua se va a actualizar</param>
        public virtual void Editar(T entidad)
        {
            _dbSet.Attach(entidad);
            _contexto.Entry(entidad).State = EntityState.Modified;
        }

        /// <summary>
        /// Prepara un registro para ser eliminado de la base de datos
        /// </summary>
        /// <param name="entidad">Registro que se va a eliminar de la base de datos</param>
        public virtual void Eliminar(T entidad)
        {
            _dbSet.Remove(entidad);
        }

        /// <summary>
        /// Prepara varios registro para ser eliminados de la base de datos de acuerdo a una condición
        /// </summary>
        /// <param name="condicion">COndición que deben cumplir los registros para ser eliminados de la base de datos</param>
        public virtual void Eliminar(Expression<Func<T, bool>> condicion)
        {
            IEnumerable<T> objects = _dbSet.Where<T>(condicion).AsEnumerable();
            foreach (T obj in objects)
            {
                _dbSet.Remove(obj);
            }
        }

        /// <summary>
        /// Obtiene un registro de la base de datos por el id
        /// </summary>
        /// <param name="id">Id por el cual obtener el registro</param>
        /// <returns>Registro que se encontró, null en caso contrarfio</returns>
        public virtual T ObtenerPorId(long id)
        {
            return _dbSet.Find(id);
        }

        /// <summary>
        /// Obtiene un registro de la base de datos por el id
        /// </summary>
        /// <param name="id">Id por el cual obtener el registro</param>
        /// <returns>Registro que se encontró, null en caso contrarfio</returns>
        public virtual T ObtenerPorId(string id)
        {
            return _dbSet.Find(id);
        }

        /// <summary>
        /// Obtiene un registro de acuerdo a una condición
        /// </summary>
        /// <param name="condicion">Condición para obtener el registro</param>
        /// <returns>Registro que cumple con la condición, null en caso que no se encuentre</returns>
        public virtual T ObtenerUno(Expression<Func<T, bool>> condicion)
        {
            return _dbSet.FirstOrDefault(condicion);
        }

        /// <summary>
        /// Devuelve los registros que cumplen conn una condición
        /// </summary>
        /// <param name="condicion">Condición para traer los registros</param>
        /// <returns>registros que cumplen con la condición</returns>
        public virtual IEnumerable<T> ObtenerVarios(Expression<Func<T, bool>> condicion)
        {
            return _dbSet.Where(condicion);
        }
    }
}
