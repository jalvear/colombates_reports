﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace capa_datos.Repositorios
{
    public interface ITipoHoraRepositorio : IRepositorio<TipoHora>
    {

    }

    public class TipoHoraRepositorio : BaseRepositorio<TipoHora>, ITipoHoraRepositorio
    {
        public override void Crear(TipoHora entidad)
        {
            entidad.fecha_creado = entidad.fecha_modificado =
                DateTime.Now;

            base.Crear(entidad);
        }

        public override void Editar(TipoHora entidad)
        {
            entidad.fecha_modificado = DateTime.Now;

            base.Editar(entidad);
        }
    }
}
