﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace capa_datos.Repositorios
{
    public interface ICentroRepositorio : IRepositorio<Centro>
    {

    }

    public class CentroRepositorio : BaseRepositorio<Centro>, ICentroRepositorio
    {
        public override void Crear(Centro entidad)
        {
            entidad.fecha_creado = entidad.fecha_modificado =
                DateTime.Now;

            base.Crear(entidad);
        }

        public override void Editar(Centro entidad)
        {
            entidad.fecha_modificado = DateTime.Now;

            base.Editar(entidad);
        }
    }
}
