//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace capa_datos
{
    using System;
    using System.Collections.Generic;
    
    public partial class Grupo_Causal_Extra
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Grupo_Causal_Extra()
        {
            this.Causal_Extra = new HashSet<Causal_Extra>();
        }
    
        public int id { get; set; }
        public string descripcion { get; set; }
        public System.DateTime fecha_creado { get; set; }
        public System.DateTime fecha_modificado { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Causal_Extra> Causal_Extra { get; set; }
    }
}
