﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;
using capa_logica_negocio.Interfaz;
using System.Globalization;

namespace servicio_interfaz
{
    public partial class Service1 : ServiceBase
    {
        private string nombre_archivo_tiempos_maquina;
        private string nombre_archivo_paradas_maquina;
        private string carpeta_generados_sap;
        private string carpeta_procesados;
        private char separador_csv;
        private int intervalo_consultar_archivos;
        private bool estaOcupado;

        private const string mensaje_error_conversion = "Fila {0}: No se pudo convertir para el campo '{1}' el valor '{2}'";
        private const string mensaje_error_campo_vacio = "Fila {0}: El campo de '{1}' no puede estar vacío";
        private const string mensaje_error_insertando = "Error insertando la fila {0}\n\nDetalles error: {1}";

        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            #region Configurando servicio

            EventLog.WriteEntry("Configurando servicio");

            nombre_archivo_tiempos_maquina = ConfigurationManager.AppSettings
                .Get("nombre_archivo_tiempos_maquina");

            if (string.IsNullOrEmpty(nombre_archivo_tiempos_maquina))
            {
                EventLog.WriteEntry("No se especificó el nombre del archivo Tiempos de Máquina (nombre_archivo_tiempos_maquina)");
                Stop();
            }

            nombre_archivo_paradas_maquina = ConfigurationManager.AppSettings
                .Get("nombre_archivo_paradas_maquina");

            if (string.IsNullOrEmpty(nombre_archivo_paradas_maquina))
            {
                EventLog.WriteEntry("No se especificó el nombre del archivo Tiempos Perdidos (nombre_archivo_paradas_maquina)");
                Stop();
            }

            carpeta_generados_sap = ConfigurationManager.AppSettings
                .Get("carpeta_generados_sap");

            if (string.IsNullOrEmpty(carpeta_generados_sap))
            {
                EventLog.WriteEntry("No se especificó la carpeta donde se generan los archivos desde SAP (carpeta_generados_sap)");
                Stop();
            }

            string intervaloTexto = ConfigurationManager.AppSettings
                .Get("intervalo_consultar_archivos");

            if (string.IsNullOrEmpty(intervaloTexto))
            {
                EventLog.WriteEntry("No se especificó el intervalo para consultar existencia de archivos generados desde SAP (intervalo_consultar_archivos)");
                Stop();
            }
            else if (!int.TryParse(intervaloTexto, out intervalo_consultar_archivos))
            {
                EventLog.WriteEntry("El valor del intervalo para consultar existencia de archivos generados desde SAP no es un entero válido (intervalo_consultar_archivos)");
                Stop();
            }

            string separador = ConfigurationManager.AppSettings
                .Get("separador_csv");

            if (string.IsNullOrEmpty(separador))
            {
                EventLog.WriteEntry("No se especificó el separador usado en los archivos CSV desde SAP (separador_csv)");
                Stop();
            }
            else
            {
                separador_csv = separador[0];
            }

            EventLog.WriteEntry("Servicio configurado correctamente");

            #endregion

            // Crear carpeta si no existe
            if (!Directory.Exists(carpeta_generados_sap))
            {
                Directory.CreateDirectory(carpeta_generados_sap);
            }

            // Crear carpeta Procesados si no existe
            carpeta_procesados = carpeta_generados_sap + "Procesados/";

            if (!Directory.Exists(carpeta_procesados))
            {
                Directory.CreateDirectory(carpeta_procesados);
            }

            // Convertir de minutos a milésimas
            timer1.Interval = intervalo_consultar_archivos * 60000;
            timer1.Tick += Timer1_Tick;
            timer1.Enabled = true;
            timer1.Start();
        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            if (!estaOcupado)
            {
                estaOcupado = true;

                LeerArchivo_TiemposMaquina();

                LeerArchivo_ParadasMaquina();

                estaOcupado = false;
            }
        }

        protected override void OnStop()
        {
            if (timer1.Enabled)
            {
                timer1.Enabled = false;
                timer1.Stop();
            }
        }

        private void LeerArchivo_TiemposMaquina()
        {
            string rutaCompleta = carpeta_generados_sap + nombre_archivo_tiempos_maquina;

            if (File.Exists(rutaCompleta))
            {
                using (StreamReader reader = new StreamReader(rutaCompleta))
                {
                    int fila = 1;

                    while (!reader.EndOfStream)
                    {
                        string linea = reader.ReadLine();
                        string[] valores = linea.Split(separador_csv);

                        // Validar campo fecha
                        DateTime fecha;

                        if (string.IsNullOrEmpty(valores[0]))
                        {
                            EventLog.WriteEntry(
                                string.Format(mensaje_error_campo_vacio, fila, "Fecha"),
                                EventLogEntryType.Error);

                            continue;
                        }
                        else if (!DateTime.TryParseExact(valores[0], "dd/MM/yyyy",
                                CultureInfo.InvariantCulture, DateTimeStyles.None,
                                out fecha))
                        {
                            EventLog.WriteEntry(
                                string.Format(mensaje_error_conversion, fila, "Fecha", valores[0]),
                                EventLogEntryType.Error);

                            continue;
                        }

                        // Validar campo recurso
                        if (string.IsNullOrEmpty(valores[1]))
                        {
                            EventLog.WriteEntry(
                                string.Format(mensaje_error_campo_vacio, fila, "Recurso"),
                                EventLogEntryType.Error);

                            continue;
                        }

                        // Validar campo Cant.Alistamiento
                        int cant_alistamiento;

                        if (string.IsNullOrEmpty(valores[2]))
                        {
                            EventLog.WriteEntry(
                                string.Format(mensaje_error_campo_vacio, fila, "Cant.Alistamiento"),
                                EventLogEntryType.Error);

                            continue;
                        }
                        else if (!int.TryParse(valores[2], out cant_alistamiento))
                        {
                            EventLog.WriteEntry(
                                string.Format(mensaje_error_conversion, fila, "Cant.Alistamiento", valores[2]),
                                EventLogEntryType.Error);

                            continue;
                        }

                        // Validar campo Alistamiento
                        decimal alistamiento;

                        if (string.IsNullOrEmpty(valores[3]))
                        {
                            EventLog.WriteEntry(
                                string.Format(mensaje_error_campo_vacio, fila, "Alistamiento"),
                                EventLogEntryType.Error);

                            continue;
                        }
                        else if (!decimal.TryParse(valores[3], out alistamiento))
                        {
                            EventLog.WriteEntry(
                                string.Format(mensaje_error_conversion, fila, "Alistamiento", valores[3]),
                                EventLogEntryType.Error);

                            continue;
                        }

                        // Validar campo Corrida
                        decimal corrida;

                        if (string.IsNullOrEmpty(valores[4]))
                        {
                            EventLog.WriteEntry(
                                string.Format(mensaje_error_campo_vacio, fila, "Corrida"),
                                EventLogEntryType.Error);

                            continue;
                        }
                        else if (!decimal.TryParse(valores[4], out corrida))
                        {
                            EventLog.WriteEntry(
                                string.Format(mensaje_error_conversion, fila, "Corrida", valores[4]),
                                EventLogEntryType.Error);

                            continue;
                        }

                        // Validar campo Paradas
                        decimal paradas;

                        if (string.IsNullOrEmpty(valores[5]))
                        {
                            EventLog.WriteEntry(
                                string.Format(mensaje_error_campo_vacio, fila, "Paradas"),
                                EventLogEntryType.Error);

                            continue;
                        }
                        else if (!decimal.TryParse(valores[5], out paradas))
                        {
                            EventLog.WriteEntry(
                                string.Format(mensaje_error_conversion, fila, "Paradas", valores[5]),
                                EventLogEntryType.Error);

                            continue;
                        }

                        // Validar campo O.head
                        decimal o_head;

                        if (string.IsNullOrEmpty(valores[6]))
                        {
                            EventLog.WriteEntry(
                                string.Format(mensaje_error_campo_vacio, fila, "O.head"),
                                EventLogEntryType.Error);

                            continue;
                        }
                        else if (!decimal.TryParse(valores[6], out o_head))
                        {
                            EventLog.WriteEntry(
                                string.Format(mensaje_error_conversion, fila, "O.head", valores[6]),
                                EventLogEntryType.Error);

                            continue;
                        }

                        // Validar campo T.Horas
                        decimal t_horas;

                        if (string.IsNullOrEmpty(valores[7]))
                        {
                            EventLog.WriteEntry(
                                string.Format(mensaje_error_campo_vacio, fila, "T.Horas"),
                                EventLogEntryType.Error);

                            continue;
                        }
                        else if (!decimal.TryParse(valores[7], out t_horas))
                        {
                            EventLog.WriteEntry(
                                string.Format(mensaje_error_conversion, fila, "T.Horas", valores[7]),
                                EventLogEntryType.Error);

                            continue;
                        }

                        // Validar campo T.Unit
                        decimal t_unit;

                        if (string.IsNullOrEmpty(valores[8]))
                        {
                            EventLog.WriteEntry(
                                string.Format(mensaje_error_campo_vacio, fila, "T.Unit"),
                                EventLogEntryType.Error);

                            continue;
                        }
                        else if (!decimal.TryParse(valores[8], out t_unit))
                        {
                            EventLog.WriteEntry(
                                string.Format(mensaje_error_conversion, fila, "T.Unit", valores[8]),
                                EventLogEntryType.Error);

                            continue;
                        }

                        // Validar campo T.Kilos
                        decimal t_kilos;

                        if (string.IsNullOrEmpty(valores[9]))
                        {
                            EventLog.WriteEntry(
                                string.Format(mensaje_error_campo_vacio, fila, "T.Kilos"),
                                EventLogEntryType.Error);

                            continue;
                        }
                        else if (!decimal.TryParse(valores[9], out t_kilos))
                        {
                            EventLog.WriteEntry(
                                string.Format(mensaje_error_conversion, fila, "T.Kilos", valores[9]),
                                EventLogEntryType.Error);

                            continue;
                        }

                        TiempoMaquinaModel registroNuevo = new TiempoMaquinaModel()
                        {
                            fecha = fecha,
                            recurso = valores[1],
                            cant_alistamiento = cant_alistamiento,
                            alistamiento = alistamiento,
                            corrida = corrida,
                            paradas = paradas,
                            o_head = o_head,
                            t_horas = t_horas,
                            t_unit = t_unit,
                            t_kilos = t_kilos
                        };


                        try
                        {
                            TiempoMaquinaModel.Crear(registroNuevo);
                        }
                        catch (Exception ex)
                        {
                            EventLog.WriteEntry(
                                string.Format(mensaje_error_insertando, fila, ex.StackTrace),
                                EventLogEntryType.Error);

                            continue;
                        }

                        fila++;
                    }
                }

                MoverArchivo_A_Procesados(rutaCompleta);
            }
        }

        private void LeerArchivo_ParadasMaquina()
        {
            string rutaCompleta = carpeta_generados_sap + nombre_archivo_paradas_maquina;

            if (File.Exists(rutaCompleta))
            {
                using (StreamReader reader = new StreamReader(rutaCompleta))
                {
                    int fila = 1;

                    while (!reader.EndOfStream)
                    {
                        string linea = reader.ReadLine();
                        string[] valores = linea.Split(separador_csv);

                        // Validar campo Turno
                        if (string.IsNullOrEmpty(valores[0]))
                        {
                            EventLog.WriteEntry(
                                string.Format(mensaje_error_campo_vacio, fila, "Turno"),
                                EventLogEntryType.Error);

                            continue;
                        }

                        // Validar campo Denominación Turno
                        if (string.IsNullOrEmpty(valores[1]))
                        {
                            EventLog.WriteEntry(
                                string.Format(mensaje_error_campo_vacio, fila, "Denominación Turno"),
                                EventLogEntryType.Error);

                            continue;
                        }

                        // Validar campo Código Operario
                        if (string.IsNullOrEmpty(valores[2]))
                        {
                            EventLog.WriteEntry(
                                string.Format(mensaje_error_campo_vacio, fila, "Código Operario"),
                                EventLogEntryType.Error);

                            continue;
                        }

                        // Validar campo Nombre Operario
                        if (string.IsNullOrEmpty(valores[3]))
                        {
                            EventLog.WriteEntry(
                                string.Format(mensaje_error_campo_vacio, fila, "Nombre Operario"),
                                EventLogEntryType.Error);

                            continue;
                        }

                        // Validar campo recurso
                        if (string.IsNullOrEmpty(valores[4]))
                        {
                            EventLog.WriteEntry(
                                string.Format(mensaje_error_campo_vacio, fila, "Recurso"),
                                EventLogEntryType.Error);

                            continue;
                        }

                        // Validar campo Descripción recurso
                        if (string.IsNullOrEmpty(valores[5]))
                        {
                            EventLog.WriteEntry(
                                string.Format(mensaje_error_campo_vacio, fila, "Descripción recurso"),
                                EventLogEntryType.Error);

                            continue;
                        }

                        // Validar campo Orden de proceso
                        if (string.IsNullOrEmpty(valores[6]))
                        {
                            EventLog.WriteEntry(
                                string.Format(mensaje_error_campo_vacio, fila, "Orden de proceso"),
                                EventLogEntryType.Error);

                            continue;
                        }

                        // Validar campo Código Causa
                        if (string.IsNullOrEmpty(valores[7]))
                        {
                            EventLog.WriteEntry(
                                string.Format(mensaje_error_campo_vacio, fila, "Código Causa"),
                                EventLogEntryType.Error);

                            continue;
                        }

                        // Validar campo Descripción causa
                        if (string.IsNullOrEmpty(valores[8]))
                        {
                            EventLog.WriteEntry(
                                string.Format(mensaje_error_campo_vacio, fila, "Descripción causa"),
                                EventLogEntryType.Error);

                            continue;
                        }

                        // Validar campo UM de parada
                        if (string.IsNullOrEmpty(valores[9]))
                        {
                            EventLog.WriteEntry(
                                string.Format(mensaje_error_campo_vacio, fila, "UM de parada"),
                                EventLogEntryType.Error);

                            continue;
                        }

                        // Validar campo Fecha de Parada
                        DateTime fecha;

                        if (string.IsNullOrEmpty(valores[10]))
                        {
                            EventLog.WriteEntry(
                                string.Format(mensaje_error_campo_vacio, fila, "Fecha de Parada"),
                                EventLogEntryType.Error);

                            continue;
                        }
                        else if (!DateTime.TryParseExact(valores[10], "dd/MM/yyyy",
                                CultureInfo.InvariantCulture, DateTimeStyles.None,
                                out fecha))
                        {
                            EventLog.WriteEntry(
                                string.Format(mensaje_error_conversion, fila, "Fecha de Parada", valores[10]),
                                EventLogEntryType.Error);

                            continue;
                        }

                        // Validar campo Tiempo de parada
                        int tiempo_parada;

                        if (string.IsNullOrEmpty(valores[11]))
                        {
                            EventLog.WriteEntry(
                                string.Format(mensaje_error_campo_vacio, fila, "Tiempo de parada"),
                                EventLogEntryType.Error);

                            continue;
                        }
                        else if (!int.TryParse(valores[11], out tiempo_parada))
                        {
                            EventLog.WriteEntry(
                                string.Format(mensaje_error_conversion, fila, "Tiempo de parada", valores[11]),
                                EventLogEntryType.Error);

                            continue;
                        }

                        ParadaMaquinaModel registroNuevo = new ParadaMaquinaModel()
                        {
                            turno = valores[0],
                            denominacion_turno = valores[1],
                            codigo_operario = valores[2],
                            nombre_operario = valores[3],
                            recurso = valores[4],
                            descripcion_recurso = valores[5],
                            orden_proceso = valores[6],
                            codigo_causa = valores[7],
                            descripcion_causa = valores[8],
                            um_parada = valores[9],
                            fecha_parada = fecha,
                            tiempo_parada = tiempo_parada
                        };


                        try
                        {
                            ParadaMaquinaModel.Crear(registroNuevo);
                        }
                        catch (Exception ex)
                        {
                            EventLog.WriteEntry(
                                string.Format(mensaje_error_insertando, fila, ex.StackTrace),
                                EventLogEntryType.Error);

                            continue;
                        }

                        fila++;
                    }
                }

                MoverArchivo_A_Procesados(rutaCompleta);
            }
        }

        private void MoverArchivo_A_Procesados(string rutaActual)
        {
            // Mover archivo a procesados
            string nuevoNombre = Path.GetFileNameWithoutExtension(rutaActual)
                + DateTime.Now.ToString("_ddMMyyy_hhmmss")
                + ".csv";

            string rutaNueva = carpeta_procesados + nuevoNombre;

            File.Move(rutaActual, rutaNueva);
        }
    }
}
